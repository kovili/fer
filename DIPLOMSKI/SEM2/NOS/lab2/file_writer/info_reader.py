import re
import base64
from enum import Enum


class Modes(Enum):
    READING_ITEM = 0
    SEARCHING_ITEM = 1


MAX_LEN = 60


def read(file_name):
    # Read file
    file_name = file_name if file_name is not None else "error"
    file = open("./crypto_info/" + file_name, 'r')
    lines = file.readlines()

    # Throw out useless lines
    begin = lines.index("---BEGIN OS2 CRYPTO DATA---\n")
    end = lines.index("---END OS2 CRYPTO DATA---\n")
    lines = lines[begin + 1: end]

    # Init values for extracting info
    file_info = {}
    mode = Modes.SEARCHING_ITEM
    current_list = []
    current_val = ""
    current_key = ""

    for line in lines:
        # Find key
        if mode == Modes.SEARCHING_ITEM:
            pattern = re.search("^\s*(\w+):\s*$", line)
            if pattern is not None:
                current_key = pattern.group(1)
                mode = Modes.READING_ITEM
        # Find item(s)
        elif mode == Modes.READING_ITEM:
            pattern = re.search("^\s*([\w=/+-0-9]+)\s*", line)
            # Found item part
            if pattern is not None:
                current_val += pattern.group(1)
                # Found item ending
                if len(pattern.group(1)) < MAX_LEN:
                    if current_key in ('key_length', 'secret_key', 'private_exponent', 'modulus',
                                       'public_exponent', 'envelope_crypt_key', 'initialization_vector', 'mode'):
                        current_val = '0' if current_val == '' else current_val
                        current_val = int(current_val, 16)
                    elif current_key in ('data', 'envelope_data'):
                        # Get ascii text - use base64 as input
                        base64_bytes = current_val.encode('ascii')
                        current_val = base64.b64decode(base64_bytes)
                        # current_val = message_bytes.decode('ascii')
                    current_list.append(current_val)
                    current_val = ""
            # Found empty (hopefully) line
            else:
                file_info[current_key] = current_list
                mode = Modes.SEARCHING_ITEM
                current_val = ""
                current_key = ""
                current_list = []
    return file_info
