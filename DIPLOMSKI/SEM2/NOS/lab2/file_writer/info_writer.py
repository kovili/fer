import base64

MAX_LEN = 60


def write(destination=None, args=None, **kwargs):
    kwargs = kwargs if args is None else args
    destination = destination if destination is not None else "error"
    lines = ['---BEGIN OS2 CRYPTO DATA---\n']
    for item in kwargs.items():
        add_line(lines, item)
    lines.append('---END OS2 CRYPTO DATA---\n')

    file = open('./crypto_info/' + destination, 'w')
    file.writelines(lines)
    file.close()


def add_line(lines, item):
    key = item[0]
    values = item[1]
    # Make it a list for special cases (digital envelope)
    if type(values) is not list:
        values = [values]
    for value in values:
        if value is None or value == "":
            return

    # Normalize data
    for index in range(len(values)):
        if key in ('key_length', 'secret_key', 'private_exponent', 'public_exponent',
                   'envelope_crypt_key', 'initialization_vector', 'modulus', 'mode'):
            values[index] = hex(int(values[index])).split('x')[-1]
            if len(values[index]) % 2 != 0:
                values[index] = '0' + values[index]
        if key in ('data', 'envelope_data'):
            # Get base64 string - take bytes as input
            base64_bytes = base64.b64encode(values[index])
            values[index] = base64_bytes.decode('ascii')

    # Append to list
    lines.append(key + ":" + "\n")
    for value in values:
        value_len = len(value)
        while value_len > MAX_LEN:
            value_part = value[:60]
            value = value[60:]
            lines.append("    " + value_part + "\n")
            value_len -= 60
        lines.append("    " + value + "\n")
    lines.append("\n")
