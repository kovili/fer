import sys


def int_to_byte_array(num: int, byte_len: int) -> bytes:
    return num.to_bytes(byte_len, sys.byteorder)


def byte_array_to_str(array: bytes) -> str:
    return array.decode('ascii')


def str_to_byte_array(text: str) -> bytearray:
    return bytearray(text, 'ascii')


def byte_array_to_int(array: bytes) -> int:
    return int.from_bytes(array, byteorder=sys.byteorder, signed=False)
