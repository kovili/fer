from crypto.cryptosystems import config_cryptosystems
from file_writer.info_reader import read
from model.writeable import Writeable


class SealedEnvelope(Writeable):
    def __init__(self, target_folder, target_file, config_file=None, config=None):
        self.target_folder = target_folder
        self.target_file = target_file
        self.config_file = config_file
        self.my_config_file = config_file if config_file is None else config_file + "_my"
        self.config = config if config is not None else {'hashf': 'SHA-256', 'asymf': 'RSA', 'symf': '3DES'}
        self.symmetric_function, self.hash_function, self.asymmetric_function = self.config.get(
            'symf'), self.config.get('hashf'), self.config.get('asymf')
        self.hash_cryptosystem, self.symmetric_cryptosystem, self.asymmetric_cryptosystem = None, None, None
        self.my_asymmetric_cryptosystem = None
        self.signature, self.envelope_data, self.encrypted_key = None, None, None

    def seal(self):
        self.symmetric_cryptosystem, self.asymmetric_cryptosystem, self.hash_cryptosystem = config_cryptosystems(
            self.config_file, self.config, needed=(True, True, True))
        self.my_asymmetric_cryptosystem = \
            config_cryptosystems(self.my_config_file, self.config, needed=(False, True, False))[1]

        file = open(self.target_folder + '/' + self.target_file, 'r')
        text = "".join(file.readlines())

        self.envelope_data = self.symmetric_cryptosystem.encrypt(text)
        self.encrypted_key = self.asymmetric_cryptosystem.encrypt(self.symmetric_cryptosystem.key)
        self.signature = self.hash_cryptosystem.digest(text)
        self.signature = hex(self.my_asymmetric_cryptosystem.decrypt(int(self.signature, 16), opposite=True))[2:]

        self.write_to_file(self.target_file + ".seal")
        self.symmetric_cryptosystem.write_to_file(self.target_file + ".seal." + self.symmetric_cryptosystem.name())
        self.asymmetric_cryptosystem.write_to_file(self.target_file + ".seal." + self.asymmetric_cryptosystem.name())
        self.my_asymmetric_cryptosystem.write_to_file(
            self.target_file + "_my.seal." + self.my_asymmetric_cryptosystem.name())

    def unseal(self):
        if self.config_file is None:
            raise ValueError("config_file must point to the file with the configurations")
        seal_info = read(self.config_file + ".seal")

        self.config['symf'] = seal_info['method'][0]
        self.config['asymf'] = seal_info['method'][1]
        self.config['hashf'] = seal_info['method'][2]

        self.symmetric_cryptosystem, self.asymmetric_cryptosystem, self.hash_cryptosystem = config_cryptosystems(
            self.config_file + '.seal', self.config, needed=(True, True, True))
        self.my_asymmetric_cryptosystem = \
            config_cryptosystems(self.my_config_file + '.seal', self.config, needed=(False, True, False))[1]

        key = self.asymmetric_cryptosystem.decrypt(seal_info['envelope_crypt_key'][0])
        if key != self.symmetric_cryptosystem.key:
            raise ValueError(
                'Symmetric key from symmetric algorithm file and decrypted symmetric key are not the same!')

        decrypted_text = self.symmetric_cryptosystem.decrypt(seal_info['envelope_data'][0])

        signature = self.hash_cryptosystem.digest(decrypted_text)
        self.hash_cryptosystem.signature = hex(self.my_asymmetric_cryptosystem.encrypt(
            int(self.hash_cryptosystem.signature, 16), opposite=True))[2:]
        if signature == self.hash_cryptosystem.signature:
            print("File signature is correct - file is unmodified")
            print("====================DECRYPTED TEXT===================")
            print(decrypted_text)
            print("=======================END===========================")
            file = open("./crypto_files/" + self.target_file + ".seal.decrypted", 'w')
            file.write(decrypted_text)
        else:
            print("File signature differs from the recorded signature - file is modified")

    def info(self):
        return {'description': 'Sealed Envelope', 'file_name': self.target_file,
                'method': [self.symmetric_function, self.asymmetric_function, self.hash_function],
                'key_length': [self.symmetric_cryptosystem.key_len * 8, self.asymmetric_cryptosystem.key_length,
                               self.hash_cryptosystem.info()['key_length']],
                'signature': self.signature, 'envelope_data': self.envelope_data,
                'envelope_crypt_key': self.encrypted_key
                }
