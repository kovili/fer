from crypto.cryptosystems import config_cryptosystems
from file_writer.info_reader import read
from model.writeable import Writeable


class DigitalEnvelope(Writeable):
    def __init__(self, target_folder, target_file, config_file=None, config=None):
        if config is None:
            config = {'symf': '3DES', 'asymf': 'RSA'}
        self.target_folder = target_folder
        self.target_file = target_file
        self.envelope_data, self.encrypted_key = None, None
        self.config_file = config_file
        self.config = config if config is not None else {'symf': '3DES', 'asymf': 'RSA'}
        self.symmetric_function, self.asymmetric_function = config.get('symf'), config.get('asymf')
        self.symmetric_cryptosystem, self.asymmetric_cryptosystem = None, None

    def envelop(self):
        self.symmetric_cryptosystem, self.asymmetric_cryptosystem, hashf = \
            config_cryptosystems(self.config_file, self.config, needed=(True, True, False))

        file = open(self.target_folder + "/" + self.target_file, 'r')
        text = "".join(file.readlines())
        self.envelope_data = self.symmetric_cryptosystem.encrypt(text)
        self.encrypted_key = self.asymmetric_cryptosystem.encrypt(self.symmetric_cryptosystem.key)

        self.write_to_file(self.target_file + ".env")
        self.symmetric_cryptosystem.write_to_file(self.target_file + ".env." + self.symmetric_cryptosystem.name())
        self.asymmetric_cryptosystem.write_to_file(self.target_file + ".env." + self.asymmetric_cryptosystem.name())

    def deenvelop(self):
        if self.config_file is None:
            raise ValueError("config_file must point to the file with the configurations")
        envelope_info = read(self.config_file + ".env")

        self.config['symf'] = envelope_info['method'][0]
        self.config['asymf'] = envelope_info['method'][1]
        self.symmetric_cryptosystem, self.asymmetric_cryptosystem, hashf = config_cryptosystems(
            self.config_file + ".env", self.config, needed=(True, True, False))

        key = self.asymmetric_cryptosystem.decrypt(envelope_info['envelope_crypt_key'][0])
        if key != self.symmetric_cryptosystem.key:
            raise ValueError(
                'Symmetric key from symmetric algorithm file and decrypted symmetric key are not the same!')

        decrypted_text = self.symmetric_cryptosystem.decrypt(envelope_info['envelope_data'][0])

        print("====================DECRYPTED TEXT===================")
        print(decrypted_text)
        print("=======================END===========================")

        file = open("./crypto_files/" + self.target_file + ".env.decrypted", 'w')
        file.write(decrypted_text)

    def info(self):
        return {'description': 'Envelope', 'file_name': self.target_file,
                'method': [self.symmetric_function, self.asymmetric_function],
                'key_length': [self.symmetric_cryptosystem.key_len * 8, self.asymmetric_cryptosystem.key_length],
                'envelope_data': self.envelope_data, 'envelope_crypt_key': self.encrypted_key
                }
