from crypto.cryptosystems import config_cryptosystems
from file_writer.info_reader import read
from model.writeable import Writeable


class DigitalSignature(Writeable):
    def __init__(self, target_folder, target_file, config_file=None, config=None):
        self.target_folder = target_folder
        self.target_file = target_file
        self.config_file = config_file
        self.my_config_file = config_file if config_file is None else config_file + "_my"
        self.config = config if config is not None else {'hashf': 'SHA-256', 'asymf': 'RSA'}
        self.hash_function, self.asymmetric_function = self.config.get('hashf'), self.config.get('asymf')
        self.hash_cryptosystem, self.asymmetric_cryptosystem = None, None
        self.signature = None

    def sign(self):
        sym, asymf, self.hash_cryptosystem = config_cryptosystems(self.config_file, self.config,
                                                                  needed=(False, False, True))
        sym, self.asymmetric_cryptosystem, hashf = config_cryptosystems(self.my_config_file, self.config,
                                                                        needed=(False, True, False))

        file = open(self.target_folder + '/' + self.target_file, 'r')
        self.signature = self.hash_cryptosystem.digest("".join(file.readlines()))
        self.signature: int = self.asymmetric_cryptosystem.decrypt(int(self.signature, 16), opposite=True)
        self.signature = hex(self.signature)
        self.signature = self.signature[2:]

        self.write_to_file(self.target_file + ".sign")
        self.asymmetric_cryptosystem.write_to_file(self.target_file + "_my.sign." + self.asymmetric_cryptosystem.name())

    def check_signature(self):
        if self.config_file is None:
            raise ValueError("config_file must point to the file with the configurations")
        signature_info = read(self.config_file + ".sign")

        self.config['hashf'] = signature_info['method'][0]
        self.config['asymf'] = signature_info['method'][1]

        sym, asymf, self.hash_cryptosystem = config_cryptosystems(self.config_file + '.sign', self.config,
                                                                  needed=(False, False, True))
        sym, self.asymmetric_cryptosystem, hashf = config_cryptosystems(self.my_config_file + '.sign', self.config,
                                                                        needed=(False, True, False))

        signature = self.hash_cryptosystem.digest(
            "".join(open(self.target_folder + '/' + self.target_file, 'r').readlines()))
        self.hash_cryptosystem.signature = self.asymmetric_cryptosystem.encrypt(
            int(self.hash_cryptosystem.signature, 16), opposite=True)
        self.hash_cryptosystem.signature = hex(self.hash_cryptosystem.signature)
        self.hash_cryptosystem.signature = self.hash_cryptosystem.signature[2:]

        if signature == self.hash_cryptosystem.signature:
            print("File signature is correct - file is unmodified")
        else:
            print("File signature differs from the recorded signature - file is modified")

    def info(self):
        return {'description': 'Signature', 'file_name': self.target_file,
                'method': [self.hash_function, self.asymmetric_function],
                'key_length': [self.hash_cryptosystem.info()['key_length'], self.asymmetric_cryptosystem.key_length],
                'signature': self.signature
                }
