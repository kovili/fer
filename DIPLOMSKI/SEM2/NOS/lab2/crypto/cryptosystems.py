from implementation.des3 import NOS_DES3
from implementation.nos_aes import NOS_AES
from implementation.rsa import NOS_RSA

from implementation.sha_256 import NOS_SHA256
from implementation.sha_512 import NOS_SHA512

__crypto_symmetric_implemented__ = {'3DES': NOS_DES3, 'AES': NOS_AES}
__crypto_asymmetric_implemented__ = {'RSA': NOS_RSA}
__crypto_hash_implemented__ = {'SHA-256': NOS_SHA256, 'SHA-512': NOS_SHA512}


def get_symmetric_cryptosystem(cryptosystem: str):
    return None if cryptosystem is None else __crypto_symmetric_implemented__.get(cryptosystem)


def get_asymmetric_cryptosystem(cryptosystem: str):
    return None if cryptosystem is None else __crypto_asymmetric_implemented__.get(cryptosystem)


def get_hash_cryptosystem(cryptosystem: str):
    return None if cryptosystem is None else __crypto_hash_implemented__.get(cryptosystem)


def config_cryptosystems(config_file=None, config=None, needed=(False, False, False)):
    symmetric_function, asymmetric_function, hash_function = config.get('symf'), config.get('asymf'), config.get(
        'hashf')
    symmetric_cryptosystem_constructor = get_symmetric_cryptosystem(symmetric_function)
    asymmetric_cryptosystem_constructor = get_asymmetric_cryptosystem(asymmetric_function)
    hash_cryptosystem_constructor = get_hash_cryptosystem(hash_function)
    symmetric_cryptosystem, asymmetric_cryptosystem, hash_cryptosystem = None, None, None

    if symmetric_function is not None and needed[0]:
        if config_file is not None:
            symmetric_cryptosystem = symmetric_cryptosystem_constructor(
                file_name=config_file)

        else:
            symmetric_cryptosystem = symmetric_cryptosystem_constructor(
                key_len=config.get('sym_key_len'),
                mode=config.get('sym_mode'),
            )

    if asymmetric_function is not None and needed[1]:
        if config_file is not None:
            asymmetric_cryptosystem = asymmetric_cryptosystem_constructor(
                file_name=config_file)
        else:
            asymmetric_cryptosystem = asymmetric_cryptosystem_constructor(
                key_len=config.get('asym_key_len'),
            )

    if hash_function is not None and needed[2]:
        hash_function = hash_cryptosystem_constructor(config_file)

    return symmetric_cryptosystem, asymmetric_cryptosystem, hash_function
