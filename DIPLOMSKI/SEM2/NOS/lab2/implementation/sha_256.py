from Cryptodome.Hash import SHA256

from model.hash import NOS_HASH


class NOS_SHA256(NOS_HASH):
    def __init__(self, file_name):
        super().__init__(SHA256.new, file_name=file_name)

    def info(self):
        return {'Description': 'Hashing function', 'Method': 'SHA-256', 'key_length': 256}
