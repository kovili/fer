from Cryptodome.Random import get_random_bytes

from model.sym_crypto import *
from file_writer.info_reader import read
from util.byte_ops import byte_array_to_int, int_to_byte_array


class NOS_DES3(Symmetric_Crypto):
    block_len = 8

    def __init__(self, key_len: int = 24, mode=DES3.MODE_OFB, file_name: str = None):
        super().__init__(key_len, mode, file_name)
        self.key_len = key_len if key_len is not None else 24
        self.mode = mode if mode is not None else DES3.MODE_OFB
        self.init_vector = None
        # Generate key
        if file_name is None:
            while True:
                try:
                    self.iv_bytes = get_random_bytes(NOS_DES3.block_len)
                    self.init_vector = byte_array_to_int(self.iv_bytes)
                    break
                except ValueError:
                    pass
            while True:
                try:
                    self.key_bytes = DES3.adjust_key_parity(get_random_bytes(self.key_len))
                    self.key = byte_array_to_int(self.key_bytes)
                    break
                except ValueError:
                    pass
        # Extract key from file
        else:
            options = read(file_name + ".3des")
            self.key = options['secret_key'][0]
            self.key_len = options['key_length'][0] // 8
            self.encrypted = options.get('data')
            self.encrypted = None if self.encrypted is None else self.encrypted[0]
            self.init_vector = options['initialization_vector'][0] if options[
                                                                          'initialization_vector'] is not None else None
            self.key_bytes = int_to_byte_array(self.key, key_len)
            self.iv_bytes = int_to_byte_array(self.init_vector, NOS_DES3.block_len) \
                if self.init_vector is not None else None
            self.mode = options['mode'][0]
        self.init_vector = self.init_vector if self.mode in (DES3.MODE_CBC, DES3.MODE_CFB, DES3.MODE_OFB) else None
        self.cypher: DES3 = DES3.new(self.key_bytes, self.mode, iv=self.iv_bytes)

    def get_cypher(self):
        return DES3.new(self.key_bytes, self.mode, iv=self.iv_bytes)

    def info(self):
        return {'description': 'Secret key', 'method': '3DES', 'key_length': self.key_len * 8, 'secret_key': self.key,
                'initialization_vector': self.init_vector, 'mode': self.mode}

    def name(self) -> str:
        return "3des"


if __name__ == '__main__':
    des = NOS_DES3()
    cyphertext = des.encrypt("Test")
    plaintext = des.decrypt(cyphertext)
    assert ("Test" == plaintext)
