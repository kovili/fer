from Cryptodome.Hash import SHA512

from model.hash import NOS_HASH


class NOS_SHA512(NOS_HASH):
    def __init__(self, file_name):
        super().__init__(SHA512.new, file_name=file_name)

    def info(self):
        return {'Description': 'Hashing function', 'Method': 'SHA-512', 'key_length': 512}

