from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes

from file_writer.info_reader import read
from model.sym_crypto import Symmetric_Crypto
from util.byte_ops import byte_array_to_int, int_to_byte_array


class NOS_AES(Symmetric_Crypto):
    block_len = 16

    def __init__(self, key_len: int = 16, mode=AES.MODE_OFB, file_name: str = None):
        super().__init__(key_len, mode, file_name)
        self.key_len = key_len if key_len is not None else 16
        self.mode = mode if mode is not None else AES.MODE_OFB
        # Generate key
        if file_name is None:
            while True:
                try:
                    self.key_bytes = get_random_bytes(self.key_len)
                    self.iv_bytes = get_random_bytes(NOS_AES.block_len)
                    self.init_vector = byte_array_to_int(self.iv_bytes)
                    self.key = byte_array_to_int(self.key_bytes)
                    break
                except ValueError:
                    pass
        # Extract key from file
        else:
            options = read(file_name + ".aes")
            self.key = options['secret_key'][0]
            self.key_len = options['key_length'][0] // 8
            self.encrypted = options.get('data')
            self.encrypted = None if self.encrypted is None else self.encrypted[0]
            self.init_vector = options['initialization_vector'][0] if options[
                                                                          'initialization_vector'] is not None else None
            self.key_bytes = int_to_byte_array(self.key, self.key_len)
            self.iv_bytes = int_to_byte_array(self.init_vector, NOS_AES.block_len) if \
                self.init_vector is not None else None
            self.mode = options['mode'][0]
        self.iv_bytes = self.iv_bytes if self.mode in (AES.MODE_CFB, AES.MODE_OFB) else None

    def get_cypher(self):
        return AES.new(self.key_bytes, self.mode, iv=self.iv_bytes)

    def info(self):
        return {'description': 'Secret key', 'method': 'AES', 'key_length': self.key_len * 8, 'secret_key': self.key,
                'initialization_vector': self.init_vector, 'mode': self.mode}

    def name(self) -> str:
        return "aes"


if __name__ == '__main__':
    aes = NOS_AES(32)
    file = open('../crypto_files/test.txt', 'r')
    text = "".join(file.readlines())
    ciphertext = aes.encrypt(text)
    plaintext = aes.decrypt(ciphertext)
    assert(text == plaintext)
