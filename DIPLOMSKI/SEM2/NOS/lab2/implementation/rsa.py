from Cryptodome.Cipher import PKCS1_OAEP
from Cryptodome.PublicKey import RSA

from file_writer.info_reader import read
from file_writer.info_writer import write
from model.asym_crypto import Asymmetric_Crypto
from util.byte_ops import int_to_byte_array, byte_array_to_int


class NOS_RSA(Asymmetric_Crypto):
    def __init__(self, key_len=2048, file_name=None):
        super().__init__(key_len, file_name)
        self.key_length = key_len if key_len is not None else 2048
        # Generate key
        if file_name is None:
            while True:
                try:
                    self.key = RSA.generate(self.key_length)
                    self.opposite_key = RSA.construct((self.key.n, self.key.d, self.key.e))
                    break
                except ValueError:
                    pass
        # Extract key from file
        else:
            # Get modulus and private key
            options = read(file_name + ".rsa")
            self.key_length = options['key_length'][0]
            self.modulus = options['modulus'][0]
            self.private_exponent = options['private_exponent'][0]
            # Get public key
            options = read(file_name + ".rsa.pub")
            self.public_exponent = options['public_exponent'][0]
            # Get RSA key object
            self.key = RSA.construct((self.modulus, self.public_exponent, self.private_exponent))
            self.opposite_key = RSA.construct((self.modulus, self.private_exponent, self.public_exponent))

    def encrypt(self, encrypt_key: int, opposite: bool = False) -> int:
        # Encrypt the session key with the public RSA key
        if opposite:
            decipher = PKCS1_OAEP.new(self.opposite_key)
            return byte_array_to_int(decipher.decrypt(int_to_byte_array(encrypt_key, self.key_length // 8)))
        else:
            cipher = PKCS1_OAEP.new(self.key)
            return byte_array_to_int(
                cipher.encrypt(int_to_byte_array(encrypt_key, 128 if self.key_length == 2048 else 64)))

    def decrypt(self, encrypted: int, opposite: bool = False) -> int:
        # Decrypt the session key with the private RSA key
        if opposite:
            cipher = PKCS1_OAEP.new(self.opposite_key)
            return byte_array_to_int(
                cipher.encrypt(int_to_byte_array(encrypted, 128 if self.key_length == 2048 else 64)))
        else:
            decipher = PKCS1_OAEP.new(self.key)
            return byte_array_to_int(decipher.decrypt(int_to_byte_array(encrypted, self.key_length // 8)))

    def info(self):
        return ({'description': 'Public key', 'method': 'RSA', 'key_length': self.key_length, 'modulus': self.key.n,
                 'public_exponent': self.key.e},
                {'description': 'Private key', 'method': 'RSA', 'key_length': self.key_length, 'modulus': self.key.n,
                 'private_exponent': self.key.d})

    def write_to_file(self, file_name):
        write(file_name + ".pub", self.info()[0])
        write(file_name, self.info()[1])

    def name(self) -> str:
        return "rsa"


if __name__ == '__main__':
    rsa = NOS_RSA()
    num = 25465
    cyphertext = rsa.encrypt(num)
    plaintext = rsa.decrypt(cyphertext)
    assert (num == plaintext)
    print(rsa.info())
