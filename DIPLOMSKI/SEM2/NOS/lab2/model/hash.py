from Cryptodome.Hash import SHA256, SHA512

from file_writer.info_reader import read
from model.writeable import Writeable
from util.byte_ops import str_to_byte_array


class NOS_HASH(Writeable):
    def __init__(self, hash_constructor, file_name=None):
        self.hash_constructor = hash_constructor
        file_info = None
        if file_name is not None:
            file_info = read(file_name)
        self.signature = None if file_info is None else file_info['signature'][0]

    def digest(self, text):
        return self.hash_constructor(str_to_byte_array(text)).hexdigest()


if __name__ == '__main__':
    nos_hash = NOS_HASH(SHA256.new)
    print(nos_hash.digest(text="test"))
    nos_hash = NOS_HASH(SHA512.new)
    print(nos_hash.digest(text="test"))
