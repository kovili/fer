from file_writer.info_writer import write
from model.informable import Informant


class Writeable(Informant):
    def write_to_file(self, file_name):
        write(file_name, self.info())