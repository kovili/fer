from model.writeable import Writeable


class Asymmetric_Crypto(Writeable):
    def __init__(self, key_len=2048, file_name=None):
        self.key_len = key_len
        self.file_name = file_name

    def encrypt(self, encrypt_key: int, opposite: bool) -> int:
        return 0

    def decrypt(self, encrypted: int, opposite: bool) -> int:
        return 0

    def name(self) -> str:
        return "default"
