from Cryptodome.Cipher import DES3

from model.writeable import Writeable
from util.byte_ops import byte_array_to_str, str_to_byte_array


class Symmetric_Crypto(Writeable):
    def __init__(self, key_len: int = 24, mode=DES3.MODE_OFB, file_name: str = None):
        self.cypher = None
        self.key_len = key_len
        self.mode = mode
        self.file_name = file_name

    def get_cypher(self):
        return self.cypher

    def encrypt(self, text: str) -> bytes:
        return self.get_cypher().encrypt(str_to_byte_array(text))

    def decrypt(self, cipher: bytes) -> str:
        return byte_array_to_str(self.get_cypher().decrypt(cipher))

    def name(self) -> str:
        return "default"
