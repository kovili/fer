import argparse
import os
import time

from Cryptodome.Cipher import AES, DES

from crypto.cryptosystems import config_cryptosystems
from crypto.digital_envelope import DigitalEnvelope
from crypto.digital_signature import DigitalSignature
from crypto.sealed_digital_envelope import SealedEnvelope
from file_writer.info_writer import write


def get_text(file_name):
    return "".join(open('./crypto_files/' + file_name, 'r').readlines())


def sym_alg_chooser(decryption=False):
    key_len, mode = None, None
    while True:
        sym_alg = input(
            'Choose the symmetric encryption algorithm, must be one of: \'3DES\', \'AES\'. \'quit\' to exit: ')

        if sym_alg == 'quit':
            return -1
        if sym_alg not in ('3DES', 'AES'):
            print('Invalid algorithm! Try again.')
        else:
            if decryption:
                return sym_alg, None, None
            while True:
                if sym_alg == 'AES':
                    key_len = input('Choose key length in bytes. Must be one of 16, 24, 32: ')
                    if key_len not in ('16', '24', '32'):
                        print('Invalid key_len value. Choose again.')
                        continue
                    key_len = int(key_len)
                elif sym_alg == '3DES':
                    key_len = input('Choose key length in bytes. Must be one of 16, 24: ')
                    if key_len not in ('16', '24'):
                        print('Invalid key_len value. Choose again.')
                        continue
                    key_len = int(key_len)
                break
            while True:
                aes_modes = {'cfb': AES.MODE_CFB, 'ofb': AES.MODE_OFB}
                des3_modes = {'cfb': DES.MODE_CFB, 'ofb': DES.MODE_OFB}
                mode = input('Choose mode. Must be one of: \'cfb\', \'ofb\': ')
                mode = aes_modes.get(mode) if sym_alg == 'AES' else des3_modes.get(mode)
                if mode is None:
                    print('Invalid mode chosen! Choose again.')
                    continue
                break
            return sym_alg, key_len, mode


def asym_alg_chooser(decryption=False):
    while True:
        asym_alg = input('Choose the asymmetric encryption algorithm, must be one of: \'RSA\'. \'quit\' to exit: ')
        if asym_alg == 'quit':
            return -1
        if asym_alg not in 'RSA':
            print('Invalid algorithm! Try again.')
        else:
            if decryption:
                return asym_alg, None
            while True:
                key_len = input('Choose key length in bits. Must be one of 1024, 2048: ')
                if key_len not in ('1024', '2048'):
                    print('Invalid key length! Try again.')
                    continue
                return asym_alg, int(key_len)


def hash_alg_chooser():
    while True:
        hash_alg = input(
            'Choose the asymmetric encryption algorithm, must be one of: \'SHA-256\', \'SHA-512\'. \'quit\' to exit: ')
        if hash_alg == 'quit':
            return -1
        if hash_alg not in ('SHA-256', 'SHA-512'):
            print('Invalid algorithm! Try again.')
        else:
            return hash_alg


def encrypt(file_name):
    sym_alg, sym_key_len, sym_mode = sym_alg_chooser()
    if sym_alg == -1:
        return sym_alg
    sym_crypto = config_cryptosystems(config={'symf': sym_alg, 'sym_key_len': sym_key_len, 'sym_mode': sym_mode},
                                      needed=(True, False, False))[0]
    encrypted = sym_crypto.encrypt(get_text(file_name))
    write(file_name + ".solo." + sym_crypto.name(), {'description': 'Crypted file',
                                                     'method': sym_crypto.name(),
                                                     'key_length': sym_crypto.key_len * 8,
                                                     'data': encrypted,
                                                     'secret_key': sym_crypto.key,
                                                     'initialization_vector': sym_crypto.init_vector,
                                                     'mode': sym_crypto.mode})
    return 0


def decrypt(file_name):
    sym_alg, sym_key_len, sym_mode = sym_alg_chooser(decryption=True)
    if sym_alg == -1:
        return sym_alg
    sym_crypto = \
        config_cryptosystems(config_file=file_name + ".solo", config={'symf': sym_alg}, needed=(True, False, False))[0]
    decrypted = sym_crypto.decrypt(sym_crypto.encrypted)
    file = open('./crypto_files/' + file_name + ".decrypted", 'w+')
    file.write(decrypted)
    return 0


def envelop(file_name):
    sym_alg, sym_key_len, sym_mode = sym_alg_chooser()
    if sym_alg == -1:
        return sym_alg
    asym_alg, asym_key_len = asym_alg_chooser()
    if asym_alg == -1:
        return asym_alg
    nos_envelope = DigitalEnvelope(target_folder='./crypto_files/', target_file=file_name, config={
        'symf': sym_alg, 'sym_key_len': sym_key_len, 'sym_mode': sym_mode,
        'asymf': asym_alg, 'asym_key_len': asym_key_len,
    })
    nos_envelope.envelop()
    return 0


def deenvelop(file_name):
    nos_envelope = DigitalEnvelope(target_folder='./crypto_files/', target_file=file_name, config_file=file_name)
    nos_envelope.deenvelop()
    return 0


def sign(file_name):
    asym_alg, asym_key_len = asym_alg_chooser()
    if asym_alg == -1:
        return asym_alg
    hash_alg = hash_alg_chooser()
    if hash_alg == -1:
        return hash_alg
    nos_signature = DigitalSignature(target_folder='./crypto_files/', target_file=file_name,
                                     config={'asymf': asym_alg, 'asym_key_len': asym_key_len, 'hashf': hash_alg})
    nos_signature.sign()
    return 0


def check(file_name):
    nos_signature = DigitalSignature(target_folder='./crypto_files/', target_file=file_name, config_file=file_name)
    nos_signature.check_signature()
    return 0


def seal(file_name):
    sym_alg, sym_key_len, sym_mode = sym_alg_chooser()
    if sym_alg == -1:
        return sym_alg
    asym_alg, asym_key_len = asym_alg_chooser()
    if asym_alg == -1:
        return asym_alg
    hash_alg = hash_alg_chooser()
    if hash_alg == -1:
        return hash_alg
    nos_sealed_env = SealedEnvelope(target_folder='./crypto_files/', target_file=file_name,
                                    config={'symf': sym_alg, 'sym_key_len': sym_key_len, 'sym_mode': sym_mode,
                                            'asymf': asym_alg, 'asym_key_len': asym_key_len, 'hashf': hash_alg})
    nos_sealed_env.seal()
    return 0


def unseal(file_name):
    nos_sealed_env = SealedEnvelope(target_folder='./crypto_files/', target_file=file_name, config_file=file_name)
    nos_sealed_env.unseal()
    return 0


def clean(file_name):
    extensions = ('.env', '.env.3des', '.env.aes', '.env.rsa', '.env.rsa.pub',
                  '.seal', '.seal.3des', '.seal.aes', '.seal.rsa', '.seal.rsa.pub',
                  '.sign', '.solo.3des', '.solo.aes',
                  '_my.seal.rsa', '_my.seal.rsa.pub', '_my.sign.rsa', '_my.sign.rsa.pub')
    for ext in extensions:
        if os.path.isfile('./crypto_info/' + file_name + ext):
            os.remove('./crypto_info/' + file_name + ext)

    extensions = ('.decrypted', '.env.decrypted', '.seal.decrypted')
    for ext in extensions:
        if os.path.isfile('./crypto_files/' + file_name + ext):
            os.remove('./crypto_files/' + file_name + ext)
    return 0


def shell(arguments):
    if arguments.option == 1:
        path = './crypto_files/proba.nos'
        file_name = 'proba.nos'
        sym_alg = '3DES'
        sym_mode = DES.MODE_OFB
        sym_key_len = 24
        asym_alg = 'RSA'
        asym_key_len = 2048
        hash_alg = 'SHA-256'

        print('Creating file proba.nos and filling with Lorem Ipsum text...')
        time.sleep(1)
        file = open(path, 'w+')
        text = "Lorem ipsum dolor sit amet, consectetur adipiscing " \
               "elit. Nulla suscipit nisi in commodo viverra. \n" \
               "Fusce a nisi pharetra, consectetur quam vel, " \
               "sagittis velit. Ut euismod risus ultrices tellus \n" \
               "commodo, id rhoncus urna sollicitudin. Nunc viverra " \
               "maximus tortor vitae congue. In tempor suscipit\n " \
               "tortor sit amet pulvinar. Proin feugiat interdum " \
               "efficitur. Duis tristique elit ut elit pulvinar \n" \
               "blandit. Aenean bibendum ligula vitae blandit dapibus. " \
               "Nulla rutrum condimentum quam, ac bibendum \n" \
               "ligula consectetur vel. In facilisis metus mi, id " \
               "dignissim turpis faucibus et. Lorem ipsum dolor sit \n" \
               "amet, consectetur adipiscing elit. Nullam augue nulla, " \
               "pretium eu viverra vitae, sodales id libero. \n" \
               "Sed neque dui, porta ac vulputate nec, auctor in augue. " \
               "Phasellus in feugiat nibh. "

        print('Original text:')
        print(text)
        print()

        file.write(text)
        file.close()

        text = "".join(open(path, 'r').readlines())
        print('File successfully created!')
        print()

        print('Encrypting proba.nos...')
        time.sleep(1)
        sym_crypto = config_cryptosystems(config={'symf': sym_alg, 'sym_key_len': sym_key_len, 'sym_mode': sym_mode},
                                          needed=(True, False, False))[0]
        encrypted = sym_crypto.encrypt(text)
        write(file_name + ".solo." + sym_crypto.name(), {'description': 'Crypted file',
                                                         'method': sym_crypto.name(),
                                                         'key_length': sym_crypto.key_len * 8,
                                                         'data': encrypted,
                                                         'secret_key': sym_crypto.key,
                                                         'initialization_vector': sym_crypto.init_vector,
                                                         'mode': sym_crypto.mode})
        print('proba.nos encrypted...')
        print()

        print('Decrypting proba.nos...')
        time.sleep(1)
        sym_crypto = \
            config_cryptosystems(config_file=file_name + ".solo", config={'symf': sym_alg},
                                 needed=(True, False, False))[0]
        decrypted = sym_crypto.decrypt(sym_crypto.encrypted)
        file = open('./crypto_files/' + file_name + ".decrypted", 'w+')
        file.write(decrypted)
        print('proba.nos decrypted')
        print()

        print('Enveloping proba.nos...')
        time.sleep(1)
        nos_envelope = DigitalEnvelope(target_folder='./crypto_files/', target_file=file_name, config={
            'symf': sym_alg, 'sym_key_len': sym_key_len, 'sym_mode': sym_mode,
            'asymf': asym_alg, 'asym_key_len': asym_key_len,
        })
        nos_envelope.envelop()
        print('proba.nos enveloped')
        print()

        print('De-enveloping proba.nos...')
        time.sleep(1)
        nos_envelope = DigitalEnvelope(target_folder='./crypto_files/', target_file=file_name, config_file=file_name)
        nos_envelope.deenvelop()
        print('proba.nos de-enveloped...')
        print()

        print('Signing proba.nos...')
        time.sleep(1)
        nos_signature = DigitalSignature(target_folder='./crypto_files/', target_file=file_name,
                                         config={'asymf': asym_alg, 'asym_key_len': asym_key_len, 'hashf': hash_alg})
        nos_signature.sign()
        print('proba.nos signed...')
        print()

        print('Checking signature for proba.nos...')
        time.sleep(1)
        nos_signature = DigitalSignature(target_folder='./crypto_files/', target_file=file_name, config_file=file_name)
        nos_signature.check_signature()
        print('Signature checked...')
        print()

        print('Sealing proba.nos...')
        time.sleep(1)
        nos_sealed_env = SealedEnvelope(target_folder='./crypto_files/', target_file=file_name,
                                        config={'symf': sym_alg, 'sym_key_len': sym_key_len, 'sym_mode': sym_mode,
                                                'asymf': asym_alg, 'asym_key_len': asym_key_len, 'hashf': hash_alg})
        nos_sealed_env.seal()
        print('proba.nos sealed...')
        print()

        print('Unsealing proba.nos...')
        time.sleep(1)
        nos_sealed_env = SealedEnvelope(target_folder='./crypto_files/', target_file=file_name, config_file=file_name)
        nos_sealed_env.unseal()
        print('proba.nos unsealed...')
        print()

        to_clean = input('Would you like to clean the files before exiting? y/n: ')
        if to_clean.lower() == 'y':
            clean(file_name)

        print('Demonstration finished, see you again soon :)')

    elif arguments.option == 2:
        while True:
            try:
                print("Choose the file you want to encode and put it in the crypto_files folder.")
                file_name = input(
                    "Input the name of the file you have chosen (just the name, not the full path). 'quit' to exit: ")
                if file_name == 'quit':
                    return
                if not os.path.isfile('./crypto_files/' + file_name):
                    print('The file you have chosen is not a file! Try again.')
                    print()
                    continue

                operation = ('encrypt', 'decrypt', 'envelop', 'de-envelop', 'sign', 'check', 'seal', 'unseal', 'clean')
                while True:
                    action = input(
                        'Choose action, must be one of: \'encrypt\', \'decrypt\', \'envelop\', \'de-envelop\', '
                        '\'sign\', \'check\', \'seal\', \'unseal\', \'clean\'. \'quit\' to exit: ')
                    if action == 'quit':
                        return
                    if action not in operation:
                        print('Invalid action! Try again.')
                    else:
                        break

                code = 0
                if action == 'encrypt':
                    code = encrypt(file_name)
                elif action == 'decrypt':
                    code = decrypt(file_name)
                elif action == 'envelop':
                    code = envelop(file_name)
                elif action == 'de-envelop':
                    code = deenvelop(file_name)
                elif action == 'sign':
                    code = sign(file_name)
                elif action == 'check':
                    code = check(file_name)
                elif action == 'seal':
                    code = seal(file_name)
                elif action == 'unseal':
                    code = unseal(file_name)
                elif action == 'clean':
                    code = clean(file_name)

                if code == -1:
                    to_clean = input('Would you like to clean the files before exiting? y/n: ')
                    if to_clean.lower() == 'y':
                        clean(file_name)
                    return
                else:
                    answer = input("Would you like to try something else? y/n: ")
                    if answer.lower() != 'y':
                        to_clean = input('Would you like to clean the files before exiting? y/n: ')
                        if to_clean.lower() == 'y':
                            clean(file_name)
                        break

            except FileNotFoundError:
                print("The file with the chosen name does not exist")
    else:
        print('Invalid option! Exiting...')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run crypto shell.')
    parser.add_argument('--option', type=int, default=1,
                        help='Shell mode - must be one of:\n\
                                \t1) Default mode - everything is set up automatically\n\
                                \t2) Guided mode - the user defines how the program is run')
    args = parser.parse_args()
    shell(args)
    print('Thank you for using the NOS cryptography module. See you soon!')
