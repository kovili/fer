#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <signal.h>

// Number of times a process will request CS entry
#define REPEAT_CNT 5
// Minimal number of processes in simulation
#define MIN_PROC 3
// Maximal number of processes in simulation
#define MAX_PROC 10

// Minimal time (in miliseconds) a process will sleep
#define MIN_SLEEP 100
// Maximal time (in miliseconds) a process will sleep
#define MAX_SLEEP 2000
// A milisecond - used for converting from microseconds
#define MILI 1000

// Reading end of the pipe
#define PIPE_READ 0
// Writing end of the pipe
#define PIPE_WRITE 1

// Time in miliseconds a process waits after finishing with simulation, until checking if the other processes are done
#define WAIT_TIME 800
// Size of buffer for writing messages to pipes
#define WRITE_BUFF_SIZE 9
// Buffer size for storing string representation of the database
#define DB_BUFF_SIZE 2000
// Swap buffer for storing different kinds of strings
#define SWP_BUFF_SIZE 100
// A second in miliseconds
#define MILIS_IN_SECOND 1000

// An entry to the database. Access to the database is considered a critical section and will be controlled using the Lamport protocol
typedef struct db_entry {
	pid_t process_id;
	unsigned long log_clock;
	unsigned long cs_entry_cnt;
} db_entry;

// Structure of a process request for entrance to the critical section
// It contains the process index and current process logical clock needed for the Lamport protocol
typedef struct request {
	int process_index;
	int process_clock;
} request;

// A node in the request list (which acts as a priority queue where the items are sorted according to the Lamport protocol)
typedef struct request_node {
	request req;
	struct	request_node *next;
} request_node;

// Request list - implemented as a priority queue according to the Lamport protocol
typedef struct request_list {
	request_node *first;
	request_node *last;
} request_list;

// Priority queue insert of a new node.
// Arguments list and new_node must not be NULL.
// Argument new_node must have defined members process_clock and process_index (so, in the case of ints, they both shouldn't be arbitrarily set to 0).
bool queue_insert(request_list *list, request_node *new_node) {
	request_node *curr_node = list->first, *previous_node = NULL;

	if(new_node == NULL) {
		fprintf(stderr, "Cannot enter NULL pointer!\n");
		return false;
	} else if(list == NULL) {
		fprintf(stderr, "List is NULL pointer!\n");
		return false;
	}

	if(curr_node = NULL) {
		list->first = new_node;
		list->last = new_node;
	} else {
		for(curr_node = list->first; curr_node != NULL; curr_node = curr_node->next) {
			if(curr_node->req.process_clock > new_node->req.process_clock) {
				break;
			} else if(curr_node->req.process_clock == new_node->req.process_clock) {
				if(curr_node->req.process_index > new_node->req.process_index) {
					break;
				}
			}
			previous_node = curr_node;
		}
		if(previous_node == NULL) {
			list->first = new_node;
			new_node->next = curr_node;
		} else if(curr_node == NULL) {
			list->last = new_node;
			previous_node->next = new_node;
			new_node->next = NULL;
		} else {
			previous_node->next = new_node;
			new_node->next = curr_node;
		}
	}
	return true;
}

// Removes first element from queue. Should probably be used
bool queue_remove(request_list *list) {
	request_node *first = list->first;
	if(list->first == NULL) {
		fprintf(stderr, "Cannot remove from empty list!\n");
		return false;
	}
	list->first = first->next;
	first->next = NULL;
	free(first);
	return true;
}

// Removes process from queue with the given ID. Not sure if I should be using it or not...
bool queue_remove_id(request_list *list, int process_index) {
	request_node *previous = NULL;
	for(request_node *curr = list->first; curr != NULL; curr = curr->next) {
		if(curr->req.process_index == process_index) {
			if(previous == NULL) {
				list->first = curr->next;
			} else {
				previous->next = curr->next;
			}
			curr->next = NULL;
			free(curr);
			return true;
		}
		previous = curr;
	}
	return false;
}

// Creates new node with given arguments - this is the only way a new node should be created. By calling this function
request_node *create_node(int process_index, int process_clock) {
	request_node *new = (request_node *) malloc(sizeof(request_node));
	new->req.process_index = process_index;
	new->req.process_clock = process_clock;
	new->next = NULL;
	return new;
}

// Extracts the process count argument from the argv list. Also prints error if the program is called without the proper number of arguments
int extract_count(int argc, char **argv) {
	int process_count;
	if(argc != 2) {
		fprintf(stderr, "This program takes in one argument.\n");
		fprintf(stderr, "The number of processes to start for distributed Lamport mutual exclusion protocol simulation.\n");
		fprintf(stderr, "Tested, and should be used for 3-10 processes.\n");
		exit(2);
	}
	process_count = atoi(argv[1]);

	// Check if start argument is valid
	if(process_count < MIN_PROC || process_count > MAX_PROC) {
		fprintf(stderr, "Current implementation allows only for [3-10] processes in simulation.\n");
		exit(2);
	}
	return process_count;
}

// Print out nicely formatted database representation
void print_entries2(db_entry *entries, int process_count, int process_index) {
	char buff[DB_BUFF_SIZE];
	char swap[SWP_BUFF_SIZE];
	buff[0] = '\0';

	for(int i = 0; i < process_count; i++) {
		strcat(buff, "\033[34m==========   \033[0m");
	}
	strcat(buff, "\n");

	for(int i = 0; i < process_count; i++) {
		snprintf(swap, 40, "\033[34m|ID %5d|   \033[0m", entries[i].process_id);
		strcat(buff, swap);
	}
	strcat(buff, "\n");

	for(int i = 0; i < process_count; i++) {
		strcat(buff, "\033[34m|        |   \033[0m");
	}
	strcat(buff, "\n");

	for(int i = 0; i < process_count; i++) {
		snprintf(swap, 40, "\033[34m|CL %5lu|-->\033[0m", entries[i].log_clock);
		strcat(buff, swap);
	}
	strcat(buff, "\n");

	for(int i = 0; i < process_count; i++) {
		strcat(buff, "\033[34m|        |   \033[0m");
	}
	strcat(buff, "\n");

	for(int i = 0; i < process_count; i++) {
		snprintf(swap, 40, "\033[34m|CS %5lu|   \033[0m", entries[i].cs_entry_cnt);
		strcat(buff, swap);
	}
	strcat(buff, "\n");

	for(int i = 0; i < process_count; i++) {
		strcat(buff, "\033[34m==========   \033[0m");
	}
	strcat(buff, "\n");

	puts(buff);
}

// Initializes all entry so they have the correct value for their properties (might not be needed)
void init_entries(db_entry *entries, int process_count) {
	for(int i = 0; i < process_count; i++) {
		entries[i].log_clock = 0;
		entries[i].cs_entry_cnt = 0;
	}
}

// Helper function for writing to pipe
void write_msg(int fd, char *buff) {
	int nread;
	if((nread = write(fd, buff, WRITE_BUFF_SIZE)) == -1) {
		fprintf(stderr, "Process could not write to pipe.\n");
		exit(1);
	}
}

// Send message to all the other processes
void send_message(int *pipes, int process_index, int log_clock, int process_count, char msg_type) {
	int offset = process_index * process_count, error, nwrite;
	char buff[WRITE_BUFF_SIZE];
	buff[0] = '\0';
	nwrite = snprintf(buff, WRITE_BUFF_SIZE, "%c%d-%5d", msg_type, process_index, log_clock);
	buff[WRITE_BUFF_SIZE] = '\0';
	for(int i = 0; i < process_count; i++) {
		if(i != process_index) {
			write_msg(pipes[2 * (i * process_count + process_index) + PIPE_WRITE], buff);
		}
	}
}

// Insert own entry node into queue and send a REQUEST message to all of the other processes
int send_request(int *pipes, int process_index, int process_count, int log_clock, request_list *wait_list) {
	request_node *new = create_node(process_index, log_clock);
	queue_insert(wait_list, new); 	
	send_message(pipes, process_index, log_clock, process_count, 'z');
}

// Checks if the Lamport condition is satisfied (the process got an answer from all the other processes and is first in the queue)
bool lamport_condition_satisfied(int process_index, int process_count, int reply_count, request_list *wait_list) {
	if(reply_count != (process_count - 1)) {
		return false;
	}

	if(wait_list == NULL) {
		fprintf(stderr, "Wait list is NULL, something went horribly wrong. Exiting...\n");
		exit(2);
	} else if(wait_list->first->req.process_index == process_index) {
		return true;
	} 

	return false;
}

// Extracts process index and local clock from message
void extract_numbers(char *message, int *msg_indx, int *msg_clock) {
	char *token;
	token = strtok(message, "-");
	*msg_indx = atoi(token);
	token = strtok(NULL, "-");
	*msg_clock = atoi(token);
}

// Get replies from other processes before going into the critical section
void get_replies(db_entry *database, int *pipes, int process_index, int process_count, int *requests_over, request *private_entry, request_list *wait_list, fd_set remember_set, struct timeval *timeout, int maxfd) {
	fd_set active_set;
	int reply_cnt = 0, retval, fd_index;
	char buff[WRITE_BUFF_SIZE];
	int msg_indx, msg_clock, new_clock;
	request_node *new, *test;
	int nwrite;

	while(!lamport_condition_satisfied(process_index, process_count, reply_cnt, wait_list)) {
		active_set = remember_set;
		retval = select(maxfd + 1, &active_set, NULL, NULL, timeout);
		if(retval == -1) {
			fprintf(stderr, "Error while using select!\n");

		} else if(retval) {
			for(int i = 0; i < process_count; i++) {
				fd_index = 2 * (process_count * process_index + i) + PIPE_READ;
				if(FD_ISSET(pipes[fd_index], &active_set)) {
					read(pipes[fd_index], buff, WRITE_BUFF_SIZE);

					if(strlen(buff) > 0) {

						if(buff[0] == 'z') {
							extract_numbers(buff + 1, &msg_indx, &msg_clock);
							new_clock = msg_clock > private_entry->process_clock ? msg_clock : private_entry->process_clock;
							private_entry->process_clock = new_clock + 1;
							new = create_node(msg_indx, msg_clock);
							queue_insert(wait_list, new);
							nwrite = snprintf(buff, WRITE_BUFF_SIZE, "o%d-%5d", msg_indx, private_entry->process_clock);
							buff[WRITE_BUFF_SIZE] = '\0';
							write_msg(pipes[2 * (msg_indx * process_count + process_index) + PIPE_WRITE], buff);
						
						// Exit message, remove from queue
						} else if (buff[0] == 'i') {

							extract_numbers(buff + 1, &msg_indx, &msg_clock);
							queue_remove(wait_list);
							(*requests_over)++;


						} else if (buff[0] == 'o') {
							extract_numbers(buff + 1, &msg_indx, &msg_clock);
							new_clock = msg_clock > private_entry->process_clock ? msg_clock : private_entry->process_clock;
							private_entry->process_clock = new_clock + 1;
							reply_cnt++;

						} else {
							fprintf(stderr, "Unexpected message - %s\n", buff);
							fprintf(stderr, "Simulation probably over...\n");
							break;
						}

					} else {
						fprintf(stderr, "Pipe closed from other side. Unexpected and rude.\n");
					}
				}
			}
		} else {
			break;
		}
	}
}

// Remove own entry node from queue and send EXIT message to all of the other processes
void send_exit(int *pipes, int process_index, int process_count, int log_clock, request_list *wait_list) {
	queue_remove(wait_list);
	send_message(pipes, process_index, log_clock, process_count, 'i');
}

// Enter critical section. Update own DB entry and print all of the DB entries.
void update_database(db_entry *database, int process_index, int process_count, request private_entry) {
	database[process_index].log_clock = private_entry.process_clock;
	database[process_index].cs_entry_cnt = database[process_index].cs_entry_cnt + 1;
	print_entries2(database, process_count, process_index);
}

// Run the Lamport protocol to ensure only one process enters the critical section
void lamport_protocol(db_entry *database, int *pipes, int process_index, int process_count, int *requests_over, request *private_entry, request_list *wait_list, fd_set remember_set, int maxfd) {
	// Copy the logical clock so it's available for the exit message
	int clock_copy = private_entry->process_clock;
	send_request(pipes, process_index, process_count, clock_copy, wait_list);
	get_replies(database, pipes, process_index, process_count, requests_over, private_entry, wait_list, remember_set, NULL, maxfd);
	update_database(database, process_index, process_count, *private_entry);
	send_exit(pipes, process_index, process_count, clock_copy, wait_list);
}


// Run simulation of the Lamport protocol. All of the child processes enter this function
void run_simulation(db_entry *database, int *pipes, int process_index, int process_count) {
	request private_entry;
	fd_set remember_set;
	request_list wait_list;
	struct timeval tv;
	int maxfd = -1;
	int offset = (process_count * process_index);
	// Number of open pipes
	int pipe_count = process_count * process_count;
	// Lower index bound of own pipe reading locations
	int lower_bound = process_index * process_count;
	// Upper index bound of own pipe reading locations
	int upper_bound  = (process_index + 1) * process_count;
	int milis;
	// Current number of other processes that are done
	int requests_over = 0;
	// Stop condition
	int simulation_over_request_count = (process_count - 1) * REPEAT_CNT;

	wait_list.first = NULL;
	wait_list.last = NULL;

	private_entry.process_index = 0;
	private_entry.process_clock = 0;

	// Set up pipes and FD for select
	FD_ZERO(&remember_set);
	for(int i = 0; i < process_count; i++) {
		if(i != process_index) {
			FD_SET(pipes[2 * (i + offset) + PIPE_READ], &remember_set);
			maxfd = pipes[2 * (i + offset) + PIPE_READ] > maxfd ? pipes[2 * (i + offset) + PIPE_READ] : maxfd;
		}
	}

	// Initialize random seed
	srand((unsigned long) time(NULL) + (process_index * 17));

	// Enter critical section via Lamport protocol REPEAT_CNT times
	for(int i = 0; i < REPEAT_CNT; i++) {
		// Izvedi Lamportov protokol
		lamport_protocol(database, pipes, process_index, process_count, &requests_over, &private_entry, &wait_list, remember_set, maxfd);

		// Determine random wait time
		milis = MIN_SLEEP + (rand() % (MAX_SLEEP - MIN_SLEEP));
		tv.tv_sec = milis / MILIS_IN_SECOND;
		milis = milis % MILIS_IN_SECOND;
		tv.tv_usec = milis * MILI;

		// Check if simulation is over before going in again
		if(requests_over < simulation_over_request_count) {
			get_replies(database, pipes, process_index, process_count, &requests_over, &private_entry, &wait_list, remember_set, &tv, maxfd);
		}
	}

	// Check if all the other processes are done and wait for their messages until they are not
	while(requests_over < simulation_over_request_count) {
		tv.tv_sec = 0;
		tv.tv_usec = WAIT_TIME * MILI;
		// Print a nice yellow waiting message
		printf("\033[33mProcess %d waiting for others to end...\033[0m\n", process_index);
		get_replies(database, pipes, process_index, process_count, &requests_over, &private_entry, &wait_list, remember_set, &tv, maxfd);	
	}
	// Print a nice green exit message
	fprintf(stderr, "\033[32mProcess %d simulation is over\033[0m\n", process_index);
	// Clean up nicely :)
	for(int j = 0; j < pipe_count; j++) {
		if((j % process_count) == process_index) {
			close(pipes[2 * j + PIPE_WRITE]);
		} else {
			close(pipes[2 * j + PIPE_READ]);
		}
		if(!(j >= lower_bound && j < upper_bound)) {
			close(pipes[2 * j + PIPE_WRITE]);
		}

	}
}

// This is a program for simulating the Lamport protocol for 3-10 processes which communicate using pipes.
// The critical section is access to the shared memory which contains a linked list of structs containing information about the process using the Lamport protocol.
// The information includes: the process id, the process logical clock, the process critical section entry count
int main(int argc, char **argv) {
	// Number of processes in simulation
	int process_count = extract_count(argc, argv);
	// Number of pipes used in simulation (should be equal to 2 * number_of_connections_in_a_fully_connected_graph)
	int pipe_count = process_count * (process_count);
	// Database - is a linked list
	db_entry *database;
	// Contains all of the pipe file descriptors	
	int pipes[pipe_count][2];
	// Current forked process PID
	pid_t process_id;
	int lower_bound, upper_bound;

	// Initialize shared memory
	database = (db_entry *) mmap(NULL, process_count * sizeof(db_entry), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

	// Initialize entry information
	init_entries(database, process_count);

	//Create pipes
	for(int i = 0; i < pipe_count; i++) {
		if(pipe(&(pipes[i][0])) == -1) {
			fprintf(stderr, "Could not create pipe for a process. Exiting.\n");
			exit(1);
		}
	}

	// Create child processes
	for(int i = 0; i < process_count; i++) {
		process_id = fork();
		switch (process_id) {
			// Shouldn't happen
			case 1:
				fprintf(stderr, "Could not create child. Exiting.\n");
				exit(1);
			//Close writing pipe end for other process pipes.
			//Close own reading pipe end.
			case 0:
				sleep(1);
				lower_bound = i * process_count;
				upper_bound  = (i + 1) * process_count;
				for(int j = 0; j < pipe_count; j++) {
					if((j % process_count) == i) {
						close(pipes[j][PIPE_READ]);
					} else {
						close(pipes[j][PIPE_WRITE]);
					}
					if(!(j >= lower_bound && j < upper_bound)) {
						close(pipes[j][PIPE_READ]);
					}

				}
				run_simulation(database, &(pipes[0][0]), i, process_count);
				exit(0);
			// Sets the process ID (hopefully) before the simulation starts
			default:
				database[i].process_id = process_id;
		}

	}

	// Clean up nicely :)
	for(int i = 0; i < pipe_count; i++) {
		wait(NULL);
		close(pipes[i][PIPE_READ]);
		close(pipes[i][PIPE_WRITE]);
	}
	if(munmap(database, process_count * sizeof(db_entry)) != 0){
		printf("Unmapping failed.\n");
		return 1;
	}
	printf("Done.\n");
	return 0;
}
