#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <signal.h>

// Number of arguments this program expects
#define ARG_COUNT 1
// Minimal number of cars crossing the bridge
#define CAR_MIN 5
// Maximum number of cars crossing the bridge
#define CAR_MAX 100
// Maximum size of registration label
#define REG_SIZE 10

#define LEFT "1"
#define RIGHT "2"

// Message structure
typedef struct my_msgbuf {
	long mtype;
	char mtext[200];
} mbuf;

// Creates a registration at given index - Registration value is the string value of given car_index
char *create_registration(char **registrations, int car_index) {
	char *new_reg = registrations[car_index];
	int enough;
	if((enough = snprintf(new_reg, REG_SIZE, "%d", car_index)) > (REG_SIZE - 1)) {
		printf("Need %d bytes, but had %d\n", enough + 1, REG_SIZE);
	}
	return new_reg;
}

// Clean up nicely :)
void cleanup(char **registrations, short *directions, int car_count, int msqid) {
	for(int i = 0; i < car_count; i++) {
		wait(NULL);
	}
	for(int i = 0; i <  car_count; i++) {
		free(registrations[i]);
	}
	free(registrations);
	free(directions);

	if (msgctl(msqid, IPC_RMID, NULL) == -1) {
		perror("Could not remove message queue. Exiting... \n");
		exit(1);
	}
	exit(0);
}

// Extracts number of cars from argv and fails if something smells fishy
int extract_car_count(int argc, char **argv) {
	int car_count;
	if (argc == (ARG_COUNT + 1)) {
		car_count = atoi(argv[1]);
		if(car_count < CAR_MIN) {
			car_count = CAR_MIN;
		} else if(car_count > CAR_MAX) {
			car_count = CAR_MAX;
		}

	} else {
		fprintf(stderr, "This program takes one and only one argument - the number of cars being simulated on the bridge..\n");
		fprintf(stderr, "Each car is implemented as a process.\n");
		exit(1);
	}
	return car_count;
}

// Allocates memory for all of the variables used in main - Fails if something smells fishy
void allocate_mem(char ***registrations, short **directions, int car_count) {
	if((*registrations = malloc(car_count * sizeof(char *))) == NULL) {
		fprintf(stderr, "Could not allocate registration memory!\n");
		exit(1);
	}
	if((*directions = malloc(car_count * sizeof(short))) == NULL) {
		fprintf(stderr, "Could not allocate direction memory!\n");
		exit(1);
	}
	for(int i = 0; i < car_count; i++) {
		if(((*registrations)[i] = malloc(REG_SIZE * sizeof(char))) == NULL) {
			fprintf(stderr, "Could not allocate registration memory!\n");
			exit(1);
		}
	}
}

// Allows interruption of wait function and for the process to cleanup after itself 
void sig_handle(int signo) {}

int main(int argc, char **argv) {
	// Number of cars trying to cross the bridge
	int car_count = extract_car_count(argc, argv);
	// Directions of cars crossing the bridge. Can only be one of {0, 1} (0 is LEFT, 1 is RIGHT)
	short *directions;
	// Car registrations - string values of car indices
	char **registrations;
	// Car direction string - used as argument for calling car processes
	char direction_str[2];
	// ID of queue fetched with the user ID key
	int msqid;
	// Argument passed to the semaphore program
	char car_count_arg[10];

	// Allocates resource memory
	allocate_mem(&registrations, &directions, car_count);
	// Initialize rand seed
	srand((unsigned) time(NULL));

	snprintf(car_count_arg, 10, "%d", car_count);

	// Setup signal handle BEFORE requesting queue
	signal(SIGINT, sig_handle);
	// Try to create queue, exit on fail
	if ((msqid = msgget(getuid(), 0600 | IPC_CREAT)) == -1) {
		perror("Could not initialize queue. Exiting... \n");
		exit(1);
	}

	// Create semaphore process - exit on fail
	switch(fork()) {
		case -1: 
			fprintf(stderr, "Could not create new SEMAFOR process.\n");
				exit(1);
		case 0:
			// Execute semaphore program with child thread
			execl("./semafor", car_count_arg, NULL);
	}

	// Create car processes with random directions - exit on fail
	for(int i = 0; i < car_count; i++) {
		directions[i] =  rand() % 2;
		switch (fork()) {
			case -1:
				printf("Could not create new CAR process.\n");
				exit(1);
			case 0:
				create_registration(registrations, i);
				strcpy(direction_str, directions[i] == 0 ? LEFT : RIGHT);
				// Execute car program with child thread
				execl("./car", registrations[i], direction_str, NULL);
				exit(0);
		}

	}
	
	// Cleanup nicely :)
	cleanup(registrations, directions, car_count, msqid);	
	return 0;
}
