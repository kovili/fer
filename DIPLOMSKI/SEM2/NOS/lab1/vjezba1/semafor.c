#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <signal.h>
#include <stdbool.h>

// Number of arguments program expects
#define ARG_COUNT 2

// Minimum wait time before allowing cars from one direction to cross
#define WAIT_MIN 500
// Maximum wait time before allowing cars from one direction to cross
#define WAIT_MAX 1000

// Minimal time needed for a car to cross the bridge
#define CROSS_MIN 1000
// Maximal time needed for a car to cross the bridge
#define CROSS_MAX 3000

// A milisecond in microseconds
#define MILI_TO_MICRO 1000

// A second in miliseconds
#define SEC_TO_MILI 1000

// A second in microseconds
#define SEC_TO_MICRO 1000000

// Maximum cars from one direction allowed to cross bridge in one go
#define MAX_CARS 3

// The direction LEFT
#define LEFT 1
// The direction RIGHT
#define RIGHT 2
// Mtype offset needed to skip the mtypes 0 (used for recieving any type of message), 1 (used for cars going LEFT) and 2 (used for cars going RIGHT)
#define OFFSET 3

// Size of buffer for writing messages
#define BUFF_SIZE 200

// Flag that keeps track the SIGALRM was triggered - Not absolutely sure if it is needed 
bool alarm_triggered;

// Message struct
typedef struct my_msgbuf {
	long mtype;
	char mtext[BUFF_SIZE];
} usrmsgbuf;

// Structure for keeping track of requests incoming via the message queue
typedef struct request {
	int car_reg;
	struct request *next;
} request;

// List of requests (implemented as a queue)
typedef struct request_list {
	request *first;
	request *last;
} request_list;

// Creates a new request instance and populates it with the car registration data
request *create_request(int car_reg) {
	request *new_req = (request *) malloc(sizeof(request));
	new_req->car_reg = car_reg;
	new_req->next = NULL;
	return new_req;
}

// Adds a request to the queue
bool add_request(request_list *list, request *req) {
	// EDGE CASE - Null pointer added as argument
	if(list == NULL) {
		fprintf(stderr, "Cannot insert into NULL list!\n");
		return false;
	} 
	// EDGE CASE - Null pointer added as argument
	if(req == NULL) {
		fprintf(stderr, "Cannot insert NULL into list!\n");
		return false;
	}

	// Is the list empty?
	if(list->first == NULL) {
		list->first = req;
		list->last = req;
	} else {
		list->last->next = req;
		list->last = req;
	}
	// Make sure no funny bussiness is going on
	req->next = NULL;
	return true;
}

// Removest the first request from the queue
request *remove_request(request_list *list) {
	request *first = list->first;

	if(first != NULL) {
		list->first = first->next;
		list->last = first->next == NULL ?  NULL : list->last;
		// Make sure no funny bussiness is going on
		first->next = NULL;
	}
	return first;

}

// Helper function for manipulating the itimerval struct. Zeros the timer
void reset_timer(struct itimerval *timer) {	
	timer->it_interval.tv_sec = 0;
	timer->it_interval.tv_usec = 0;
	timer->it_value.tv_sec = 0;
	timer->it_value.tv_sec = 0;
}


// SIGALRM signal handler
void signal_handle(int signo) {
	if(signo == SIGALRM) {
		alarm_triggered = true;
	}
}

void init_queue(request_list *list) {
	list->first = NULL;
	list->last = NULL;
}

// Lets up to MAX_CARS pass the critical section
void green_light_cars(request_list *list, int *car_count, short direction, int msqid) {
	request *curr_car;
	request_list remember, check;
	usrmsgbuf msg;
	strcpy(msg.mtext, "Prijeđi");
	long sleep_time;
	init_queue(&remember);
	init_queue(&check);

	// Sends a message to up to MAX_CARS telling them to get up to the bridge
	for(int i = 0; i < MAX_CARS; i++) {
		curr_car = remove_request(list);
		if(curr_car == NULL) {
			break;
		} else {
			(*car_count)--;
			add_request(&remember, curr_car);
			msg.mtype = OFFSET + curr_car->car_reg;

			if(msgsnd(msqid, (struct msgbuf *) &msg, strlen(msg.mtext) + 1, 0) == -1) {
				fprintf(stderr, "Semaphore could not send PASS message to car %d!\n", curr_car->car_reg);
			}
		}
	}
	
	// Let cars pass for anywhere from CROSS_MIN and CROSS_MAX miliseconds
	sleep_time = (CROSS_MIN + (rand() % (CROSS_MAX - CROSS_MIN))) * MILI_TO_MICRO;
	while(sleep_time - SEC_TO_MICRO >= 0) {
		sleep(1);
		sleep_time = sleep_time - SEC_TO_MICRO;
	}
	if(sleep_time > 0) {
		usleep(sleep_time);
	}

	strcpy(msg.mtext, "Prešao");

	// Sends a message to up to MAX_CARS telling them to get off the bridge
	while((curr_car = remove_request(&remember)) != NULL) {

		msg.mtype = OFFSET + curr_car->car_reg;

		if(msgsnd(msqid, (struct msgbuf *) &msg, strlen(msg.mtext) + 1, 0) == -1) {
			fprintf(stderr, "Semaphore could not send CLEAR message to car %d!\n", curr_car->car_reg);
		}
		add_request(&check, create_request(curr_car->car_reg));
		
	}


	while((curr_car = remove_request(&check)) != NULL) {
		if(msgrcv(msqid, (struct msgbuf *) &msg, sizeof(msg) - sizeof(long), OFFSET + curr_car->car_reg, 0) == -1) {
			perror("Could not recieve message! Stopping semaphore.");
			exit(1);
		}
		free(curr_car);
	}
}

// This is the semaphore program which acts as the central node in a centralized mutex protocol
int main(int argc, char **argv) {
	// Currently chosen direction from which cars can cross
	short direction;
	// Message object
	usrmsgbuf msg;
	// Number of cars in the simulation
	int car_count = atoi(argv[0]);
	// The queue id that is fetched using the key (the user ID)
	int msqid, key = getuid();
	// The time between WAIT_MIN and WAIT_MAX the semaphore will wait for MAX_CARS to come up to the semaphore
	int sleep_milis;
	// The index of the current car coming up to the semaphore
	int car_index;
	// Queues for the left cars and right cars respectively
	request_list left_cars, right_cars;
	// Structure used in the setitimer function to setup an SIGALRM to be triggered after the given time passes
	// The timer variable is used to arm the timer, and the reset variable is used to disarm the timer
	struct itimerval timer, reset;

	// Initializes queue state
	init_queue(&left_cars);
	init_queue(&right_cars);

	// Initializes timer states
	reset_timer(&reset);
	reset_timer(&timer);

	// Produces a random seed for srand
	srand((unsigned long) time(NULL));

	// Determines beginning direction
	direction = (rand() % 2) + 1;

	// Gets the queue with the user ID key and sets the user R/W rights
	if((msqid = msgget(key, 0600)) == -1) {
		fprintf(stderr, "Error SEM: could not get message queue.\n");
		exit(1);
	}

	// Connects the ALARM signal to the signal_handle function
	signal(SIGALRM, signal_handle);

	// Until all the cars cross the bridge
	while(car_count != 0) {
		// Alarm setup
		sleep_milis = WAIT_MIN + (rand() % (WAIT_MAX - WAIT_MIN));
		timer.it_value.tv_sec = sleep_milis / SEC_TO_MILI;
		timer.it_value.tv_usec = (sleep_milis % SEC_TO_MILI) * MILI_TO_MICRO;
		alarm_triggered = false;
		setitimer(ITIMER_REAL, &timer, NULL);

		// Print nicely colored text
		printf("\n\033[1;31m#########\033[0m\033[1;37m%6s%3s\033[0m\033[1;31m###########\033[0m\n\n", direction == LEFT ? "LEFT" : "RIGHT", "");
		for(int i = 0; i < MAX_CARS && !alarm_triggered; i++) {
			// Waits for a car from the given direction to try and cross the bridge
			if(msgrcv(msqid, (struct msgbuf *) &msg, sizeof(msg) - sizeof(long), direction, 0) == -1) {
				if(errno == EINTR) {
					break;
				} else {
					perror("Could not recieve message! Stopping semaphore.");
					exit(1);
				}
			}

			car_index = atoi(msg.mtext);
			// Adds the car request to the queue for bridge crossing
			add_request(direction == LEFT ? &left_cars : &right_cars , create_request(car_index));

		}
		// Resets timer
		setitimer(ITIMER_REAL, &reset, NULL);
		// Print nicely colored text
		printf("\n\033[5;32m#########\033[0m\033[1;37m%6s%3s\033[0m\033[5;32m###########\033[0m\n\n", direction == LEFT ? "LEFT" : "RIGHT", "");
		green_light_cars(direction == LEFT ? &left_cars : &right_cars, &car_count, direction, msqid);
		// Flip direction
		direction = direction == LEFT ? RIGHT : LEFT;
	}

	// Pray that you reach the end
	return 0;
}

