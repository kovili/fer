#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/wait.h>
#include <signal.h>

// Number of args this program takes
#define ARG_COUNT 2

// Maximum sleep time
#define MILI_MAX 2000
// 1 milisecond
#define MILI_STEP 0.001
// Minimum sleep time
#define MILI_MIN 100
// Microseconds in milisecond
#define MICROS_IN_MILI 1000
// Microseconds in milisecond
#define MICROS_IN_SEC 1000000

// Minimum time needed to cross bridge
#define CROSS_MIN 1000
// Maximum time needed to cross bridge
#define CROSS_MAX 3000


// Macro for the LEFT direction
#define LEFT 1
// Macro for the RIGHT direction
#define RIGHT 2
// Direction offset
#define OFFSET 3

// Size of buffer for messages
#define BUFF_SIZE 200

// Message data structure
typedef struct my_msgbuf {
	// Message type
	long mtype;
	// Message buffer
	char mtext[BUFF_SIZE];
} usrmsgbuf;

// This program simulates a car that tries to cross a narrow bridge (AKA a thread trying to enter a critical section) in a distributed system
int main(int argc, char **argv) {
	// Car registration
	char *registration;
	// Car direction - 1 for RIGHT, 0 for LEFT
	short direction;
	// Car index - same as car registration, but int
	int car_index; 
	// Message queue key - equals user ID
	int key = getuid();
	// Message queue ID
	int msqid;
	// Sleep time before car requests bridge crossing
	long sleep_time;
	// Message object
	usrmsgbuf msg;

	// Terminate if incorrect number of arguments
	if(argc != ARG_COUNT) {
		fprintf(stderr, "Takes 2 arguments:\n");
		fprintf(stderr, "\t1) Registration value\n");
		fprintf(stderr, "\t2) Direction (either 1 for RIGHT or 0 for LEFT\n");
		exit(2);
	}

	// Define values of variables
	registration = argv[0];
	direction = atoi(argv[1]);
	car_index = atoi(registration);
	msg.mtype = direction;
	snprintf(msg.mtext, BUFF_SIZE, "%s", registration);


	// Sleep before requesting bridge crossing
	srand((unsigned) time(NULL) + car_index * 17);
	sleep_time = (MILI_MIN + (rand() % (MILI_MAX - MILI_MIN))) * MICROS_IN_MILI;
	while(sleep_time - MICROS_IN_SEC >= 0) {
		sleep(1);
		sleep_time -= MICROS_IN_SEC;
	}
	if(sleep_time > 0) {
		usleep(sleep_time);
	}
	
	
	// Try to get message queue - exit if failed
	if ((msqid = msgget(key, 0600)) == -1) {
		fprintf(stderr, "Error: Could not get message queue.\n");
		exit(1);
	}


	// Try to send crossing request - exit if failed
	if(msgsnd(msqid, (struct msgbuf *) &msg, strlen(msg.mtext) + 1, 0) == -1) {
		perror("Could not send message! Crashing car off the bridge.");
		exit(1);
	}

	printf("Automobil %s (%s) čeka na prelazak preko mosta\n", registration, direction == LEFT ? "LIJEVO" : "DESNO");

	// Wait until answer message recieved - exit if failed
	if(msgrcv(msqid, (struct msgbuf *) &msg, sizeof(msg) - sizeof(long), OFFSET + car_index, 0) == -1) {
		perror("Could not recieve message! Crashing car off the bridge.");
		exit(1);
	}

	// Check if message is correct and cross bridge if it is, else exit
	if(strcmp(msg.mtext, "Prijeđi") == 0) {
		printf("\033[1;34mAutomobil %s (%s) se popeo na most.\033[0m\n", registration, direction == LEFT ? "LIJEVO" : "DESNO");
	} else {
		fprintf(stderr, "Unknown message: '%s' recieved. Expected 'Prijeđi'. Crashing car off the bridge.", msg.mtext); 
	}
	// Wait until CS section over message recieved - exit if failed
	if(msgrcv(msqid, (struct msgbuf *) &msg, sizeof(msg) - sizeof(long), OFFSET + car_index, 0) == -1) {
		perror("Could not recieve message! Crashing car off the bridge.");
		exit(1);
	}

	// Check if message is correct and finish crossing bridge if it is, else exit
	if(strcmp(msg.mtext, "Prešao") == 0) {
		printf("\033[1;33mAutomobil %d (%s) je prešao most.\033[0m\n", car_index, direction == LEFT ? "LIJEVO" : "DESNO");
	} else {
		fprintf(stderr, "Unknown message: '%s' recieved. Expected 'Prešao'. Crashing car off the bridge.", msg.mtext);
		exit(1);
	}

	msg.mtype = OFFSET + car_index;
	snprintf(msg.mtext, BUFF_SIZE, "%s", "DONE");

	// Send DONE message - not sure if needed
	if(msgsnd(msqid, (struct msgbuf *) &msg, strlen(msg.mtext) + 1, 0) == -1) {
		perror("Could not send message! Crashing car off the bridge.");
		exit(1);
	}

	return 0;
}

