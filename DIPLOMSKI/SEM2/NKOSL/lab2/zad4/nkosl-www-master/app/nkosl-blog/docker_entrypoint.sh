#!/bin/bash
set -euo pipefail


flask db upgrade

until PGPASSWORD=$DB_PASSWORD psql -h "$DB_HOST:$DB_PORT" -U $DB_USER -c '\q'; do
>&2 echo "Postgres is unavailable - sleeping"
sleep 1
done

>&2 echo "Postgres is up - executing command"
exec $cmd


gunicorn -b 0.0.0.0:80 app:nkosl_app
