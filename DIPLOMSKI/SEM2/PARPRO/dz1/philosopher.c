#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

// Maximal time in miliseconds a philosopher will think
#define MAX_THINK_MILI 4000
// Minimal time in miliseconds a philosopher will think
#define MIN_THINK_MILI 1000
// A second, in miliseconds
#define SEC_IN_MILI 1000
// A milisecond, in microseconds
#define MILI_IN_MICRO 1000
// The general-use-buffer buffer size
#define BUF_SIZE 200
// One of a kind fork request tag
#define FORK_OVER_THE_FORK_TAG 1
// One of a kind fork reply tag
#define FORKING_OVER_THE_FORK_TAG 2
// A kind request
#define FORK_RQST_MSG "GIVE ME YOUR FORK"
// A kind answer
#define FORK_RPLY_MSG "OKAY"

// I should remember this
int phil_index;
// I should remember this
int phil_cnt;

// Guys, I heard there's more forks at the Mexican place...
void signal_handle(int signo) {
	printf("Philosopher %d decided to leave the table since the restaurant over the street has more than %d forks...\n", phil_index, phil_cnt);
	MPI_Finalize();
	exit(0);
}

// Heighten philosopher frequency so he can be distinguished from the others.
void input_tabs(char *buf, int philosopher_index) {
	for(int i = 0; i < philosopher_index; i++) {
		buf[i] = '\t';
	}
	buf[philosopher_index] = '\0';
}

// Checks if there are any messages for current philosopher
bool check_mailbox(MPI_Status *status) {
	int flag;
	// Listen
	if(MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, status) != MPI_SUCCESS) {
		perror("Error while probing.");
	}

	return flag ? true : false;
}

// Get a message from one of your neighbors
bool get_message(int philosopher_cnt, int philosopher_index, char *buf, MPI_Status *status) {
	// What number is my left neighbor
	int left_neighbor_index = philosopher_index == 0 ? philosopher_cnt - 1 :  philosopher_index - 1;
	// What number is my right neighbor
	int right_neighbor_index = philosopher_index == (philosopher_cnt - 1) ? 0 : philosopher_index + 1;

	// Do I even know this guy?
	if(status->MPI_SOURCE != left_neighbor_index && status->MPI_SOURCE != right_neighbor_index) {
		fprintf(stderr, "Message to philosopher %d from unexpected source %d\n", philosopher_index, status->MPI_SOURCE);
		exit(1);
		return false;
	}
	
	// I don't understand what he is asking me
	if(status->MPI_TAG != FORK_OVER_THE_FORK_TAG && status->MPI_TAG != FORKING_OVER_THE_FORK_TAG) {
		fprintf(stderr, "Message to philosopher %d with unexpected tag %d\n", philosopher_index, status->MPI_TAG);
		exit(1);
		return false;
	 }

	// Listen
	if(MPI_Recv(buf, BUF_SIZE, MPI_CHAR, status->MPI_SOURCE, status->MPI_TAG, MPI_COMM_WORLD, status) != MPI_SUCCESS) {
		perror("Error while recieving message.");
		return false;
	}
	return true;
}

// Answer message after recieving it.
// A philosopher could either answer with a FORK_RPLY_MSG or exclamate 'Hooray' for recieving a fork. 
void answer_message(int philosopher_cnt, int philosopher_index, bool *left_fork, bool *right_fork, bool *left_dirty, bool *right_dirty, char *buf, MPI_Status *status, bool *left_pending, bool *right_pending) {
	int left_neighbor_index = philosopher_index == 0 ? philosopher_cnt - 1 :  philosopher_index - 1;
	int right_neighbor_index = philosopher_index == (philosopher_cnt - 1) ? 0 : philosopher_index + 1;
	bool left_request = status->MPI_SOURCE == left_neighbor_index ? true : false;
	bool right_request = status->MPI_SOURCE == right_neighbor_index ? true : false;

	// RECIEVED REQUEST - answer or just remember
	if(strcmp(buf, FORK_RQST_MSG) == 0) {

		if(left_request && *left_fork) {
			// This should be reachable only when the philosopher is not thinking...
			if(!(*left_dirty)) {
				*left_pending = true;
				return;
			}
			// Cleaning my fork
			*left_dirty = false; 
			*left_fork = false;
		} else if(right_request && *right_fork){
			// This should be reachable only when the philosopher is not thinking...
			if(!(*right_dirty)) {
				*right_pending = true;
				return;
			}
			// Cleaning my fork
			*right_dirty = false;
			*right_fork = false;
		} else {
			fprintf(stderr, "MPI_Status for philosopher %d  has nonsensical source: %d\n", philosopher_index, status->MPI_SOURCE);
		}

		// Pass it to the neighbor who asked for a fork
		if(MPI_Send(FORK_RPLY_MSG, strlen(FORK_RPLY_MSG) + 1, MPI_CHAR, status->MPI_SOURCE, FORKING_OVER_THE_FORK_TAG, MPI_COMM_WORLD) != MPI_SUCCESS) {
			perror("Error while sending fork request.");
			return;
		}

	// RECIEVED ANSWER - update fork statuses
	} else if(strcmp(buf, FORK_RPLY_MSG) == 0) {

		// A neighbor (left or right) gave me a clean fork. Hooray!
		if(left_request && !(*left_fork)) {
			*left_dirty = false;
			*left_fork = true;
		} else {
			*right_dirty = false;
			*right_fork = true;
		}
	} else {
		fprintf(stderr, "Unexpected message: %s\n", buf);
	}
}

// Makes the philosopher think anywhere in the range of (MIN_THINK_MILI, MAX_THINK_MILI) miliseconds.
// The philosopher also checks for messages after every second and at the end of think-time.
void think(int philosopher_cnt, int philosopher_index, bool *left_fork, bool *right_fork, bool *left_dirty, bool *right_dirty) {
	// Think time in miliseconds
	int mili_think = (MIN_THINK_MILI + (rand() % (MAX_THINK_MILI - MIN_THINK_MILI)));
	// Buffer for various uses :)
	char buf[BUF_SIZE];
	// MPI Status struct - useful for knowing where a message came from
	MPI_Status status;

	input_tabs(buf, philosopher_index);
	printf("%sPhilosopher %d is thinking.\n", buf, philosopher_index);

	// Think for a second, while you can.
	while(mili_think >= SEC_IN_MILI) {
		sleep(1);
		// Is somebody talking to me?
		while(check_mailbox(&status)) {
			// I should answer
			get_message(philosopher_cnt, philosopher_index, buf, &status);
			answer_message(philosopher_cnt, philosopher_index, left_fork, right_fork, left_dirty, right_dirty, buf, &status, NULL, NULL);
		}
		mili_think -= SEC_IN_MILI;
	}
	// Were you thinking about anything else?
	if(mili_think > 0) {
		usleep(mili_think * MILI_IN_MICRO);
		// Is somebody talking to me?
		while(check_mailbox(&status)) {
			// I should answer
			get_message(philosopher_cnt, philosopher_index, buf, &status);
			answer_message(philosopher_cnt, philosopher_index, left_fork, right_fork, left_dirty, right_dirty, buf, &status, NULL, NULL);
		}
	}
}


// Answer all of the pending requests. Also reset pending request variables so they are false.
void answer_pending(int philosopher_cnt, int philosopher_index, bool *left_fork, bool *right_fork, bool *left_dirty, bool *right_dirty, bool *left_pending, bool *right_pending) {
	int left_neighbor_index = philosopher_index == 0 ? philosopher_cnt - 1 :  philosopher_index - 1;
	int right_neighbor_index = philosopher_index == (philosopher_cnt - 1) ? 0 : philosopher_index + 1;


	// Anybody waiting on the left side?
	if(*left_pending) {
		// Wait! Let me clean my fork.
		*left_dirty = false;
		// That was quick! Here's my fork.
		if(MPI_Send(FORK_RPLY_MSG, strlen(FORK_RPLY_MSG) + 1, MPI_CHAR, left_neighbor_index, FORKING_OVER_THE_FORK_TAG, MPI_COMM_WORLD) != MPI_SUCCESS) {
			perror("Error while handling reply.");
			return;
		}
		// My left hand is empty
		*left_fork = false;
	}
	// Anybody waiting on the right side?
	if(*right_pending) {
		// Wait! Let me clean my fork.
		*right_dirty = false;
		// That was quick! Here's my fork.
		if(MPI_Send(FORK_RPLY_MSG, strlen(FORK_RPLY_MSG) + 1, MPI_CHAR, right_neighbor_index, FORKING_OVER_THE_FORK_TAG, MPI_COMM_WORLD) != MPI_SUCCESS) {
			perror("Error while handling reply.");
			return;
		}
		*right_fork = false;
	}

	*left_pending = false;
	*right_pending = false;
}

// Request fork from either left or right neighbor - determined by the from_left variable
bool request_fork(int philosopher_cnt, int philosopher_index, bool from_left) {
	int fork_location;

	if(from_left) {
		fork_location = philosopher_index == 0 ? philosopher_cnt - 1 : philosopher_index - 1;
	} else {
		fork_location = philosopher_index == (philosopher_cnt - 1) ? 0 : philosopher_index + 1;
	}

	if(MPI_Send(FORK_RQST_MSG, strlen(FORK_RQST_MSG) + 1, MPI_CHAR, fork_location, FORK_OVER_THE_FORK_TAG, MPI_COMM_WORLD) != MPI_SUCCESS) {
		perror("Error while sending fork request.");
		return false;
	}

	return true;
}


// Philosopher asks others for forks
void get_forks(int philosopher_cnt, int philosopher_index, bool *left_fork, bool *right_fork, bool *sent_left_rqst, bool *sent_right_rqst) {
	int fork_location;
	char buf[BUF_SIZE];

	input_tabs(buf, philosopher_index);

	// If I don't have the left fork and I haven't asked before
	if(!(*left_fork || *sent_left_rqst)) {
		// Ask for left fork
		request_fork(philosopher_cnt, philosopher_index, true);
		*sent_left_rqst = true;
		printf("%s\033[33mPhilosopher %d is requesting LEFT fork. \033[0m\n", buf, philosopher_index);
		return;
	}

	// If I don't have the right fork and I haven't asked before
	if(!(*right_fork || *sent_right_rqst)) {	
		// Ask for right fork
		request_fork(philosopher_cnt, philosopher_index, false);
		*sent_right_rqst = true;
		printf("%s\033[33mPhilosopher %d is requesting RIGHT fork. \033[0m\n", buf, philosopher_index);
		return;
	}
}

// This is where the philosopher goes when he is hungry.
// The philosopher must exclaim to the others that he is eating.
// The philosopher will produce dirty forks by eating.
void eat(int philosopher_cnt, int philosopher_index, bool *left_dirty, bool *right_dirty) {
	char buf[BUF_SIZE];

	input_tabs(buf, philosopher_index);

	printf("%sPhilosopher %d is eating.\n", buf, philosopher_index);

	*left_dirty = true;
	*right_dirty = true;
}

int main(int argc, char** argv) {
	// Number of processes
	int philosopher_cnt;
	// Index recieved from MPI
	int philosopher_index;
	// Fork ownership
	bool left_fork, right_fork;
	// Fork hygiene
	bool left_dirty = true, right_dirty = true;
	// Fork requests
	bool left_rqst = false, right_rqst = false;
	// Edge case - makes sure philosophers don't sent multiple requests to one side
	bool sent_left_rqst = false, sent_right_rqst = false;
	// MPI Status struct - useful for knowing where a message came from
	MPI_Status status;
	// Buffer for various message uses :)
	char buf[BUF_SIZE];

	// Initialize the MPI environment.
	MPI_Init(NULL, NULL);

	// Get number of processes.
	MPI_Comm_size(MPI_COMM_WORLD, &philosopher_cnt);

	// Get the rank of the process.
	MPI_Comm_rank(MPI_COMM_WORLD, &philosopher_index);

	// Starting conditions
	left_fork = philosopher_index == 0 ? true : false;
	right_fork =  (philosopher_index + 1) != philosopher_cnt ? true : false;
	right_fork = philosopher_index == 0 && philosopher_cnt == 1 ? true : right_fork;

	// Setup signal handle
	signal(SIGINT, signal_handle);
	phil_index = philosopher_index;
	phil_cnt = philosopher_cnt;

	// Init random seed
	srand((unsigned int) time(NULL) + philosopher_index * 17);


	while(true) {
		// Hmmmmmmmmmmmm
		think(philosopher_cnt, philosopher_index, &left_fork, &right_fork, &left_dirty, &right_dirty);
		// I don't have enough forks!
		while(!(left_fork && right_fork)) {
			// Can you give me a fork
			get_forks(philosopher_cnt, philosopher_index, &left_fork, &right_fork, &sent_left_rqst, &sent_right_rqst);
			while(true) {
				// Listen
				if(MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status) == MPI_SUCCESS) {
					get_message(philosopher_cnt, philosopher_index, buf, &status);
					answer_message(philosopher_cnt, philosopher_index, &left_fork, &right_fork, &left_dirty, &right_dirty, buf, &status, &left_rqst, &right_rqst);
					break;
				}
			
			}
		}
		sent_left_rqst = false;
		sent_right_rqst = false;
		// Finally, I'm starving!
		eat(philosopher_cnt, philosopher_index, &left_dirty, &right_dirty);
		// Does anybody need a fork?
		answer_pending(philosopher_cnt, philosopher_index, &left_fork, &right_fork, &left_dirty, &right_dirty, &left_rqst, &right_rqst);
	}

	// Finalize the MPI environment.
	MPI_Finalize();
}

