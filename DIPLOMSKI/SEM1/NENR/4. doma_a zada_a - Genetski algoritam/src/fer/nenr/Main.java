package fer.nenr;

import fer.nenr.genetic.algorithm.GenerationalGeneticAlgorithm;
import fer.nenr.genetic.algorithm.IGenericGeneticAlgorithm;
import fer.nenr.genetic.algorithm.SimpleTournamentGeneticAlgorithm;
import fer.nenr.genetic.fitness.MultiParameterFunctionDatabase;
import fer.nenr.genetic.model.Individual;
import fer.nenr.genetic.model.NumericBound;
import fer.nenr.genetic.model.ResultRecord;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String NO_NOISE_DATA_PATH = "./resources/zad4-dataset1.txt";
    private static final String WITH_NOISE_DATA_PATH = "./resources/zad4-dataset2.txt";
    private static final int DEFAULT_ITERATIONS = 10000;
    private static final int DEFAULT_POPULATION_SIZE = 50;
    private static final int SINGLE_ROW_PARAMETER_COUNT = 3;
    private static final double MUTATION_PROBABILITY = 0.05;
    private static final int TASK_4_PARAMS = 5;
    private static final NumericBound TASK_4_BOUND = new NumericBound(-4, 4);

    public static void main(String[] args) throws IOException {
        List<String> measuringResults = Files.readAllLines(Paths.get(WITH_NOISE_DATA_PATH));
        ResultRecord record = new ResultRecord(measuringResults.size(), SINGLE_ROW_PARAMETER_COUNT);
        for(int i = 0, limit = measuringResults.size(); i < limit; i++) {
            String[] parts = measuringResults.get(i).split("\\t");
            record.setXForIndex(i, Double.parseDouble(parts[0]));
            record.setYForIndex(i, Double.parseDouble(parts[1]));
            record.setResultForIndex(i, Double.parseDouble(parts[2]));
        }
        IGenericGeneticAlgorithm algorithm;

        Scanner sc = new Scanner(System.in);
        boolean tournamentFlag = false;
        boolean elitismFlag = false;
        int iterationCount = 0, populationSize = 0;
        double mutationProbability = -1.0;


        System.out.println("Upišite T za Tournament algoritam, a G za generation algoritam.");
        while(true) {

            String in = sc.nextLine();
            if(Character.toLowerCase(in.charAt(0)) == 'g') {
                tournamentFlag = false;
                System.out.println("Upišite T za elitizam, F za pokretanje bez elitizma");
                while(true) {

                    in = sc.nextLine();
                    if(Character.toLowerCase(in.charAt(0)) == 't') {
                        elitismFlag = true;
                        break;
                    } else if(Character.toLowerCase(in.charAt(0)) == 'f') {
                        elitismFlag = false;
                        break;
                    }
                    System.out.println("Pogrešan unos, pokušajte opet.");
                }
                break;
            } else if(Character.toLowerCase(in.charAt(0)) == 't') {
                tournamentFlag = true;
                break;
            }
            System.out.println("Pogrešan unos, pokušajte opet.");
        }

        System.out.println("Odaberite veličinu populacije,");
        while(true) {

            String in = sc.nextLine();
            try {
                populationSize = Integer.parseInt(in);
            } catch (NumberFormatException e) {}
            if(populationSize <= 0) {
                System.out.println("Pogrešan unos, pokušajte opet.");
            } else {
                break;
            }
        }

        System.out.println("Odaberite broj iteracija,");
        while(true) {
            String in = sc.nextLine();
            try {
                iterationCount = Integer.parseInt(in);
            } catch (NumberFormatException e) {}

            if (iterationCount <= 0) {
                System.out.println("Pogrešan unos, pokušajte opet.");
            } else {
                break ;
            }
        }

        System.out.println("Odaberite vjerojatnost mutacije,");
        while(true) {
            String in = sc.nextLine();
            try {
                mutationProbability = Double.parseDouble(in);
            } catch (NumberFormatException e) {
            }

            if (mutationProbability < 0 || mutationProbability > 1) {
                System.out.println("Pogrešan unos, pokušajte opet.");
            } else {
                break;
            }
        }

        algorithm = tournamentFlag ? new SimpleTournamentGeneticAlgorithm(populationSize, iterationCount, TASK_4_PARAMS,
                                                                                    TASK_4_BOUND, mutationProbability)
                                    :
                                     new GenerationalGeneticAlgorithm(populationSize, iterationCount, TASK_4_PARAMS,
                                                                        TASK_4_BOUND, mutationProbability, elitismFlag);

        Individual bestResult = algorithm.geneticAlgorithm(MultiParameterFunctionDatabase.TASK_4_FUNCTION, record);

        System.out.println();
        System.out.println("Najbolji rezultat je: " + bestResult);
    }
}
