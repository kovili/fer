package fer.nenr.genetic.operators.crossover;

import fer.nenr.genetic.model.Individual;

public interface ICrossoverOperator {

    Individual crossover(Individual mother, Individual father);
}
