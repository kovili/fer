package fer.nenr.genetic.model;

public class Parents {
    private Individual mother;
    private Individual father;

    public Parents() {}

    public Parents(Individual mother, Individual father) {
        this.mother = mother;
        this.father = father;
    }

    public void setFather(Individual father) {
        this.father = father;
    }

    public void setMother(Individual mother) {
        this.mother = mother;
    }

    public void addParent(Individual parent) {
        if(mother == null) mother = parent;
        if(father == null) father = parent;
    }

    public Individual getMother() {
        return mother;
    }

    public Individual getFather() {
        return father;
    }

    @Override
    public String toString() {
        return "Parents{" +
                "mother=" + mother +
                ", father=" + father +
                '}';
    }
}
