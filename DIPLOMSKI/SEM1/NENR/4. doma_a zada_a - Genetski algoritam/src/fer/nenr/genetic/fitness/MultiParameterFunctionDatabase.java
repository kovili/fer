package fer.nenr.genetic.fitness;

public class MultiParameterFunctionDatabase {

    //f(x, y)=sin(β0 + β1⋅x) + β2⋅cos(x ⋅ (β3 + y)) ⋅ (1 / (1 + e^((x−β4)^2))
    public static final IMultiParameterFunction TASK_4_FUNCTION =
            (parameters, x, y) -> Math.sin(parameters[0] + parameters[1] * x) +
                                    parameters[2] * Math.cos(x * (parameters[3] + y)) *
                                            (1 / (1 + Math.exp(Math.pow(x - parameters[4], 2))));
}
