package fer.nenr.genetic.model;

import fer.nenr.genetic.fitness.IFitnessFunction;
import fer.nenr.genetic.fitness.IMultiParameterFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.UnaryOperator;

public class Population implements Iterable<Individual> {
    private List<Individual> population;
    private double fitnessSum;


    private Population(int populationSize) {
        population = new ArrayList<>(populationSize);
    }

    public static Population generateRandomPopulationOfSize(int populationSize, int geneticMaterialSize, NumericBound bound) {
        Population randomPopulation = new Population(populationSize);
        for(int i = 0; i < populationSize; i++) {
            randomPopulation.addIndividual(Individual.generateRandomIndividualOfSize(geneticMaterialSize, bound));
        }
        return randomPopulation;
    }

    public static Population populationOf(List<Individual> individuals) {
        Population population = new Population(individuals.size());
        population.population = individuals;
        return population;
    }

    public static Population generateEmptyPopulationOfSize(int populationSize) {
        Population emptyPopulation = new Population(populationSize);
        return emptyPopulation;
    }

    public Individual getIndividual(int index) {
        return population.get(index);
    }

    public Individual removeIndividual(int index) {
        return population.remove(index);
    }

    public void setIndividual(int index, Individual individual) {
        population.set(index, individual);
    }

    public boolean addIndividual(Individual individual) { return population.add(individual); }

    public int getSize() { return population.size(); }

    public void randomizePopulation() { Collections.shuffle(population); }

    public double getFitnessSum() {
        return fitnessSum;
    }

    public void setFitnessSum(double fitnessSum) {
        this.fitnessSum = fitnessSum;
    }

    public Individual getBestIndividual(ResultRecord resultRecord, IMultiParameterFunction goalFunction,
                                        IFitnessFunction ratingFunction, UnaryOperator<Double> resultFunction) {
        fitnessSum = 0;
        Individual bestIndividual = population.get(0);
        double bestResult = bestIndividual.rateIndividual(resultRecord, goalFunction, ratingFunction, resultFunction);
        fitnessSum += bestIndividual.getResultFitness();

        for(int i = 1, limit = population.size(); i < limit; i++) {
            Individual potentialBest = population.get(i);
            double potentialBestResult = potentialBest.rateIndividual(resultRecord, goalFunction, ratingFunction, resultFunction);
            fitnessSum += potentialBest.getResultFitness();
            if(bestResult > potentialBestResult) {
                bestIndividual = potentialBest;
                bestResult = potentialBestResult;
            }

        }
        return bestIndividual;
    }

    public List<Individual> listView(int begin, int end) {
        return population.subList(begin, end);
    }

    @Override
    public Iterator<Individual> iterator() {
        return population.iterator();
    }
}
