package fer.nenr.genetic.model;

public class NumericBound {
    double lowerBound;
    double upperBound;

    public NumericBound(double lowerBound, double upperBound) {
        if(lowerBound > upperBound) {
            this.lowerBound = upperBound;
            this.upperBound = lowerBound;
        } else {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
        }
    }

    public double getLowerBound() {
        return lowerBound;
    }

    public double getUpperBound() {
        return upperBound;
    }

    @Override
    public String toString() {
        return "[" + lowerBound + ", " + upperBound + "]";
    }
}
