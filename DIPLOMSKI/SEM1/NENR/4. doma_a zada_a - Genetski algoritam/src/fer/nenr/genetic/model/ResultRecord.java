package fer.nenr.genetic.model;

import java.util.Arrays;

public class ResultRecord {
    private double [][]resultRecord;

    public ResultRecord(int matrixHeight, int matrixWidth) {
        resultRecord = new double[matrixHeight][matrixWidth];
    }

    public double getXForIndex(int index) { return resultRecord[index][0]; }
    public double getYForIndex(int index) { return resultRecord[index][1]; }
    public double getResultForIndex(int index) { return resultRecord[index][2]; }

    public void setXForIndex(int index, double value) { resultRecord[index][0] = value; }
    public void setYForIndex(int index, double value) { resultRecord[index][1] = value; }
    public void setResultForIndex(int index, double value) { resultRecord[index][2] = value; }

    public int getSingleResultSize() {
        return resultRecord[0].length;
    }

    public int getResultSize() {
        return resultRecord.length;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for(int i = 0; i < resultRecord.length - 1; i++) {
            builder.append(Arrays.toString(resultRecord[i]) + ",\n");
        }
        builder.append(Arrays.toString(resultRecord[resultRecord.length - 1]) + "]");
        return builder.toString();
    }
}
