package fer.nenr.genetic.operators.eliminaton;

import fer.nenr.genetic.model.Individual;

import java.util.List;

public interface IEliminationOperator {

    int eliminate(List<Individual> participants);
}
