package fer.nenr.genetic.operators.selection;

import fer.nenr.genetic.model.Individual;
import fer.nenr.genetic.model.Parents;

public class SelectionOperatorDatabase {

    public static final ISelectionOperator SELECT_PROPORTIONAL = (population) ->  {
        double proportionalSum = population.getFitnessSum();

        Parents parents = new Parents(null, null);

        for(int i = 0; i < 2; i++) {
            double selectedPoint = Math.random() * proportionalSum;
            double currentPoint = 0;
            for(Individual individual : population) {
                currentPoint +=  individual.getResultFitness();
                if(currentPoint >= selectedPoint) {
                    if(i == 0) parents.setMother(individual);
                    if(i == 1) parents.setFather(individual);
                    break;
                }
            }
        }
        return parents;
    };

}
