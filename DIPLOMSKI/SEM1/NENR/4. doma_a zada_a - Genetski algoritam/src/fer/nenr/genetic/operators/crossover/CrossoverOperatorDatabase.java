package fer.nenr.genetic.operators.crossover;

import fer.nenr.genetic.model.Individual;

public class CrossoverOperatorDatabase {

    public static final ICrossoverOperator DISCRETE_RECOMBINATION = (mother, father) -> {
        Individual child = new Individual(mother.getGeneticSize(), mother.getBound());
        for(int i = 0, limit = mother.getGeneticSize(); i < limit; i++) {
            child.setMaterialForIndex(i, Math.random() >= 0.5 ?
                                    mother.getMaterialForIndex(i) : father.getMaterialForIndex(i));
        }
        return child;
    };

    public static final ICrossoverOperator SIMPLE_ARITHMETIC_RECOMBINATION = (mother, father) -> {
        Individual child = mother.likeThis();
        int random = (int) (Math.random() * (mother.getGeneticSize()));
        if(random == mother.getGeneticSize()) random--;
        for(int i = random, limit = mother.getGeneticSize(); i < limit; i++) {
            child.setMaterialForIndex(i, (mother.getMaterialForIndex(i) + father.getMaterialForIndex(i)) / 2);
        }
        return child;
    };

    public static final ICrossoverOperator SINGLE_ARITHMETIC_RECOMBINATION = (mother, father) -> {
        Individual child = mother.likeThis();
        int random = (int) (Math.random() * (mother.getGeneticSize()));
        if(random == mother.getGeneticSize()) random--;
        child.setMaterialForIndex(random, (mother.getMaterialForIndex(random) + father.getMaterialForIndex(random)) / 2);
        return child;
    };

    public static final ICrossoverOperator WHOLE_ARITHMETIC_RECOMBINATION = (mother, father) -> {
        Individual child = new Individual(mother.getGeneticSize(), mother.getBound());
        for(int i = 0, limit = mother.getGeneticSize(); i < limit; i++) {
            child.setMaterialForIndex(i, (mother.getMaterialForIndex(i) + father.getMaterialForIndex(i)) / 2);
        }
        return child;
    };



}
