package fer.nenr.genetic.algorithm;

import fer.nenr.genetic.fitness.IFitnessFunction;
import fer.nenr.genetic.fitness.IMultiParameterFunction;
import fer.nenr.genetic.model.Individual;
import fer.nenr.genetic.model.NumericBound;
import fer.nenr.genetic.model.Population;
import fer.nenr.genetic.model.ResultRecord;

import java.util.function.UnaryOperator;

public interface IGenericGeneticAlgorithm {

    default Individual geneticAlgorithm(IMultiParameterFunction function, ResultRecord resultRecord) {
        Population population = Population.generateRandomPopulationOfSize(getPopulationSize(),
                                                                            getUnknownParameterCount(), getBound());
        double bestResult = Double.MAX_VALUE;
        for(int i = 1, limit = getIterationLimit(); i <= limit; i++) {
            Individual potentialBest = evaluatePopulation(population, resultRecord, function, getResultProcessingFunction());
            population = processPopulation(population, getMutationProbability());
            population.randomizePopulation();
            if(potentialBest.getFitness() < bestResult) {
                bestResult = potentialBest.getFitness();
                System.out.println("New best result for generation " + i + " is " + bestResult);
                System.out.println("with parameters " + potentialBest);
            }
        }
        return population.getBestIndividual(resultRecord, function, getFitnessFunction(), getResultProcessingFunction());
    }

    IFitnessFunction getFitnessFunction();

    UnaryOperator<Double> getResultProcessingFunction();

    Population processPopulation(Population population, double mutationProbability);

    Individual evaluatePopulation(Population population, ResultRecord resultRecord,
                              IMultiParameterFunction goalFunction, UnaryOperator<Double> resultFunction);

    int getIterationLimit();
    void setIterationLimit(int iterationLimit);

    int getPopulationSize();
    void setPopulationSize(int populationSize);

    int getUnknownParameterCount();
    void setUnknownParameterCount(int unknownParameterCount);

    NumericBound getBound();
    void setBound(NumericBound bound);

    double getMutationProbability();

}
