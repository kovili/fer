package fer.nenr.genetic.operators.eliminaton;

import fer.nenr.genetic.model.Individual;

public class EliminationOperatorDatabase {

    public static final IEliminationOperator SIMPLE_TOURNAMENT_ELIMINATION = (participants) -> {
        double worstScore = -Double.MAX_VALUE;
        int worstIndex = 0, index = 0;

        for(Individual individual : participants) {
            double individualFitness = individual.getResultFitness();
            if(individualFitness > worstScore) {
                worstScore = individualFitness;
                worstIndex = index;
            }
            index++;
        }
        participants.set(worstIndex, null);
        return worstIndex;
    };
}
