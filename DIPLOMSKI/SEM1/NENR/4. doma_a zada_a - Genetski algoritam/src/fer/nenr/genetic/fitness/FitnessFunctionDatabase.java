package fer.nenr.genetic.fitness;

public class FitnessFunctionDatabase {

    public static final IFitnessFunction MEAN_SQUARE_LOSS =
            (predictedValue, actualValue) -> Math.pow(predictedValue - actualValue, 2);
}
