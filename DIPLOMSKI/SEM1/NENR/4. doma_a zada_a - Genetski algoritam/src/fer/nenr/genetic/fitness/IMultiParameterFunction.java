package fer.nenr.genetic.fitness;

public interface IMultiParameterFunction {

    double evaluateExpression(double[] parameters, double x, double y);

}
