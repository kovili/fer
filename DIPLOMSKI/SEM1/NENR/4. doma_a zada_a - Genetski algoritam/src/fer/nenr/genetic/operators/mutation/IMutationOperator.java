package fer.nenr.genetic.operators.mutation;

import fer.nenr.genetic.model.Individual;

public interface IMutationOperator {

    Individual mutate(Individual individual);
}
