package fer.nenr.genetic.algorithm;

import fer.nenr.genetic.fitness.FitnessFunctionDatabase;
import fer.nenr.genetic.fitness.IFitnessFunction;
import fer.nenr.genetic.fitness.IMultiParameterFunction;
import fer.nenr.genetic.model.*;
import fer.nenr.genetic.operators.crossover.CrossoverOperatorDatabase;
import fer.nenr.genetic.operators.mutation.MutationOperatorDatabase;
import fer.nenr.genetic.operators.selection.SelectionOperatorDatabase;

import java.util.function.UnaryOperator;

public class GenerationalGeneticAlgorithm extends SkeletonGenericGeneticAlgorithm {
    private boolean elitism;
    private Individual bestIndividual;

    public GenerationalGeneticAlgorithm(int populationSize, int iterationLimit,
                                        int unknownParameterCount, NumericBound bound, double mutationProbability, boolean elitism) {
        super(populationSize, iterationLimit, unknownParameterCount, bound, mutationProbability);
        this.elitism = elitism;
        bestIndividual = this.elitism ?
                Individual.generateRandomIndividualOfSize(getUnknownParameterCount(), getBound()) : null;
    }

    @Override
    public IFitnessFunction getFitnessFunction() { return FitnessFunctionDatabase.MEAN_SQUARE_LOSS; }

    @Override
    public Population processPopulation(Population population, double mutationProbability) {
        Population nextGeneration =  Population.generateEmptyPopulationOfSize(population.getSize());

        for(int i = 0, limit = population.getSize(); i < limit; i++) {
            Parents parents = SelectionOperatorDatabase.SELECT_PROPORTIONAL.selectPopulation(population);
            nextGeneration.addIndividual(
                    CrossoverOperatorDatabase.DISCRETE_RECOMBINATION.crossover(parents.getMother(), parents.getFather()));
            if(Math.random() <= mutationProbability) {
                MutationOperatorDatabase.SIMPLE_MUTATION.mutate(nextGeneration.getIndividual(i));
            }
        }
        if(elitism) nextGeneration.setIndividual(0, bestIndividual);
        return nextGeneration;
    }

    @Override
    public Individual evaluatePopulation(Population population, ResultRecord resultRecord,
                                     IMultiParameterFunction goalFunction, UnaryOperator<Double> resultFunction) {
        Individual result = population.getBestIndividual(resultRecord, goalFunction, getFitnessFunction(), getResultProcessingFunction());
        if(elitism) bestIndividual = result;
        return result;
    }

    @Override
    public UnaryOperator<Double> getResultProcessingFunction() { return (result) -> 1 / result; }

}
