package fer.nenr.genetic.algorithm;

import fer.nenr.genetic.fitness.FitnessFunctionDatabase;
import fer.nenr.genetic.fitness.IFitnessFunction;
import fer.nenr.genetic.model.*;
import fer.nenr.genetic.operators.crossover.CrossoverOperatorDatabase;
import fer.nenr.genetic.operators.eliminaton.EliminationOperatorDatabase;
import fer.nenr.genetic.operators.mutation.MutationOperatorDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class SimpleTournamentGeneticAlgorithm extends SkeletonGenericGeneticAlgorithm {
    private final int TOURNAMENT_SIZE = 3;

    public SimpleTournamentGeneticAlgorithm(int populationSize, int iterationLimit,
                                            int unknownParameterCount, NumericBound bound, double mutationProbability) {
        super(populationSize, iterationLimit, unknownParameterCount, bound, mutationProbability);
    }

    @Override
    public IFitnessFunction getFitnessFunction() { return FitnessFunctionDatabase.MEAN_SQUARE_LOSS; }

    @Override
    public UnaryOperator<Double> getResultProcessingFunction() { return result -> result; }

    @Override
    public Population processPopulation(Population population, double mutationProbability) {
        List<Individual> participants = new ArrayList<>();
        List<Individual> newPopulation = new ArrayList<>();
        int limit = population.getSize();
        for(int i = 0; i < limit; i++) {
            participants.add(population.removeIndividual((int) (Math.random() * population.getSize())));
            if(i % TOURNAMENT_SIZE == TOURNAMENT_SIZE - 1) {
                Parents parents = new Parents();
                EliminationOperatorDatabase.SIMPLE_TOURNAMENT_ELIMINATION.eliminate(participants);
                for(int j = 0; j < TOURNAMENT_SIZE; j++) {
                    if(participants.get(j) != null) parents.addParent(participants.get(j));
                }
                Individual child = CrossoverOperatorDatabase.DISCRETE_RECOMBINATION.crossover(
                                                            parents.getMother(), parents.getFather());
                if(Math.random() <= mutationProbability) {
                    MutationOperatorDatabase.SIMPLE_MUTATION.mutate(child);
                }
                newPopulation.add(parents.getMother());
                newPopulation.add(parents.getFather());
                newPopulation.add(child);
                participants = new ArrayList<>();
            }
        }
        participants.forEach(participant -> newPopulation.add(participant));
        return Population.populationOf(newPopulation);
    }

}
