package fer.nenr.genetic.model;

import fer.nenr.genetic.fitness.IFitnessFunction;
import fer.nenr.genetic.fitness.IMultiParameterFunction;
import java.util.function.UnaryOperator;

public class Individual {
    private double []geneticMaterial;
    private NumericBound bound;
    private Double fitness = null;
    private Double processedFitness;

    public Individual(int geneticMaterialSize, NumericBound bound) {
        geneticMaterial = new double[geneticMaterialSize];
        this.bound = bound;
    }

    public static Individual generateRandomIndividualOfSize(int geneticMaterialSize, NumericBound bound) {
        Individual randomIndividual = new Individual(geneticMaterialSize, bound);
        for(int i = 0; i < geneticMaterialSize; i++) {
            randomIndividual.geneticMaterial[i] = (Math.random() * (bound.upperBound - bound.lowerBound) + bound.lowerBound);
        }
        return randomIndividual;
    }

    public int getGeneticSize() {
        return geneticMaterial.length;
    }

    public double getMaterialForIndex(int index) {
        return geneticMaterial[index];
    }

    public double setMaterialForIndex(int index, double value) {
        if(value < bound.lowerBound || value > bound.upperBound) {
            throw new IllegalArgumentException("Given value must be in range of the bound: " + bound);
        }
        return geneticMaterial[index] = value;
    }

    public Individual likeThis() {
        Individual individual = new Individual(getGeneticSize(), bound);
        individual.geneticMaterial = geneticMaterial.clone();
        return individual;
    }

    //RETURNS SUMMED MEAN SQUARE ERROR
    public double rateIndividual(ResultRecord resultRecord, IMultiParameterFunction goalFunction,
                                 IFitnessFunction ratingFunction, UnaryOperator<Double> resultFunction) {
        if(fitness != null) return fitness;
        double sum = 0;
        for(int i = 0, limit = resultRecord.getResultSize(); i < limit; i++) {
            sum += ratingFunction.evaluate(
                    goalFunction.evaluateExpression(geneticMaterial, resultRecord.getXForIndex(i), resultRecord.getYForIndex(i)),
                        resultRecord.getResultForIndex(i));
        }
        sum /= resultRecord.getResultSize();
        fitness = sum;
        processedFitness = resultFunction.apply(fitness);
        return fitness;
    }

    public NumericBound getBound() { return bound; }

    public double getResultFitness() { return processedFitness; }

    public double getFitness() { return fitness; }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for(int i = 0; i < geneticMaterial.length; i++) {
            builder.append(geneticMaterial[i]).append(" ");
        }
        builder.append("]");
        return "Genetic material: " + builder.toString();
    }
}
