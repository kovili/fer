package fer.nenr.genetic.algorithm;

import fer.nenr.genetic.fitness.IFitnessFunction;
import fer.nenr.genetic.fitness.IMultiParameterFunction;
import fer.nenr.genetic.model.Individual;
import fer.nenr.genetic.model.NumericBound;
import fer.nenr.genetic.model.Population;
import fer.nenr.genetic.model.ResultRecord;

import java.util.function.UnaryOperator;

public abstract class SkeletonGenericGeneticAlgorithm implements IGenericGeneticAlgorithm {

    private int populationSize;
    private int iterationLimit;
    private int unknownParameterCount;
    private NumericBound bound;
    private double mutationProbabilty;

    public SkeletonGenericGeneticAlgorithm(int populationSize, int iterationLimit, int unknownParameterCount, NumericBound bound, double mutationProbabilty) {
        this.populationSize = populationSize;
        this.iterationLimit = iterationLimit;
        this.unknownParameterCount = unknownParameterCount;
        this.bound = bound;
        this.mutationProbabilty = mutationProbabilty;
    }

    public abstract IFitnessFunction getFitnessFunction();

    public abstract UnaryOperator<Double> getResultProcessingFunction();

    public abstract Population processPopulation(Population population, double mutationProbabilty);

    @Override
    public Individual evaluatePopulation(Population population, ResultRecord resultRecord,
                              IMultiParameterFunction goalFunction, UnaryOperator<Double> resultFunction) {
        Individual result = population.getBestIndividual(resultRecord, goalFunction, getFitnessFunction(), getResultProcessingFunction());
        return result;
    }

    @Override
    public int getIterationLimit() { return iterationLimit; }
    @Override
    public void setIterationLimit(int iterationLimit) { this.iterationLimit = iterationLimit; }

    @Override
    public int getPopulationSize() { return populationSize; }
    @Override
    public void setPopulationSize(int populationSize) { this.populationSize = populationSize; }

    @Override
    public int getUnknownParameterCount() { return unknownParameterCount; }
    @Override
    public void setUnknownParameterCount(int unknownParameterCount) { this.unknownParameterCount = unknownParameterCount; }

    @Override
    public NumericBound getBound() { return bound; }
    @Override
    public void setBound(NumericBound bound) { this.bound = bound; }

    @Override
    public double getMutationProbability() { return mutationProbabilty; }
}
