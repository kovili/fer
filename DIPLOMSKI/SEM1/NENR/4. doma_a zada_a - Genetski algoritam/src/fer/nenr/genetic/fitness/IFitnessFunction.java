package fer.nenr.genetic.fitness;

public interface IFitnessFunction {

    double evaluate(double predictedValue, double actualValue);
}
