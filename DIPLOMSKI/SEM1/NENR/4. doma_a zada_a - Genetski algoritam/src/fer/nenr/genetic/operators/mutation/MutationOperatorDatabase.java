package fer.nenr.genetic.operators.mutation;

import fer.nenr.genetic.model.Individual;
import fer.nenr.genetic.model.NumericBound;

public class MutationOperatorDatabase {

    public static final IMutationOperator SIMPLE_MUTATION = (Individual individual) -> {
        int random = (int) (Math.random() * (individual.getGeneticSize()));
        if(random == individual.getGeneticSize()) random--;
        NumericBound bound = individual.getBound();
        double value = (Math.random() * (bound.getUpperBound() - bound.getLowerBound()) + bound.getLowerBound());
        individual.setMaterialForIndex(random, value);
        return individual;
    };
}
