package fer.nenr.genetic.operators.selection;

import fer.nenr.genetic.model.Parents;
import fer.nenr.genetic.model.Population;

public interface ISelectionOperator {

    Parents selectPopulation(Population population);
}
