from greek_gui import *
from neural import *
import sys


def main():
    if len(sys.argv) < 5 or len(sys.argv) > 8:
        print("Must have at least 4 and at most 6 arguments!\n"
              "1. Name of file where learning set is stored (or where learning set will be written if learn is true)\n"
              "2. Number of stored (x, y) points per learning pair - must be parsed to number\n"
              "3. Neural network layout. Input should be of format AxBxCx...xY where A,B,C...,Y are numbers and x is "
              "the separator between numbers.\n "
              "\tParameter A must be equal to 2 * stored_points_per_learning_pair. Y must be equal to 5 (number of "
              "recognizable greek letters).\n"
              "4. Machine learning algorithm, must be one of {1, 2, 3}:\n"
              "\t1) Batch learning\n"
              "\t2) Stochastic learning\n"
              "\t3) Mini-batch learning\n"
              "5. Learn - set to 1 to use Greek letter GUI to store new learning set points. Set to 0 to "
              "disable (default)\n"
              "6. Iteration count - number of iterations in neural network algorithm\n"
              "7. Eta - Neural network will stop if it reaches a mean square error below eta in the given iteration "
              "count.")
        return

    if len(sys.argv) >= 6 and sys.argv[5] == "1":
        GreekLettersGUI(learn=True, filename=sys.argv[1], point_count=sys.argv[2])
    learn_x, learn_y = read_from_file(sys.argv[1])

    network = NeuralNetwork(sys.argv[3], learn_x, learn_y, algorithm=sys.argv[4],
                            iterations=sys.argv[6] if len(sys.argv) >= 7 else 1000,
                            eta=sys.argv[7] if len(sys.argv) >= 8 else 0.01)
    network.learn()
    GreekLettersGUI(filename=sys.argv[1], point_count=sys.argv[2], network=network)


if __name__ == '__main__':
    main()
