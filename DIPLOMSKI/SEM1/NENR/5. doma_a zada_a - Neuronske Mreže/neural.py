from helper import *


# noinspection PyAttributeOutsideInit
class NeuralNetwork:
    def __init__(self, layers, inputs, outputs, iterations=1000, eta=0.00001, learning_rate=5, algorithm="1",
                 batch_size=10):
        self.layers = list(map(lambda x: int(x), layers.split("x")))

        if self.layers[-1] != 5 or self.layers[0] != len(inputs[0]):
            raise RuntimeError(
                "First and last layer must be (" + str(len(inputs[0])) + ", 5), but are (" + str(layers[0]) + "," + str(
                    layers[-1]) + ")!")

        self.inputs = np.transpose(np.array(list(map(lambda x: list(map(lambda el: float(el), x)), inputs)), ndmin=2))
        self.outputs = np.transpose(np.array(list(map(lambda x: list(map(lambda el: float(el), x)), outputs)), ndmin=2))
        self.iterations = int(iterations)
        self.eta = float(eta)
        self.batch_size = {"1": len(self.inputs[0]),
                           "2": 1,
                           "3": batch_size}[algorithm]
        self.learning_rate = learning_rate
        self.sigmoid = lambda net: 1 / (1 + np.exp(-net))
        self.group_size = len(self.inputs[0]) // 5
        self.per_group = self.group_size // self.batch_size
        self.reset_weights()

    def reset_weights(self):
        self.weights = []
        self.delta_weights = []
        self.bias = []
        self.delta_bias = []
        for layer_index in range(len(self.layers) - 1):
            self.weights.append((np.random.rand(self.layers[layer_index + 1], self.layers[layer_index]) * 4) - 2)
            self.bias.append(np.random.rand(self.layers[layer_index + 1]))
        self.weights = np.array(self.weights, dtype=object)
        self.bias = np.array(self.bias, dtype=object)
        self.restart_delta()

    def learn(self):
        sample_size = len(self.inputs[0])
        accumulate = self.batch_size != 1
        total_square_error = 0
        for iteration in range(self.iterations):
            # Train algorithm
            for sample_index in range(sample_size):
                curr_sample_index = sample_index
                if self.batch_size != 1 and self.batch_size != sample_size:
                    curr_sample_index = ((sample_index % self.per_group) + (
                            self.group_size * (sample_index // self.per_group)) + (
                                                 self.per_group * (sample_index // self.batch_size))) % sample_size

                curr_network_outputs = self.feed_forward(np.transpose(np.array(self.inputs[:, curr_sample_index])))
                curr_mistake_vector = calculate_mistake_vector(
                    np.transpose(curr_network_outputs[-1]), self.outputs[:, curr_sample_index])
                self.backpropagation(curr_mistake_vector, curr_network_outputs, accumulate,
                                     (((iteration * sample_size) + (sample_index + 1)) % self.batch_size) == 0)

            total_square_error = 0
            # Calculate mistake
            for sample_index in range(sample_size):
                curr_network_outputs = self.feed_forward(self.inputs[:, sample_index])
                curr_mistake_vector = calculate_mistake_vector(
                    np.transpose(curr_network_outputs[-1]), self.outputs[:, sample_index])
                total_square_error += calculate_square_error(curr_mistake_vector)

            mean_square_error = total_square_error / sample_size
            if iteration % 10 == 0:
                print(f"Iteration: {iteration}    Mean square error: {mean_square_error}")

            # Check if valid solution
            if mean_square_error <= self.eta:
                print(f"Final weight values reached for iteration: {iteration + 1}.")
                print(f"Mean square error: {mean_square_error}, eta: {self.eta}.")
                print("Learning finished\n")
                return

        print(f"Weights could not be optimized in {self.iterations} iterations.")
        print(f"Mean square error: {total_square_error / sample_size}, eta: {self.eta}.")
        print(f"If you think it's possible to lower total square error below {self.eta}, try increasing iteration "
              f"number.\n")

    def predict(self, new_samples):
        new_samples = np.transpose(new_samples)
        result_arr = []
        for sample_index in range(len(new_samples[0])):
            result_outputs = self.feed_forward(new_samples[:, sample_index])
            result_arr.append(result_outputs[-1])
        return result_arr

    def feed_forward(self, vector_x):
        output_vector = vector_x
        network_outputs = [np.transpose(vector_x)]
        for layer_index in range(len(self.weights)):
            input_vector = np.dot(self.weights[layer_index], np.transpose(output_vector)) + self.bias[layer_index]
            output_vector = self.sigmoid(input_vector)
            network_outputs.append(output_vector)
        return network_outputs

    def restart_delta(self):
        self.delta_bias = []
        self.delta_weights = []
        for layer_index in range(len(self.weights)):
            self.delta_weights.append(np.zeros_like(self.weights[layer_index]))
            self.delta_bias.append(np.zeros_like(self.bias[layer_index]))

    def backpropagation(self, mistake_vector, network_outputs, accumulate=False, accumulate_done=False):
        activation_error = mistake_vector

        for layer_index in range(len(self.weights) - 1, -1, -1):
            # Calculate error
            inputs_error = activation_error * network_outputs[layer_index + 1] * (1 - network_outputs[layer_index + 1])
            weights_error = np.outer(network_outputs[layer_index], inputs_error)
            activation_error = np.dot(inputs_error, self.weights[layer_index])
            bias_error = inputs_error

            # Update weights
            if accumulate:
                self.delta_weights[layer_index] += np.transpose(weights_error)
                self.delta_bias[layer_index] += np.transpose(bias_error)
                if accumulate_done:
                    self.weights[layer_index] -= (self.learning_rate / self.batch_size) * self.delta_weights[
                        layer_index]
                    self.bias[layer_index] -= (self.learning_rate / self.batch_size) * self.delta_bias[
                        layer_index]
            else:
                self.weights[layer_index] -= self.learning_rate * np.transpose(weights_error)
                self.bias[layer_index] -= self.learning_rate * np.transpose(bias_error)

        if accumulate and accumulate_done:
            self.restart_delta()
