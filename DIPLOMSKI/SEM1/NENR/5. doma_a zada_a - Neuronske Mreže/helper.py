import numpy as np
import math


def calculate_square_error(mistake_vector):
    return np.dot(mistake_vector, mistake_vector)


def calculate_mistake_vector(output_vector, sample_output):
    return output_vector - sample_output


def write_to_file(filename, learning_set_X, learning_set_y):
    with open(filename, 'w') as result_file:
        for (x, y) in zip(learning_set_X, learning_set_y):
            result_file.write(str(x) + "X" + str(y) + "\n")


def read_from_file(filename):
    with open(filename, 'r') as result_file:
        learning_set_x = []
        learning_set_y = []
        for line in result_file.readlines():
            elements = line.split("X")
            learning_set_x.append(elements[0].strip('][').split(', '))
            learning_set_y.append(elements[1].strip().strip('][').split(', '))
    return learning_set_x, learning_set_y


def as_probability(values):
    value_sum = np.sum(values)
    probability_array = []
    for value in values:
        probability_array.append(round(value / value_sum, 3))
    return probability_array
