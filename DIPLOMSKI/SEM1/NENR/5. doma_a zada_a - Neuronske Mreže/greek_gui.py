from tkinter import *
from helper import *


# noinspection PyAttributeOutsideInit
class GreekLettersGUI:
    def __init__(self, point_count=15, learn=False, filename='output', network=None):
        self.master = Tk()
        self.coordinates_x, self.coordinates_y = [], []
        self.final_x, self.final_y = [], []
        self.learning_set_X, self.learning_set_y, self.extracted_points = [], [], []
        self.current_drawing_count, self.distance, self.point_count = 0, 0, int(point_count)
        self.number_of_letters = 5
        self.chosen_element = 0
        self.learn = learn
        self.filename = filename
        self.network = network

        self.init_learning_gui()

    def init_learning_gui(self):
        self.master.title("Greek letter recognition NN")

        self.master.geometry("800x500")

        self.label = Label(self.master, text="Instructions: Draw one of these letters: Alpha, Beta, Gamma, Delta, "
                                             "Epsilon.")
        self.label.pack(side="top", fill="both", expand=True)
        if not self.learn:
            self.result_label = Label(self.master)
            self.result_label.pack(side="top", fill="both", expand=True)

        self.canvas = Canvas(self.master, relief="raise", borderwidth=1)
        self.canvas.pack(side="top")
        self.canvas.bind("<B1-Motion>", self.paint)
        self.canvas.bind("<ButtonRelease-1>", self.mouse_released)

        self.done_button = Button(self.master, text="Finish", fg="red", command=lambda x=self: x.quit())
        self.done_button.pack(side="bottom")

        self.elements = Frame(self.master)
        self.elements.pack(side="bottom", fill="x")
        if self.learn:
            self.element_alpha = Button(self.elements, text="ALPHA = 0", fg="blue",
                                        command=lambda x=self: x.choose_current_button(0))
            self.element_beta = Button(self.elements, text="BETA = 0",
                                       command=lambda x=self: x.choose_current_button(1))
            self.element_gamma = Button(self.elements, text="GAMMA = 0",
                                        command=lambda x=self: x.choose_current_button(2))
            self.element_delta = Button(self.elements, text="DELTA = 0",
                                        command=lambda x=self: x.choose_current_button(3))
            self.element_epsilon = Button(self.elements, text="EPSILON = 0",
                                          command=lambda x=self: x.choose_current_button(4))

            self.element_alpha.grid(row=0, column=1)
            self.element_beta.grid(row=0, column=2)
            self.element_gamma.grid(row=0, column=3)
            self.element_delta.grid(row=0, column=4)
            self.element_epsilon.grid(row=0, column=5)

        else:
            self.element_alpha = Label(self.elements, text="ALPHA")
            self.element_beta = Label(self.elements, text="BETA")
            self.element_gamma = Label(self.elements, text="GAMMA")
            self.element_delta = Label(self.elements, text="DELTA")
            self.element_epsilon = Label(self.elements, text="EPSILON")

            self.element_alpha.grid(row=0, column=1)
            self.element_beta.grid(row=0, column=2)
            self.element_gamma.grid(row=0, column=3)
            self.element_delta.grid(row=0, column=4)
            self.element_epsilon.grid(row=0, column=5)

        self.element_list = [[self.element_alpha, "ALPHA", 0], [self.element_beta, "BETA", 0],
                             [self.element_gamma, "GAMMA", 0],
                             [self.element_delta, "DELTA", 0], [self.element_epsilon, "EPSILON", 0]]

        self.elements.grid_columnconfigure(0, weight=1)
        self.elements.grid_columnconfigure(6, weight=1)

        self.master.mainloop()

    def paint(self, event):
        python_green = "#476042"
        x1, y1 = (event.x - 1), (event.y - 1)
        x2, y2 = (event.x + 1), (event.y + 1)
        self.coordinates_x.append(event.x)
        self.coordinates_y.append(event.y)
        self.canvas.create_oval(x1, y1, x2, y2, fill=python_green)

    def mouse_released(self, event):
        self.canvas.delete("all")
        if len(self.coordinates_x) <= self.point_count + 1:
            return

        self.normalize_points()
        self.calculate_distance()
        extracted = self.extract_points()
        if extracted == 0:
            return

        if self.learn:
            self.element_list[self.chosen_element][2] += 1
        else:
            results = self.network.predict([self.extracted])
            self.chosen_element = np.argmax(results)
            self.result_label.config(text=str(as_probability(results[0])))

        self.color_current_element(self.chosen_element)

    def quit(self):
        if self.learn:
            write_to_file(self.filename, self.learning_set_X, self.learning_set_y)
        self.master.destroy()

    def calculate_distance(self):
        prev_x = None
        prev_y = None
        self.distance = 0
        for i in range(len(self.final_x)):
            if prev_x is not None:
                self.distance += math.sqrt((self.final_x[i] - prev_x) ** 2 + (self.final_y[i] - prev_y) ** 2)
            prev_x = self.final_x[i]
            prev_y = self.final_y[i]

    def color_current_element(self, label_index, color="blue"):
        for idx, label in enumerate(self.element_list):
            if idx == label_index:
                self.element_list[idx][0].config(
                    text=f"{self.element_list[idx][1]}{' = ' + str((self.element_list[idx][2])) if self.learn else ''}",
                    fg=color)
            else:
                self.element_list[idx][0].config(fg="black")

    def choose_current_button(self, button_index):
        self.chosen_element = button_index
        self.color_current_element(button_index)

    def extract_points(self):
        picked_point_count = 1
        point_distance = self.distance / (self.point_count - 1)
        picked_points = [self.final_x[0], self.final_y[0]]

        for point_index in range(len(self.final_x) - 1):
            point_distance -= math.sqrt((self.final_x[point_index] - self.final_x[point_index + 1]) ** 2
                                        + (self.final_y[point_index] - self.final_y[point_index + 1]) ** 2)
            if point_distance <= 10E-3:
                point_distance += self.distance / (self.point_count - 1)
                picked_points.append(self.final_x[point_index + 1])
                picked_points.append(self.final_y[point_index + 1])
                picked_point_count += 1
                if picked_point_count == self.point_count:
                    break

        if len(picked_points) < self.point_count:
            picked_points.append(self.final_x[-1])
            if len(picked_points) < self.point_count:
                return 0

        if self.learn:
            learning_set_y = [0] * self.number_of_letters
            learning_set_y[self.chosen_element] = 1
            self.learning_set_X.append(picked_points)
            self.learning_set_y.append(learning_set_y)
        else:
            self.extracted = picked_points

    def normalize_points(self):
        self.final_x = np.array(self.coordinates_x)
        self.final_y = np.array(self.coordinates_y)
        self.coordinates_x = []
        self.coordinates_y = []
        self.final_x = self.final_x - np.mean(self.final_x)
        self.final_y = self.final_y - np.mean(self.final_y)
        max_x = np.abs(self.final_x[np.argmax(np.abs(self.final_x))])
        max_y = np.abs(self.final_y[np.argmax(np.abs(self.final_y))])
        max_coordinate = np.abs(max_x if max_x > max_y else max_y)
        self.final_x = self.final_x / max_coordinate
        self.final_y = self.final_y / max_coordinate
