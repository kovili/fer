import helper
import numpy as np
import math


class ANFIS:
    def __init__(self, learn_x, learn_y, rule_count=9, ROL_linear=2, ROL_sigmoid=0.024, algorithm="batch",
                 iterations=500,
                 eta=0.01, output=True):
        self.rule_count = rule_count
        self.learn_x = np.array(learn_x)
        self.learn_y = np.array(learn_y)
        self.input_size = len(learn_x[0])
        self.ROL_linear = ROL_linear
        self.ROL_sigmoid = ROL_sigmoid
        self.output = output
        self.batch_size = {"stochastic": 1, "batch": len(self.learn_x)}[algorithm]
        if self.batch_size is None:
            raise AttributeError("Algorithm must be one of: stochastic, batch")
        self.iterations = iterations
        self.eta = eta
        # row_index = rule, column_index = input variable
        self.sigmoid_slope, self.sigmoid_offset = (np.random.rand(self.rule_count, self.input_size) * 4) - 2, (
                np.random.rand(
                    self.rule_count, self.input_size) * 4) - 2
        self.linear_params = (np.random.rand(self.rule_count, self.input_size + 1) * 2) - 1
        self.sigmoid_slope_acc, self.sigmoid_offset_acc, self.linear_params_acc = self.reset_accumulators()
        self.best_mse = math.inf
        self.errors = []

    def predict(self, sample):
        network_outputs = self.feed_forward(sample)
        return network_outputs[-1]

    def learn(self):
        for iteration in range(self.iterations):
            for sample_index in range(len(self.learn_x)):
                network_outputs = self.feed_forward(self.learn_x[sample_index])
                self.backpropagate(network_outputs, sample_index, ((sample_index + 1) % self.batch_size) == 0)
            curr_eta = 0
            for sample_index in range(len(self.learn_x)):
                network_outputs = self.feed_forward(self.learn_x[sample_index])
                curr_eta += self.square_error(self.learn_y[sample_index][0], network_outputs[-1])

            mean_square_err = curr_eta / (len(self.learn_x) * 2)
            self.errors.append(mean_square_err)
            if iteration % 10 == 0 and self.output:
                print(f"Current iteration: {iteration}")
                print(f"Current MSE: {mean_square_err}")
                if mean_square_err < self.best_mse:
                    self.best_mse = mean_square_err
                    self.best_linear_params, self.best_sigmoid_slope, self.best_sigmoid_offset = np.copy(
                        self.linear_params), np.copy(self.sigmoid_slope), np.copy(self.sigmoid_offset)
            if mean_square_err <= self.eta:
                helper.write_to_file(self.errors)
                print(f"Successfully reached mean square error of {mean_square_err} <= {self.eta} "
                      f"in {iteration} iterations.")
                return
        helper.write_to_file(self.errors)
        self.linear_params, self.sigmoid_slope, self.sigmoid_offset = \
            self.best_linear_params, self.best_sigmoid_slope, self.best_sigmoid_offset
        print(f"Failed to reach mean square error less than {self.eta} in {self.iterations} iterations.")
        print(f"Got mean square error of {self.best_mse} instead.")

    def feed_forward(self, sample):
        memberships = np.zeros(shape=(self.rule_count, self.input_size))
        antecedent_weights = np.ones(shape=(self.rule_count, 1))
        for i in range(self.rule_count):
            curr_min = 1
            for j in range(self.input_size):
                memberships[i][j] = helper.sigmoid(self.sigmoid_slope[i][j], self.sigmoid_offset[i][j], sample[j])
                curr_min *= memberships[i][j]
            antecedent_weights[i][0] = curr_min

        weights_sum = np.sum(antecedent_weights)
        normalized_weights = antecedent_weights / weights_sum
        function_outputs = np.zeros(shape=(self.rule_count, 1))

        for i in range(self.rule_count):
            for j in range(self.input_size):
                function_outputs[i][0] += self.linear_params[i][j] * sample[j]
            function_outputs[i][0] = (function_outputs[i][0] +
                                      self.linear_params[i][self.input_size]) * normalized_weights[i]

        result = np.sum(function_outputs)
        return sample, memberships, antecedent_weights, normalized_weights, function_outputs, result

    def backpropagate(self, network_outputs, sample_index, add):
        sample, memberships, antecedent_weights, normalized_weights, function_outputs, result = network_outputs

        error = self.learn_y[sample_index][0] - result

        # Calculate errors
        for i in range(self.rule_count):
            weight_sum = 0
            function_weight_prodsum = 0

            for k in range(self.rule_count):
                weight_sum += antecedent_weights[k]
                function_weight_prodsum += antecedent_weights[k] * (function_outputs[i] - function_outputs[k])
            weight_sum_squared = weight_sum ** 2
            # Error for a_i
            for j in range(self.input_size):
                sigmoid_offset_derivative_val_i = self.sigmoid_slope[i][j] * memberships[i][j] * (
                        1 - memberships[i][j]) * memberships[i][1 - j]

                self.sigmoid_offset_acc[i][j] += -error * (
                        function_weight_prodsum / weight_sum_squared) * sigmoid_offset_derivative_val_i

                # Error for b_i
                sigmoid_slope_derivative_val_i = (sample[j] - self.sigmoid_offset[i][j]) * memberships[i][j] * (
                        1 - memberships[i][j]) * memberships[i][1 - j]
                self.sigmoid_slope_acc[i][j] += error * (
                        function_weight_prodsum / weight_sum_squared) * sigmoid_slope_derivative_val_i

                # Errors for p, q, and r
                self.linear_params_acc[i][j] = self.linear_params_acc[i][j] - error * (
                        (antecedent_weights[i] * sample[j]) / weight_sum)
            self.linear_params_acc[i][self.input_size] = self.linear_params_acc[i][self.input_size] - error * (
                    antecedent_weights[i] / weight_sum)

        if add:
            np.subtract(self.sigmoid_offset, self.sigmoid_offset_acc * (self.ROL_sigmoid / self.batch_size),
                        out=self.sigmoid_offset)
            np.subtract(self.sigmoid_slope, self.sigmoid_slope_acc * (self.ROL_sigmoid / self.batch_size),
                        out=self.sigmoid_slope)
            np.subtract(self.linear_params, self.linear_params_acc * (self.ROL_linear / self.batch_size),
                        out=self.linear_params)
            self.sigmoid_slope_acc, self.sigmoid_slope_acc, self.linear_params_acc = self.reset_accumulators()

    def reset_accumulators(self):
        return np.zeros(shape=(self.rule_count, self.input_size)), np.zeros(
            shape=(self.rule_count, self.input_size)), np.zeros(
            shape=(self.rule_count, self.input_size + 1))

    @staticmethod
    def square_error(function_output, network_output):
        return (function_output - network_output) ** 2
