import math
import numpy as np
import matplotlib.pyplot as plt


def approximate(exponential):
    if abs(exponential) < 20:
        return exponential
    return 20 if exponential > 0 else -20


def color_picker():
    return [np.random.rand(), np.random.rand(), np.random.rand()]


def sigmoid(sig_slope, sig_offset, x):
    return 1 / (1 + np.exp(approximate(sig_slope * (x - sig_offset))))


def generate_problem_dataset():
    problem_x = []
    problem_y = []
    for i in range(-4, 5):
        for j in range(-4, 5):
            problem_x.append([i, j])
            problem_y.append([problem_function(i, j)])
    return problem_x, problem_y


def write_to_file(errors):
    file = open('output.txt', 'w')
    for line in errors:
        file.write(str(line) + "\n")
    file.close()


def problem_function(x, y):
    return ((x - 1) ** 2 + (y + 2) ** 2 - 5 * x * y + 3) * (math.cos(x / 5.)) ** 2


def t_norm(membership_first, membership_second):
    return membership_first * membership_second


def graph_function(X, Y):
    result = np.zeros(shape=(len(X), len(X[0])))
    for i in range(len(X)):
        for j in range(len(X[0])):
            result[i][j] = problem_function(X[i][j], Y[i][j])
    return graph_3d(X, Y, result, "Target function")


def graph_neural_function(X, Y, neural_network):
    Z = np.zeros(shape=(len(X), len(X[0])))
    for i in range(len(X)):
        for j in range(len(X[0])):
            Z[i][j] = neural_network.predict([X[i][j], Y[i][j]])
    return graph_3d(X, Y, Z, "Learned function")


def graph_difference(X, Y, neural_network):
    Z = np.zeros(shape=(len(X), len(X[0])))
    for i in range(len(X)):
        for j in range(len(X[0])):
            Z[i][j] = neural_network.predict([X[i][j], Y[i][j]]) - problem_function(X[i][j], Y[i][j])
    return graph_3d(X, Y, Z, "Difference between target function and learned function")


def graph_rules(sigmoid_offset, sigmoid_slope):
    plot_rule_x = plt.figure()
    plot_rule_y = plt.figure()
    plot_consequent = plt.figure()
    for i in range(len(sigmoid_offset)):
        color = color_picker()
        X_rule_A = np.linspace(-4, 4, 200)
        Y_rule_A = []
        for x in X_rule_A:
            Y_rule_A.append(sigmoid(x, sigmoid_slope[i][0], sigmoid_offset[i][0]))
        graph_2d(X_rule_A, Y_rule_A, plot_rule_x, color, "Rules for x")

        X_rule_B = np.linspace(-4, 4, 200)
        Y_rule_B = []
        for y in X_rule_B:
            Y_rule_B.append(sigmoid(y, sigmoid_slope[i][1], sigmoid_offset[i][1]))
        graph_2d(X_rule_B, Y_rule_B, plot_rule_y, color, "Rules for y")

        rule_consequent = []
        for j in range(len(X_rule_B)):
            rule_consequent.append((Y_rule_A[j] * Y_rule_B[j]))
        graph_2d(X_rule_A, rule_consequent, plot_consequent, color, "Antecedent strength")
    return plt


def graph_error(errors):
    fig = plt.figure()
    return graph_2d(range(len(errors)), errors, fig, None, "Errors for iterations")


def graph_3d(X, Y, Z, title="Fig"):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_title(title)
    ax.plot_surface(X, Y, Z, rstride=10, cstride=10)
    plt.xlabel("x")
    plt.ylabel("y")
    return plt


def graph_2d(X, Y, fig, color=None, title="Fig"):
    ax = fig.gca()
    ax.set_title(title)
    ax.plot(X, Y, color=color)
    plt.xlabel("x")
    plt.ylabel("y")
    return plt
