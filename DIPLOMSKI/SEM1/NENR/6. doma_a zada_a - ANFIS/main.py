from helper import *
from ANFIS import *
import sys


def main():
    if len(sys.argv) < 5:
        print("HELP")
        print("Takes in atleast 3 arguments")
        print("1. Rule count")
        print("2. Rate of learning for linear params")
        print("3. Rate of learning for sigmoid params")
        print("4. Number of iterations")
        print("MORE [OPTIONAL] graph output - any number of 'target', 'mse_iter', 'anfis_func', 'anfis_error_func', "
              "'rules'")
        print("\ttarget - shows graph of target function ")
        print("\tmse_iter - shows graph of MSE as a variable of iterations ")
        print("\tanfis_func - shows graph of function that was learned by the ANFIS system")
        print("\tanfis_error_func - shows graph of difference between the target function and the ANFIS function")
        print("\trules - shows graphs of: learned rules for x, learned rules for y and antecedent strength w")

    dataset_x, dataset_y = generate_problem_dataset()
    anfis = ANFIS(dataset_x, dataset_y,
                  rule_count=int(sys.argv[1]),
                  ROL_linear=float(sys.argv[2]),
                  ROL_sigmoid=float(sys.argv[3]),
                  iterations=int(sys.argv[4]))
    anfis.learn()

    if "mse_iter" in sys.argv:
        graph_error(anfis.errors).show()

    X = np.arange(-4, 4, 0.05)
    Y = np.arange(-4, 4, 0.05)
    X, Y = np.meshgrid(X, Y)

    if "target" in sys.argv:
        graph_function(X, Y).show()

    if "anfis_func" in sys.argv:
        graph_neural_function(X, Y, anfis).show()

    if "anfis_error_func" in sys.argv:
        graph_difference(X, Y, anfis).show()

    if "rules" in sys.argv:
        graph_rules(anfis.sigmoid_offset, anfis.sigmoid_slope).show()


if __name__ == '__main__':
    main()
