package hr.fer.zemris.fuzzy.decoder;

import hr.fer.zemris.fuzzy.set.IFuzzySet;

public interface IFuzzyDecoder {

    int decode(IFuzzySet set);
}
