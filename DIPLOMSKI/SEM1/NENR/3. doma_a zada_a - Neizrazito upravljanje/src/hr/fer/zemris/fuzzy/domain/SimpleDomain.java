package hr.fer.zemris.fuzzy.domain;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class SimpleDomain extends Domain {

    private int first;
    private int last;

    public SimpleDomain(int first, int last) {
        if(first > last) {
            this.last = first;
            this.first = last;
        } else {
            this.first = first;
            this.last = last;
        }
    }

    @Override
    public int getCardinality() {
        return last - first;
    }

    @Override
    public IDomain getComponent(int index) {
        if(index == 0) return this;
        return null;
    }

    @Override
    public int getNumberOfComponents() {
        return 1;
    }

    @Override
    public DomainElement elementForIndex(int index) {
        if(index > last - first) {
            throw new IllegalArgumentException("Index too big!");
        }
        return DomainElement.of(new int[]{first + index});
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return new Iterator<DomainElement>() {
            private int current = first;

            @Override
            public boolean hasNext() { return current < last; }

            @Override
            public DomainElement next() {
                if(hasNext()) {
                    return DomainElement.of(current++);
                }
                throw new NoSuchElementException("No more elements");
            }
        };
    }

    public int getFirst() {
        return first;
    }

    public int getLast() {
        return last;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleDomain that = (SimpleDomain) o;
        return first == that.first &&
                last == that.last;
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, last);
    }
}
