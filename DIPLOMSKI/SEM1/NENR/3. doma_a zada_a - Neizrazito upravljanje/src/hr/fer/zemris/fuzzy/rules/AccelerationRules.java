package hr.fer.zemris.fuzzy.rules;

import hr.fer.zemris.fuzzy.functions.TNorm;
import hr.fer.zemris.fuzzy.language.AccelerationInterpretation;
import hr.fer.zemris.fuzzy.language.DistanceTermSet;
import hr.fer.zemris.fuzzy.language.VelocityTermSet;

import java.util.ArrayList;
import java.util.List;

public class AccelerationRules {
    private static List<Rule> accelerationRules = null;
    private static TNorm tNorm = null;


    private static List<Rule> initializeRules(TNorm tNorm) {
        if(accelerationRules != null && AccelerationRules.tNorm == tNorm) return accelerationRules;

        List<Rule> rules = new ArrayList<>();

        Rule.RuleBuilder builder = new Rule.RuleBuilder();

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{1, 0, 0, 0, 1, 0}).
                setDescription("If L is near and velocity is slow, accelerate slowly").
                addAntecedent(DistanceTermSet.NEAR).addAntecedent(VelocityTermSet.SLOW).
                addInterpretation(AccelerationInterpretation.ACCELERATE_SLOWLY));
        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 1, 0, 0, 1, 0}).
                setDescription("If D is near and velocity is slow, accelerate slowly").
                addAntecedent(DistanceTermSet.NEAR).addAntecedent(VelocityTermSet.SLOW).
                addInterpretation(AccelerationInterpretation.ACCELERATE_SLOWLY));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 0, 0, 1, 0}).
                setDescription("If velocity is slow go fast").
                addAntecedent(VelocityTermSet.SLOW).
                addInterpretation(AccelerationInterpretation.ACCELERATE_FAST));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 0, 0, 1, 0}).
                setDescription("If velocity is very fast brake").
                addAntecedent(VelocityTermSet.FASTER).
                addInterpretation(AccelerationInterpretation.BRAKE));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 1, 0, 1, 0}).
                setDescription("If LK is good distance and velocity is fast, brake").
                addAntecedent(DistanceTermSet.GOOD).addAntecedent(VelocityTermSet.FAST).
                addInterpretation(AccelerationInterpretation.BRAKE));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 0, 1, 1, 0}).
                setDescription("If DK is good distance and velocity is fast, brake").
                addAntecedent(DistanceTermSet.GOOD).addAntecedent(VelocityTermSet.FAST).
                addInterpretation(AccelerationInterpretation.BRAKE));


        AccelerationRules.accelerationRules = rules;
        AccelerationRules.tNorm = tNorm;

        return rules;
    }

    public static List<Rule> getRules(TNorm tNorm) {
        return initializeRules(tNorm);
    }
}
