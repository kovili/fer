package hr.fer.zemris.fuzzy.decoder;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.set.IFuzzySet;

public class CenterOfAreaDecoder implements IFuzzyDecoder {

    @Override
    public int decode(IFuzzySet set) {
        double numerator = 0, divisor = 0, membership;
        for(DomainElement element : set.getDomain()) {
            membership = set.getValueAt(element);
            numerator += membership * element.getComponentValue(0);
            divisor += membership;
        }
        return (int) (numerator / divisor);
    }
}
