package hr.fer.zemris.fuzzy.primjeri.treca;

import hr.fer.zemris.fuzzy.Debug;
import hr.fer.zemris.fuzzy.decoder.CenterOfAreaDecoder;
import hr.fer.zemris.fuzzy.relations.Relations;
import hr.fer.zemris.fuzzy.rules.AngleRules;
import hr.fer.zemris.fuzzy.rules.Rule;
import hr.fer.zemris.fuzzy.set.IFuzzySet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AngleRuleEngineTester {

    private final static int MAX_DIST = 100;
    private final static int MAX_VEL = 50;
    private final static int DISTANCE_UNIT = 5;

    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        Scanner s;
        String line;

        List<Rule> angleRules = AngleRules.getRules(Math::min);

        while(true) {
            System.out.println("Unesite u jednom retku vrijednosti (L, D, LK, DK, V, S) odvojene razmakom.");
            List<IFuzzySet> inferences = null;

            if ((line = input.readLine()) != null) {
                s = new Scanner(line);
                int L = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                int D = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                int LK = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                int DK = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                int V = Math.min(s.nextInt(), MAX_VEL);
                int S = s.nextInt();
                inferences = angleRules.stream().map((rule) -> rule.getResult(L, D, LK, DK, V, S)).collect(Collectors.toList());
            }

            IFuzzySet result = Relations.unionOfSimpleSets(inferences);

            Debug.print(result, "Rezultatni skup:");
            System.out.println();
            System.out.println("Dekodirana vrijednost zakljucka je: " + new CenterOfAreaDecoder().decode(result));
            System.out.println();
        }
    }

}
