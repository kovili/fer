package hr.fer.zemris.fuzzy.language;

import hr.fer.zemris.fuzzy.domain.SimpleDomain;
import hr.fer.zemris.fuzzy.set.CalculatedFuzzySet;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.StandardFuzzySets;

public class OrientationTermSet {

    public static final IFuzzySet GOOD_ORIENTATION =
            new CalculatedFuzzySet(new SimpleDomain(0, 2), StandardFuzzySets.gammaFunction(0, 1));

    public static final IFuzzySet BAD_ORIENTATION =
            Grammar.not(GOOD_ORIENTATION);
}
