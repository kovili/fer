package hr.fer.zemris.fuzzy.engine;

import hr.fer.zemris.fuzzy.decoder.IFuzzyDecoder;
import hr.fer.zemris.fuzzy.functions.TNorm;
import hr.fer.zemris.fuzzy.relations.Relations;
import hr.fer.zemris.fuzzy.rules.AccelerationRules;
import hr.fer.zemris.fuzzy.rules.AngleRules;
import hr.fer.zemris.fuzzy.rules.Rule;
import hr.fer.zemris.fuzzy.set.IFuzzySet;

import java.util.List;
import java.util.stream.Collectors;

public interface Engine {

    default int inferVelocity(int L, int D, int LK, int DK, int V, int S) {
        List<Rule> rules = AccelerationRules.getRules(getTNorm());
        List<IFuzzySet> inferences = rules.stream().map((rule) -> rule.getResult(L, D, LK, DK, V, S)).collect(Collectors.toList());

        IFuzzySet result = Relations.unionOfSimpleSets(inferences);
        return getDecoder().decode(result);
    }

    default int inferRotation(int L, int D, int LK, int DK, int V, int S) {
        List<Rule> rules = AngleRules.getRules(getTNorm());
        List<IFuzzySet> inferences = rules.stream().map((rule) -> rule.getResult(L, D, LK, DK, V, S)).collect(Collectors.toList());

        IFuzzySet result = Relations.unionOfSimpleSets(inferences);
        return getDecoder().decode(result);
    }

    TNorm getTNorm();

    IFuzzyDecoder getDecoder();

}
