package hr.fer.zemris.fuzzy.rules;

import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.functions.TNorm;
import hr.fer.zemris.fuzzy.relations.Relations;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.MutableFuzzySet;

import java.util.ArrayList;
import java.util.List;

public class Rule {

    private IFuzzySet rule;
    private TNorm tNorm;
    private int []activeRules;
    private IDomain resultDomain;
    private String description;

    private Rule() {}

    public static class RuleBuilder {
        private Rule constructedRule;

        public RuleBuilder() {
            constructedRule = new Rule();
            constructedRule.tNorm = Math::min;
        }

        public RuleBuilder setActiveRules(int [] activeRules) {
            constructedRule.activeRules = activeRules;
            return this;
        }

        public RuleBuilder setDescription(String description) {
            constructedRule.description = description;
            return this;
        }

        public RuleBuilder setTNorm(TNorm tNorm) {
            constructedRule.tNorm = tNorm;
            return this;
        }

        public RuleBuilder addAntecedent(IFuzzySet rule) {
            constructedRule.rule = Relations.cartesianProduct(constructedRule.rule, rule, constructedRule.tNorm);
            return this;
        }

        public Rule addInterpretation(IFuzzySet rule) {
            addAntecedent(rule);
            Rule result = this.constructedRule;
            result.resultDomain = rule.getDomain();
            this.constructedRule = new Rule();
            constructedRule.tNorm = Math::min;
            return result;
        }


    }

    public MutableFuzzySet getResult(int left, int right, int leftUp, int rightUp, int velocity, int orientation) {
        List<Integer> singletons = new ArrayList<>();

        if(activeRules[0] != 0) singletons.add(left);
        if(activeRules[1] != 0) singletons.add(right);
        if(activeRules[2] != 0) singletons.add(leftUp);
        if(activeRules[3] != 0) singletons.add(rightUp);
        if(activeRules[4] != 0) singletons.add(velocity);
        if(activeRules[5] != 0) singletons.add(orientation);

        if(singletons.size() + 1 != this.rule.getDomain().getNumberOfComponents()) {
            throw new IllegalStateException("Invalid state: Number of components should be equal to relevant inputs for rule plus one.");
        }

        int []indexJumps = new int[singletons.size()];
        indexJumps[singletons.size() - 1] = resultDomain.getCardinality();
        for(int i = singletons.size() - 2; i >= 0; i--) {
            indexJumps[i] = rule.getDomain().getComponent(i + 1).getCardinality() * indexJumps[i + 1];
        }

        int resultIndex = 0;
        for(int i = 0; i < singletons.size(); i++) {
            resultIndex += singletons.get(i) * indexJumps[i];
        }

        MutableFuzzySet result = new MutableFuzzySet(resultDomain);

        for(int i = 0; i < resultDomain.getCardinality(); i++) {
            result.set(i, rule.getValueAt(i + resultIndex));
        }

        return result;
    }

    @Override
    public String toString() {
        return description;
    }
}
