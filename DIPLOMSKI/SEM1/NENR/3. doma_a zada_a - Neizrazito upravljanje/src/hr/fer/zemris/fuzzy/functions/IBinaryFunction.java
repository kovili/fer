package hr.fer.zemris.fuzzy.functions;

public interface IBinaryFunction {

    double valueAt(double a, double b);
}
