package hr.fer.zemris.fuzzy.language;

import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.domain.SimpleDomain;
import hr.fer.zemris.fuzzy.set.CalculatedFuzzySet;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.StandardFuzzySets;

public class AccelerationInterpretation {
    public static final IDomain accelerationDomain = new SimpleDomain(-10, 11);

    public static final IFuzzySet ACCELERATE_SLOWLY =
            new CalculatedFuzzySet(accelerationDomain, StandardFuzzySets.lambdaFunction(10, 13, 16));

    public static final IFuzzySet ACCELERATE_FAST =
            new CalculatedFuzzySet(accelerationDomain, StandardFuzzySets.lambdaFunction(14, 17, 20));

    public static final IFuzzySet BRAKE =
            new CalculatedFuzzySet(accelerationDomain, StandardFuzzySets.lFunction(6, 8));



}
