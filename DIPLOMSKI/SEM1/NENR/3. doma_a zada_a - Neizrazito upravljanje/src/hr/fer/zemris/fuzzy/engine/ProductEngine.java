package hr.fer.zemris.fuzzy.engine;

import hr.fer.zemris.fuzzy.decoder.IFuzzyDecoder;

public class ProductEngine extends SimpleEngine {

    public ProductEngine(IFuzzyDecoder decoder) {
        super((a,b) -> a * b,decoder);
    }
}
