package hr.fer.zemris.fuzzy.language;

import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.domain.SimpleDomain;
import hr.fer.zemris.fuzzy.set.CalculatedFuzzySet;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.StandardFuzzySets;

public class VelocityTermSet {

    public static final IDomain velocityDomain = new SimpleDomain(0, 51);

    public static final IFuzzySet SLOW =
            new CalculatedFuzzySet(velocityDomain, StandardFuzzySets.lFunction(15, 38));

    public static final IFuzzySet FAST =
            new CalculatedFuzzySet(velocityDomain, StandardFuzzySets.gammaFunction(25, 40));

    public static final IFuzzySet FASTER =
            new CalculatedFuzzySet(velocityDomain, StandardFuzzySets.gammaFunction(40, 50));
}
