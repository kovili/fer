package hr.fer.zemris.fuzzy.primjeri.treca;

import hr.fer.zemris.fuzzy.decoder.CenterOfAreaDecoder;
import hr.fer.zemris.fuzzy.engine.Engine;
import hr.fer.zemris.fuzzy.engine.SimpleEngine;

public class Primjer1 {

    public static void main(String[] args) {
        Engine engine = new SimpleEngine(new CenterOfAreaDecoder());

        System.out.println(engine.inferRotation(100, 5, 100, 5, 0, 0));
    }
}
