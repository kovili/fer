package hr.fer.zemris.fuzzy.language;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.MutableFuzzySet;

public class Grammar {

    public static IFuzzySet very(IFuzzySet set) {
        MutableFuzzySet result = new MutableFuzzySet(set.getDomain());

        for(DomainElement element : set.getDomain()) {
            result.set(element, Math.pow(set.getValueAt(element), 2));
        }

        return result;
    }

    public static IFuzzySet somewhat(IFuzzySet set) {
        MutableFuzzySet result = new MutableFuzzySet(set.getDomain());

        for(DomainElement element : set.getDomain()) {
            result.set(element, Math.pow(set.getValueAt(element), 0.5));
        }

        return result;
    }

    public static IFuzzySet not(IFuzzySet set) {
        MutableFuzzySet result = new MutableFuzzySet(set.getDomain());

        for(DomainElement element : set.getDomain()) {
            result.set(element, 1 - set.getValueAt(element));
        }

        return result;
    }

}
