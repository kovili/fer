package hr.fer.zemris.fuzzy.language;

import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.domain.SimpleDomain;
import hr.fer.zemris.fuzzy.set.CalculatedFuzzySet;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.StandardFuzzySets;

public class AngleInterpretation {
    public static final IDomain angleDomain = new SimpleDomain(-90, 91);

    public static final IFuzzySet SOFT_RIGHT =
            new CalculatedFuzzySet(angleDomain, StandardFuzzySets.lambdaFunction(45, 60, 75));

    public static final IFuzzySet SOFT_LEFT =
            new CalculatedFuzzySet(angleDomain, StandardFuzzySets.lambdaFunction(105, 120, 135));

    public static final IFuzzySet HARD_RIGHT =
            new CalculatedFuzzySet(angleDomain, StandardFuzzySets.lambdaFunction(30, 45, 60));

    public static final IFuzzySet HARD_LEFT =
            new CalculatedFuzzySet(angleDomain, StandardFuzzySets.lambdaFunction(120, 135, 150));

    public static final IFuzzySet TURN_RIGHT =
            new CalculatedFuzzySet(angleDomain, StandardFuzzySets.lFunction(30, 45));

    public static final IFuzzySet TURN_LEFT =
            new CalculatedFuzzySet(angleDomain, StandardFuzzySets.gammaFunction(135, 150));

    public static final IFuzzySet STEADY =
            new CalculatedFuzzySet(angleDomain, StandardFuzzySets.lambdaFunction(55, 90, 125));

}
