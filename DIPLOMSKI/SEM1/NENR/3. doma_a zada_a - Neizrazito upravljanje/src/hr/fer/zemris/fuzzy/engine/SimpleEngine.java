package hr.fer.zemris.fuzzy.engine;

import hr.fer.zemris.fuzzy.decoder.IFuzzyDecoder;
import hr.fer.zemris.fuzzy.functions.TNorm;

public class SimpleEngine implements Engine {

    private TNorm tNorm;
    private IFuzzyDecoder decoder;

    public SimpleEngine(TNorm tNorm, IFuzzyDecoder decoder) {
        this.tNorm = tNorm;
        this.decoder = decoder;
    }

    public SimpleEngine(IFuzzyDecoder decoder) {
        this(Math::min, decoder);
    }

    @Override
    public TNorm getTNorm() {
        return tNorm;
    }

    @Override
    public IFuzzyDecoder getDecoder() {
        return decoder;
    }
}
