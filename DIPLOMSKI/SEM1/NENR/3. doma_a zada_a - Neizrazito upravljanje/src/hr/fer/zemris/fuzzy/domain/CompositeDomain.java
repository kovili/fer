package hr.fer.zemris.fuzzy.domain;

import java.util.*;
import java.util.stream.Collectors;

public class CompositeDomain extends Domain {

    private List<SimpleDomain> components;

    public CompositeDomain(SimpleDomain ...domains) {
        components = Arrays.stream(domains).collect(Collectors.toList());
    }

    @Override
    public DomainElement elementForIndex(int index) {
        return super.elementForIndex(index);
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return new Iterator<DomainElement>() {

            private int []coordinateClimber = initialize();

            private int[] initialize() {
                coordinateClimber = new int[components.size()];
                for(int i = 0; i < coordinateClimber.length; i++) {
                    coordinateClimber[i] = components.get(i).getFirst();
                }
                return coordinateClimber;
            }

            @Override
            public boolean hasNext() {
                for(int i = 0; i < components.size(); i++) {
                    if(components.get(0).getLast() <= coordinateClimber[0]) return false;
                }
                return true;
            }

            @Override
            public DomainElement next() {
                if(hasNext()) {
                    DomainElement next = DomainElement.of(coordinateClimber);
                    increment();
                    return next;
                }
                throw new NoSuchElementException("No more elements in composite!");
            }

            private void increment() {
                boolean carryFlag = true;
                for(int i = coordinateClimber.length - 1; i >= 0 && carryFlag; i--) {
                    carryFlag = false;
                    coordinateClimber[i]++;
                    if(coordinateClimber[i] >= components.get(i).getLast() && i != 0) {
                        carryFlag = true;
                        coordinateClimber[i] -= components.get(i).getCardinality();
                    }
                }

            }

        };
    }

    public int getCardinality() {
        int cartesianProduct = components.get(0).getCardinality();

        for(int i = 1; i < components.size(); i++) {
            cartesianProduct *= components.get(i).getCardinality();
        }

        return cartesianProduct;
    }

    @Override
    public IDomain getComponent(int index) {
        return components.get(index);
    }

    @Override
    public int getNumberOfComponents() {
        return components.size();
    }

}
