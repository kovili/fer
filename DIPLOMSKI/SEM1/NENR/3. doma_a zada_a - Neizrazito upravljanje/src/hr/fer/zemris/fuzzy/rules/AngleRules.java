package hr.fer.zemris.fuzzy.rules;

import hr.fer.zemris.fuzzy.functions.TNorm;
import hr.fer.zemris.fuzzy.language.AngleInterpretation;
import hr.fer.zemris.fuzzy.language.DistanceTermSet;
import hr.fer.zemris.fuzzy.language.OrientationTermSet;

import java.util.ArrayList;
import java.util.List;

public class AngleRules {

    private static List<Rule> angleRules = null;
    private static TNorm tNorm = null;


    private static List<Rule> initializeRules(TNorm tNorm) {
        if(angleRules != null && AngleRules.tNorm == tNorm) return angleRules;

        List<Rule> rules = new ArrayList<>();

        Rule.RuleBuilder builder = new Rule.RuleBuilder();

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{1, 0, 0, 0, 0, 0}).
                setDescription("If L is very near, hard right").
                addAntecedent(DistanceTermSet.VERY_VERY_NEAR).
                addInterpretation(AngleInterpretation.HARD_RIGHT));
        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 1, 0, 0, 0, 0}).
                setDescription("If D is very near, hard left").
                addAntecedent(DistanceTermSet.VERY_VERY_NEAR).
                addInterpretation(AngleInterpretation.HARD_LEFT));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 1, 0, 0, 0}).
                setDescription("If LK is very near, hard right").
                addAntecedent(DistanceTermSet.VERY_NEAR).
                addInterpretation(AngleInterpretation.HARD_RIGHT));
        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 0, 1, 0, 0}).
                setDescription("If DK is very near, hard left").
                addAntecedent(DistanceTermSet.VERY_NEAR).
                addInterpretation(AngleInterpretation.HARD_LEFT));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{1, 0, 1, 0, 0, 0}).
                setDescription("If L is very near, and LK is near,  hard right").
                addAntecedent(DistanceTermSet.VERY_NEAR).addAntecedent(DistanceTermSet.NEAR).
                addInterpretation(AngleInterpretation.HARD_RIGHT));
        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 1, 0, 1, 0, 0}).
                setDescription("If D is very near, and DK is near, hard left").
                addAntecedent(DistanceTermSet.VERY_NEAR).addAntecedent(DistanceTermSet.NEAR).
                addInterpretation(AngleInterpretation.HARD_LEFT));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 1, 0, 1, 0, 0}).
                setDescription("If R is very near, and RK is very near turn left").
                addAntecedent(DistanceTermSet.VERY_NEAR).addAntecedent(DistanceTermSet.VERY_NEAR).
                addInterpretation(AngleInterpretation.TURN_LEFT));
        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{1, 0, 1, 0, 0, 0}).
                setDescription("If L is very near, and LK is very near turn right").
                addAntecedent(DistanceTermSet.VERY_NEAR).addAntecedent(DistanceTermSet.VERY_NEAR).
                addInterpretation(AngleInterpretation.TURN_RIGHT));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 0, 0, 0, 1}).
                setDescription("If orientation is bad, turn left").
                addAntecedent(OrientationTermSet.BAD_ORIENTATION).
                addInterpretation(AngleInterpretation.TURN_LEFT));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 1, 1, 0, 0}).
                setDescription("If LK and RK is very near, turn right").
                addAntecedent(DistanceTermSet.VERY_NEAR).addAntecedent(DistanceTermSet.VERY_NEAR).
                addInterpretation(AngleInterpretation.TURN_RIGHT));
        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 1, 1, 0, 0}).
                setDescription("If LK and RK is very very near, turn right").
                addAntecedent(DistanceTermSet.VERY_VERY_NEAR).addAntecedent(DistanceTermSet.VERY_VERY_NEAR).
                addInterpretation(AngleInterpretation.TURN_RIGHT));

        rules.add(builder.setTNorm(tNorm).setActiveRules(new int[]{0, 0, 1, 1, 0, 0}).
                setDescription("If LK and RK is not near, keep going steady.").
                addAntecedent(DistanceTermSet.NOT_NEAR).addAntecedent(DistanceTermSet.NOT_NEAR).
                addInterpretation(AngleInterpretation.STEADY));


        AngleRules.angleRules = rules;
        AngleRules.tNorm = tNorm;

        return rules;
    }


    public static List<Rule> getRules(TNorm tNorm) {
        return initializeRules(tNorm);
    }
}
