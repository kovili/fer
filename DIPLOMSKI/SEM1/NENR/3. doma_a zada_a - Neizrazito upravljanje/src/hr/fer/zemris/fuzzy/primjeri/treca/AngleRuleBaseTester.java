package hr.fer.zemris.fuzzy.primjeri.treca;

import hr.fer.zemris.fuzzy.Debug;
import hr.fer.zemris.fuzzy.decoder.CenterOfAreaDecoder;
import hr.fer.zemris.fuzzy.rules.AngleRules;
import hr.fer.zemris.fuzzy.rules.Rule;
import hr.fer.zemris.fuzzy.set.IFuzzySet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

public class AngleRuleBaseTester {

    private final static int MAX_DIST = 100;
    private final static int MAX_VEL = 50;
    private final static int DISTANCE_UNIT = 5;

    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        Scanner s;

        int L=0,D=0,LK=0,DK=0,V=0,S=0;
        String line = null;

        List<Rule> angleRules = AngleRules.getRules(Math::min);
        int ruleIndex;

        while(true) {
            while (true) {
                System.out.println("Odaberite pravilo iz baze za odabir kuta kormila ili 'k' za zavrsetak: ");
                System.out.println();
                for (int i = 0; i < angleRules.size(); i++) {
                    System.out.println(i + ". -  " + angleRules.get(i));
                }
                if ((line = input.readLine()) != null) {
                    if(line.charAt(0)=='K') return;
                    s = new Scanner(line);
                    ruleIndex = s.nextInt();
                    if (ruleIndex >= 0 && ruleIndex < angleRules.size()) break;
                }
            }

            System.out.println("Unesite u jednom retku vrijednosti (L, D, LK, DK, V, S) odvojene razmakom.");

            if ((line = input.readLine()) != null) {
                s = new Scanner(line);
                L = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                D = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                LK = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                DK = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                V = Math.min(s.nextInt(), MAX_VEL);
                S = s.nextInt();
            }

            Rule rule = angleRules.get(ruleIndex);

            IFuzzySet result = rule.getResult(L, D, LK, DK, V, S);

            Debug.print(result, "Rezultatni skup:");
            System.out.println();
            System.out.println("Dekodirana vrijednost zakljucka je: " + new CenterOfAreaDecoder().decode(result));
            System.out.println();
        }
    }
}
