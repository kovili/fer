package hr.fer.zemris.fuzzy.language;

import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.domain.SimpleDomain;
import hr.fer.zemris.fuzzy.set.CalculatedFuzzySet;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.StandardFuzzySets;

public class DistanceTermSet {

    private static final IDomain velocityDomain = new SimpleDomain(0, 101);

    public static final IFuzzySet NEAR =
            new CalculatedFuzzySet(velocityDomain, StandardFuzzySets.lFunction(0, 15));

    public static final IFuzzySet NOT_NEAR =
            new CalculatedFuzzySet(velocityDomain, StandardFuzzySets.gammaFunction(10, 25));

    public static final IFuzzySet GOOD =
            new CalculatedFuzzySet(velocityDomain, StandardFuzzySets.lambdaFunction(15, 25, 35));

    public static final IFuzzySet VERY_NEAR = Grammar.very(NEAR);

    public static final IFuzzySet VERY_VERY_NEAR = Grammar.very(VERY_NEAR);
}
