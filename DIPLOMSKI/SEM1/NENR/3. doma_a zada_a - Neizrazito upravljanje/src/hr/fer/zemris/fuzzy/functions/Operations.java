package hr.fer.zemris.fuzzy.functions;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.MutableFuzzySet;

public class Operations {

    public static IUnaryFunction ZADEH_NOT = (value) -> 1 - value;
    public static IBinaryFunction ZADEH_AND = (first, second) -> Math.min(first, second);
    public static IBinaryFunction ZADEH_OR = (first, second) -> Math.max(first, second);

    public Operations() { }

    public static IFuzzySet unaryOperation(IFuzzySet set, IUnaryFunction function) {
        IDomain domain = set.getDomain();
        MutableFuzzySet fuzzySet = new MutableFuzzySet(domain);
        for(int i = 0; i < domain.getCardinality(); i++) {
            DomainElement element = domain.elementForIndex(i);
            fuzzySet.set(element, function.valueAt(set.getValueAt(element)));
        }
        return fuzzySet;
    }

    public static IFuzzySet binaryOperation(IFuzzySet first, IFuzzySet second, IBinaryFunction function) {
        MutableFuzzySet fuzzySet = new MutableFuzzySet(first.getDomain());
        for(int i = 0; i < first.getDomain().getCardinality(); i++) {
            DomainElement firstElement = first.getDomain().elementForIndex(i);
            DomainElement secondElement = second.getDomain().elementForIndex(i);
            fuzzySet.set(firstElement, function.valueAt(first.getValueAt(firstElement), second.getValueAt(secondElement)));
        }
        return fuzzySet;
    }

    public static IUnaryFunction zadehNot() {
        return ZADEH_NOT;
    }

    public static IBinaryFunction zadehAnd() {
        return ZADEH_AND;
    }

    public static IBinaryFunction zadehOr() {
        return ZADEH_OR;
    }

    public static IBinaryFunction hamacherTNorm(double vParameter) {
        return (first, second) -> (first * second) / (vParameter + (1 - vParameter)*(first + second - first * second));
    }

    public static IBinaryFunction hamacherSNorm(double vParameter) {
        return (first, second) -> (first + second - (2 - vParameter) * first * second) / (1 - (1 - vParameter) * first * second);
    }
}
