package hr.fer.zemris.fuzzy.engine;

import hr.fer.zemris.fuzzy.decoder.IFuzzyDecoder;

public class MinimumEngine extends SimpleEngine {

    public MinimumEngine(IFuzzyDecoder decoder) {
        super(Math::min, decoder);
    }
}
