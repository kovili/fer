package primjer;

import hr.fer.zemris.fuzzy.decoder.CenterOfAreaDecoder;
import hr.fer.zemris.fuzzy.engine.Engine;
import hr.fer.zemris.fuzzy.engine.SimpleEngine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

    private final static int MAX_DIST = 100;
    private final static int MAX_VEL = 50;
    private final static int DISTANCE_UNIT = 5;


    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        Engine engine = new SimpleEngine(new CenterOfAreaDecoder());

        int L=0,D=0,LK=0,DK=0,V=0,S=0,akcel,kormilo;
        String line = null;
        while(true){
            if((line = input.readLine())!=null){
                if(line.charAt(0)=='K') break;
                Scanner s = new Scanner(line);
                L = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                D = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                LK = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                DK = Math.min(s.nextInt() / DISTANCE_UNIT, MAX_DIST);
                V = Math.min(s.nextInt(), MAX_VEL);
                S = s.nextInt();

            }

            // fuzzy magic ...

            akcel = engine.inferVelocity(L, D, LK, DK, V, S); kormilo = engine.inferRotation(L, D, LK, DK, V, S);
            System.out.println(akcel + " " + kormilo);
            System.out.flush();
        }
    }

}

