package hr.fer.zemris.fuzzy.functions;

public interface IUnaryFunction {

    double valueAt(double a);
}
