package hr.fer.zemris.fuzzy.functions;

public interface IIntUnaryFunction {

    double valueAt(int index);
}
