package hr.fer.zemris.fuzzy.domain;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleDomain extends Domain {

    private int first;
    private int last;

    public SimpleDomain(int first, int last) {
        if(first > last) {
            this.last = first;
            this.first = last;
        } else {
            this.first = first;
            this.last = last;
        }
    }

    @Override
    public int getCardinality() {
        return last - first;
    }

    @Override
    public IDomain getComponent(int index) {
        if(index == 0) return this;
        return null;
    }

    @Override
    public int getNumberOfComponents() {
        return 1;
    }


    @Override
    public Iterator<DomainElement> iterator() {
        return new Iterator<>() {
            private int current = first;

            @Override
            public boolean hasNext() { return current < last; }

            @Override
            public DomainElement next() {
                if(hasNext()) {
                    return DomainElement.of(current++);
                }
                throw new NoSuchElementException("No more elements");
            }
        };
    }

    public int getFirst() {
        return first;
    }

    public int getLast() {
        return last;
    }
}
