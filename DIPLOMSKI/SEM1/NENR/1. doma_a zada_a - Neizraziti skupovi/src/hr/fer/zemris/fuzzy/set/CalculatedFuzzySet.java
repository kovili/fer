package hr.fer.zemris.fuzzy.set;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.functions.IIntUnaryFunction;

import java.util.Objects;

public class CalculatedFuzzySet implements IFuzzySet {

    private IDomain domain;
    private IIntUnaryFunction function;

    public CalculatedFuzzySet(IDomain domain, IIntUnaryFunction function) {
        Objects.requireNonNull(domain);
        Objects.requireNonNull(function);
        this.domain = domain;
        this.function = function;
    }

    @Override
    public IDomain getDomain() {
        return domain;
    }

    @Override
    public double getValueAt(DomainElement element) {
        int index = domain.indexOfElement(element);
        if(index ==  -1) {
            return -1;
        } else {
            return function.valueAt(index);
        }
    }
}
