package hr.fer.zemris.fuzzy.set;

import hr.fer.zemris.fuzzy.functions.IIntUnaryFunction;

public class StandardFuzzySets {

    public StandardFuzzySets() { }

    public static IIntUnaryFunction lFunction(int alpha, int beta) {
        if(alpha > beta) return lFunction(beta, alpha);
        return (index) -> {
            if(index < alpha) {
                return 1;
            } else if(index >= beta) {
                return 0;
            } else {
                return ((double) beta - index)/(beta - alpha);
            }
        };
    }

    public static IIntUnaryFunction gammaFunction(int alpha, int beta) {
        if(alpha > beta) return gammaFunction(beta, alpha);
        return (index) -> {
            if(index < alpha) {
                return 0;
            } else if(index >= beta) {
                return 1;
            } else {
                return ((double) index - alpha)/(beta - alpha);
            }
        };
    }

    public static IIntUnaryFunction lambdaFunction(int alpha, int beta, int gamma) {
        if(alpha > beta) return lambdaFunction(beta, alpha, gamma);
        if(alpha > gamma) return lambdaFunction(gamma, beta, alpha);
        if(beta > gamma) return lambdaFunction(alpha, gamma, beta);
        return (index) -> {
            if(index < alpha) {
                return 0;
            } else if(index >= alpha && index < beta){
                return ((double) index - alpha)/(beta - alpha);
            } else if(index >= beta && index < gamma){
                return ((double) gamma - index)/(gamma - beta);
            } else if(index >= gamma) {
                return 0;
            }
            return -1;
        };
    }

}
