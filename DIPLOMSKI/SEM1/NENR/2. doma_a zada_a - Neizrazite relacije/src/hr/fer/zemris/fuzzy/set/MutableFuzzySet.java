package hr.fer.zemris.fuzzy.set;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.IDomain;

import java.util.NoSuchElementException;
import java.util.Objects;

public class MutableFuzzySet implements IFuzzySet {

    private double []memberships;
    private IDomain domain;

    public MutableFuzzySet(IDomain domain) {
        Objects.requireNonNull(domain);
        this.domain = domain;
        memberships = new double[this.domain.getCardinality()];
    }

    @Override
    public IDomain getDomain() {
        return domain;
    }

    @Override
    public double getValueAt(DomainElement element) {
        int index = domain.indexOfElement(element);
        if(index ==  -1) {
            return -1;
        } else {
            return memberships[index];
        }
    }

    public MutableFuzzySet set(DomainElement element, double value) {
        int index = domain.indexOfElement(element);
        if(index != -1) {
            memberships[index] = value;
        } else {
            throw new NoSuchElementException("Element " + element + " is not in the domain.");
        }
        return this;
    }

}
