package hr.fer.zemris.fuzzy.domain;

import java.util.Iterator;

public class Domain implements IDomain {

    public Domain() { }

    @Override
    public int getCardinality() {
        return 0;
    }

    @Override
    public IDomain getComponent(int index) {
        return null;
    }

    @Override
    public int getNumberOfComponents() {
        return 0;
    }

    @Override
    public int indexOfElement(DomainElement other) {
        if(other == null) return -1;
        int index = 0;
        for(DomainElement element : this) {
            if(element.equals(other)) return index;
            index++;
        }
        return -1;
    }

    @Override
    public DomainElement elementForIndex(int index) {
        if(index < 0) return null;
        for(DomainElement element : this) {
            if(index-- == 0) return element;
        }
        return null;
    }

    public static IDomain intRange(int first, int last) {
        return new SimpleDomain(first, last);
    }

    public static IDomain combine(IDomain first, IDomain second) {
        SimpleDomain []domains = new SimpleDomain[first.getNumberOfComponents() + second.getNumberOfComponents()];
        for(int i = 0; i < first.getNumberOfComponents(); i++) {
            domains[i] = (SimpleDomain) first.getComponent(i);
        }

        for(int i = 0, j = first.getNumberOfComponents(); i < second.getNumberOfComponents(); i++) {
            domains[i + j] = (SimpleDomain) second.getComponent(i);
        }

        return new CompositeDomain(domains);
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return null;
    }
}
