package hr.fer.zemris.fuzzy;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.set.IFuzzySet;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Debug {

    public static void print(IDomain domain, String headingText) {
        if(headingText!=null) {
            System.out.println(headingText);
        }
        for(DomainElement e : domain) {
            System.out.println("Element domene: " + e);
        }
        System.out.println("Kardinalitet domene je: " + domain.getCardinality());
        System.out.println();
    }

    public static void print(IFuzzySet set, String headingText) {
        if(headingText!=null) {
            System.out.println(headingText);
        }
        IDomain domain = set.getDomain();
        for(int i = 0; i < domain.getCardinality(); i++) {
            DomainElement element = domain.elementForIndex(i);
            System.out.println("d(" + element + ")=" + String.format(Locale.ROOT, "%.6f", set.getValueAt(element)));
        }
        System.out.println();
    }
}
