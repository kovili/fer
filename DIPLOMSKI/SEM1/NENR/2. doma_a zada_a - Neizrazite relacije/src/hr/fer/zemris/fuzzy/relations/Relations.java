package hr.fer.zemris.fuzzy.relations;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.set.IFuzzySet;
import hr.fer.zemris.fuzzy.set.MutableFuzzySet;

public class Relations {

    public static boolean isUtimesURelation(IFuzzySet relation) {
        if(relation.getDomain().getNumberOfComponents() != 2) return false;

        IDomain first = relation.getDomain().getComponent(0);
        IDomain second = relation.getDomain().getComponent(1);

        return first.equals(second);
    }

    public static boolean isSymmetric(IFuzzySet relation) {
        if(!isUtimesURelation(relation)) return false;

        IDomain domain = relation.getDomain();
        int relationDimension = domain.getComponent(0).getCardinality();

        for(int i = 0; i < relationDimension; i++) {
            for(int j = i + 1; j < relationDimension; j++) {
                double first = relation.getValueAt(domain.elementForIndex(i * relationDimension + j));
                double second = relation.getValueAt(domain.elementForIndex(j * relationDimension + i));
                if(Math.abs(first - second) >10E-6) return false;
            }
        }
        return true;
    }

    public static boolean isReflexive(IFuzzySet relation) {
        if(!isUtimesURelation(relation)) return false;

        IDomain domain = relation.getDomain();
        int relationDimension = domain.getComponent(0).getCardinality();

        for(int i = 0; i < relationDimension; i++) {
            if(Math.abs(relation.getValueAt(domain.elementForIndex(i * relationDimension + i)) - 1) > 10E-6) {
                return false;
            }
        }
        return true;
    }

    public static boolean isMaxMinTransitive(IFuzzySet relation) {
        if(!isUtimesURelation(relation)) return false;

        IDomain domain = relation.getDomain();
        int relationDimension = domain.getComponent(0).getCardinality();

        for(int i = 0; i < relationDimension; i++) {
            for(int j = 0; j < relationDimension; j++) {
                double relationValue = relation.getValueAt(domain.elementForIndex(i * relationDimension + j));
                double maxMinValue = Double.NEGATIVE_INFINITY;

                for(int k = 0; k < relationDimension; k++) {
                    double first = relation.getValueAt(domain.elementForIndex(i * relationDimension + k));
                    double second = relation.getValueAt(domain.elementForIndex(k * relationDimension + j));
                    double result = Math.min(first, second);
                    maxMinValue = Math.max(maxMinValue, result);
                }

                if((relationValue - maxMinValue + 10E-9) < 0) return false;
            }
        }
        return true;
    }

    public static IFuzzySet compositionOfBinaryRelations(IFuzzySet first, IFuzzySet second) {
        if(first.getDomain().getNumberOfComponents() != 2 || second.getDomain().getNumberOfComponents() != 2) {
            throw new IllegalArgumentException("Relations must be 2 dimensional.");
        }
        IDomain compositeFirst = first.getDomain();
        IDomain compositeSecond = second.getDomain();

        if(!compositeFirst.getComponent(1).equals(compositeSecond.getComponent(0))) {
            throw new IllegalArgumentException("First relation's second domain must be equal to the second relation's first domain.");
        }

        MutableFuzzySet result = new MutableFuzzySet(
                Domain.combine(compositeFirst.getComponent(0), compositeSecond.getComponent(1)));

        int firstMatrixHeight = compositeFirst.getComponent(0).getCardinality();
        int commonDimension = compositeFirst.getComponent(1).getCardinality();
        int secondMatrixWidth = compositeSecond.getComponent(1).getCardinality();

        for(int i = 0; i < firstMatrixHeight; i++) {
            for(int j = 0; j < secondMatrixWidth; j++) {
                double maxMinResult = Double.NEGATIVE_INFINITY;
                for(int k = 0; k < commonDimension; k++) {
                    double firstMin = first.getValueAt(compositeFirst.elementForIndex(i * commonDimension + k));
                    double secondMin = second.getValueAt(compositeSecond.elementForIndex(k * secondMatrixWidth + j));
                    maxMinResult = Math.max(maxMinResult, Math.min(firstMin, secondMin));
                }
                result.set(result.getDomain().elementForIndex(i * secondMatrixWidth + j), maxMinResult);
            }
        }

        return result;
    }

    public static boolean isFuzzyEquivalence(IFuzzySet relation) {
        return isReflexive(relation) && isSymmetric(relation) && isMaxMinTransitive(relation);
    }


}
