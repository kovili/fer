package hr.fer.zemris.fuzzy.domain;

import java.util.Arrays;

public class DomainElement {

    private int []values;

    public DomainElement(int ...values) {
        if(values == null || values.length == 0) {
            throw new IllegalArgumentException("Values must be array with at least one number.");
        }
        this.values = values.clone();
    }

    public int getNumberOfComponents() {
        return values.length;
    }

    public int getComponentValue(int index) {
        if(index >= values.length) {
            throw new ArrayIndexOutOfBoundsException("Index out of bounds for domain element " + this + " and index " + index);
        }
        return values[index];
    }

    public static DomainElement of(int ...values) {
        return new DomainElement(values);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainElement that = (DomainElement) o;
        return Arrays.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }

    @Override
    public String toString() {
        if(values.length == 1) return "" + values[0];
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        for(int i = 0; i < values.length - 1; i++) {
            builder.append(values[i]).append(",");
        }
        builder.append(values[values.length - 1]).append(")");
        return builder.toString();
    }
}
