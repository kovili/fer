from helper import *
from neuro_genalg import NeuroGeneticAlgorithm


def main():
    try:
        dataset_filename = "dataset.txt"
        neuro_genalg = NeuroGeneticAlgorithm()
        best_solution = neuro_genalg.learn(layers="2x8x3")
        log_best_solution(best_solution, neuro_genalg.neural_network.layers, "best")
        plot_best_solution(best_solution, neuro_genalg.neural_network.layers, dataset_filename, True, "best")
        compare_results(dataset_filename, neuro_genalg.neural_network)
    except:
        print("Ups! Nešto je pošlo po krivu. Sada ću umrijeti!")
        exit(1)


if __name__ == '__main__':
    main()
