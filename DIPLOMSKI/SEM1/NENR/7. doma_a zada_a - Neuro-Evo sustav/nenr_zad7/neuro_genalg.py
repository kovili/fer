from neural_network import NeuralNetwork
import math
from genalg_crossover import *
from genalg_mutation import *

class NeuroGeneticAlgorithm:
    def __init__(self, mutation_desirabilities=(0.73, 0.25, 0.07), mutation_probabilities=(0.03, 0.02, 0.02),
                 mutation_sigmas=(0.3, 0.8, 1.5), generations=300000, pop_size=50, epsilon=1E-7, show_progress=True):
        self.mutation_desirabilities = calculate_desirability_as_probability(mutation_desirabilities)
        self.mutation_probabilities = mutation_probabilities
        self.mutation_sigmas = mutation_sigmas
        self.generations = generations
        self.pop_size = pop_size
        self.epsilon = epsilon
        self.crossover_operators = [simple_arithmetic_recombination, whole_arithmetic_recombination, discrete_recombination]
        self.mutation_operators = [normal_distribution_add, normal_distribution_add, normal_distribution_replace]
        self.best_epsilon = math.inf
        self.best_solution = None
        self.show_progress = show_progress

    def learn(self, layers="2x8x3", dataset_filename="dataset.txt"):
        X, y = extract_samples(dataset_filename)
        self.neural_network = NeuralNetwork(layers, X, y)
        self.generate_population()
        for i in range(self.generations):
            for iteration in range(self.pop_size):
                participants = self.pick_participants()
                first, second, third, scores = self.evaluate_participants(participants[0], participants[1], participants[2])
                self.crossover(first, second, third)
                self.mutate(self.population[third])
                self.population_score[third] = self.neural_network.calculate_error(self.population[third])
                if scores[0] > -self.best_epsilon:
                    self.best_epsilon = -scores[0]
                    self.best_solution = self.population[first]
                if scores[0] > -self.epsilon:
                    print("Learning done, found satisfactory solution!")
                    return self.best_solution
            if i % 10 == 0:
                print(f"Generation {i}, best epsilon={self.best_epsilon}")
            if i % 1000 == 0 and i > 0:
                if self.show_progress:
                    plot_best_solution(self.best_solution, self.neural_network.layers, "dataset.txt", save_plot=True, iterations=i)
                    log_best_solution(self.best_solution, self.neural_network.layers, i)

        print(f"Learning done, best solution has MSE = {self.best_epsilon}")
        return self.best_solution

    def generate_population(self):
        self.population = []
        self.population_score = []
        solution_size = self.neural_network.get_parameter_count()
        for individual_index in range(self.pop_size):
            self.population.append(np.random.random(solution_size))
            self.population_score.append(self.neural_network.calculate_error(self.population[individual_index]))

    def pick_participants(self):
        first, second, third = np.random.randint(0, self.pop_size), np.random.randint(0,
                                                                                      self.pop_size), np.random.randint(
            0, self.pop_size)
        second = (second + 1) % self.pop_size if second == first else second
        third = (third + 1) % self.pop_size if third == first else third
        third = (third + 1) % self.pop_size if third == second else third
        return first, second, third

    def evaluate_participants(self, first, second, third):
        scores = [-self.population_score[first],
                  -self.population_score[second],
                  -self.population_score[third]]
        self.population_score[first] = -scores[0]
        self.population_score[second] = -scores[1]
        self.population_score[third] = -scores[2]
        scores, ranking_list = (list(t) for t in zip(*sorted(zip(scores, [first, second, third]), reverse=True)))
        return ranking_list[0], ranking_list[1], ranking_list[2], scores

    def crossover(self, first, second, third):
        chosen_crossover_operator = np.random.choice(self.crossover_operators)
        self.population[third] = chosen_crossover_operator(self.population[first], self.population[second])

    def mutate(self, third):
        desirability_point = np.random.random()
        curr_desirability_sum = 0
        chosen_mutation = 0
        for desirability in self.mutation_desirabilities:
            curr_desirability_sum += desirability
            if desirability_point < curr_desirability_sum:
                self.mutation_operators[chosen_mutation](third,
                                                         self.mutation_probabilities[chosen_mutation],
                                                         self.mutation_sigmas[chosen_mutation])
                return
            chosen_mutation += 1
        raise RuntimeError("Did not pick a single mutation.")
