import re
import numpy as np
import matplotlib.pyplot as plt
import os


def extract_samples(file):
    try:
        sample_file = open(file, "r")
        X = []
        y = []
        for line in sample_file:
            parts = re.split("\\s+", line)
            X.append(parts[0:2])
            y.append(parts[2:5])
    except:
        print(f"Datoteka {file} se ne nalazi u ovom direktoriju. Stavite je te pokrenite program ponovo.")
        raise RuntimeError()
    return X, y


def calculate_square_error(mistake_vector):
    return np.dot(mistake_vector, mistake_vector)


def calculate_mistake_vector(output_vector, sample_output):
    return output_vector - sample_output


def plot_for_fixed_params():
    X = np.linspace(-8, 10, 40000)
    sim_func = lambda x, w, s: 1 / (1 + (abs((x - w)) / abs(s)))
    plt.clf()
    plt.plot(X, sim_func(X, 2, 0.25), 'b', label="s = 0.25")
    plt.plot(X, sim_func(X, 2, 1), 'r', label="s = 1")
    plt.plot(X, sim_func(X, 2, 4), 'g', label="s = 4")
    plt.xlabel("x")
    plt.ylabel("Izlaz")
    plt.legend()
    plt.title(f"Izlaz neurona za w=2 i sϵ{0.25, 1, 4}")
    return plt


def get_learn_data(filename):
    X, y = extract_samples(filename)
    first_class_data = ([], [])
    second_class_data = ([], [])
    third_class_data = ([], [])
    for sample in zip(X, y):
        coor_x = sample[0][0]
        coor_y = sample[0][1]
        class_index = sample[1].index('1')
        if class_index == 0:
            first_class_data[0].append(round(float(coor_x), 3))
            first_class_data[1].append(round(float(coor_y), 3))
        elif class_index == 1:
            second_class_data[0].append(round(float(coor_x), 3))
            second_class_data[1].append(round(float(coor_y), 3))
        else:
            third_class_data[0].append(round(float(coor_x), 3))
            third_class_data[1].append(round(float(coor_y), 3))

    return first_class_data, second_class_data, third_class_data


def plot_learn_data(filename):
    first_class_data, second_class_data, third_class_data = get_learn_data(filename)
    plt.clf()
    plt.figure(figsize=(32, 16))
    plt.plot(first_class_data[0], first_class_data[1], 'go', markersize=18, label="Klasa 1")
    plt.plot(second_class_data[0], second_class_data[1], 'b^', markersize=18, label="Klasa 2")
    plt.plot(third_class_data[0], third_class_data[1], 'rx', markersize=18, label="Klasa 3")
    plt.title("Uzorci za učenje")
    plt.legend(loc="best")
    return plt


def calculate_desirability_as_probability(mutation_desirabilities):
    desirability_sum = sum(mutation_desirabilities)
    new_desirabilities = [mutation_desirabilities[i] / desirability_sum for i in range(len(mutation_desirabilities))]
    return new_desirabilities


def extract_medians(solution, layers):
    median_count = layers[1]
    solution_x, solution_y = [], []
    for solution_index in range(median_count):
        solution_x.append(solution[4 * solution_index])
        solution_y.append(solution[4 * solution_index + 1])
    return solution_x, solution_y


def create_filename(layers, iterations):
    filename = ""
    for index in range(len(layers)):
        if index != 0:
            filename += "x"
        filename += str(layers[index])
    filename += "it" + str(iterations)
    return filename


def create_dir_name(layers):
    filename = ""
    for index in range(len(layers)):
        if index != 0:
            filename += "x"
        filename += str(layers[index])
    return filename


def plot_best_solution(solution, layers, output_file, save_plot=False, iterations=0):
    solution_x, solution_y = extract_medians(solution, layers)
    plt = plot_learn_data(output_file)
    plt.plot(solution_x, solution_y, 'y*', markersize=18)
    if save_plot:
        filename = create_filename(layers, iterations)
        dir = create_dir_name(layers)
        if not os.path.isdir(dir):
            os.mkdir(dir)
        plt.savefig(dir + "/" + filename + ".png")
    return plt


def log_best_solution(solution, layers, iterations):
    dir = create_dir_name(layers)
    if not os.path.isdir(dir):
        os.mkdir(dir)
    with open(dir + "/" + create_filename(layers, iterations) + '.log', 'w') as file_handle:
        for number in solution:
            file_handle.write('%s\n' % number)


def load_best_solution(layers):
    dir = create_dir_name(layers)
    if not os.path.isdir(dir):
        raise RuntimeError(f"Directory {dir} does not exist!")
    best_solution = []
    with open(dir + "/" + create_filename(layers, "best") + ".log") as best_log:
        for line in best_log:
            best_solution.append(float(line))
    return best_solution


def compare_results(learn_data_filename, neural_network):
    X, y = extract_samples(learn_data_filename)
    best_solution = load_best_solution(neural_network.layers)
    total, correct = len(X), 0
    for sample in zip(X, y):
        network_output = neural_network.calculate_output([float(sample[0][0]), float(sample[0][1])], best_solution)[-1]
        network_output = [1 if network_output[i] > 0.5 else 0 for i in range(3)]
        classes = [int(num) for num in sample[1]]
        print(f"Uzorak: x = {round(float(sample[0][0]), 3)}, y = {round(float(sample[0][1]), 3)}, class = {classes}")
        print(f"Klasifikacija neuronske mreže: {network_output}.")
        if classes == network_output:
            print("Uzorak TOČNO klasificiran.")
            correct += 1
        else:
            print("Uzorak NETOČNO klasificiran.")
    print(f"Correct: {correct} / Total: {total}")
