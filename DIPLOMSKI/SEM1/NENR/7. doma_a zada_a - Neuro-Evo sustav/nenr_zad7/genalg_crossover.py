from helper import *
import random

def whole_arithmetic_recombination(mother, father):
    child = np.copy(mother)
    for i in range(len(mother)):
        child[i] = (mother[i] + father[i]) / 2
    return child


def simple_arithmetic_recombination(mother, father):
    point_break = np.random.randint(0, len(mother))
    child = np.copy(mother)
    for i in range(point_break, len(mother)):
        child[i] = ((mother[i] + father[i]) / 2)
    return child


def single_arithmetic_recombination(mother, father):
    point_break = np.random.randint(0, len(mother))
    child = np.copy(mother)
    child[point_break] = ((mother[point_break] + father[point_break]) / 2)
    return child


def discrete_recombination(mother, father):
    child = np.array(mother)
    for index in range(len(mother)):
        child[index] = mother[index] if random.getrandbits(1) else father[index]
    return child
