from helper import *


# noinspection PyAttributeOutsideInit
class NeuralNetwork:
    def __init__(self, layers, inputs, outputs):
        self.layers = list(map(lambda x: int(x), layers.split("x")))

        if self.layers[-1] != len(outputs[0]) or self.layers[0] != len(inputs[0]):
            raise RuntimeError(
                "First and last layer must be (" + str(len(inputs[0])) + ", " + str(len(self.outputs[0])) + "), but are (" + str(layers[0]) + "," + str(
                    layers[-1]) + ")!")
        if len(self.layers) < 3:
            raise RuntimeError("Must have at least 3 layers!")

        self.inputs = np.transpose(np.array(list(map(lambda x: list(map(lambda el: float(el), x)), inputs)), ndmin=2))
        self.outputs = np.transpose(np.array(list(map(lambda x: list(map(lambda el: float(el), x)), outputs)), ndmin=2))
        self.sigmoid = lambda net: 1 / (1 + np.exp(-net))
        self.parameter_count = 4 * self.layers[1] + sum((self.layers[i - 1] + 1) * self.layers[i] for i in range(2, len(self.layers)))

    def calculate_error(self, parameters):
        sample_size = len(self.inputs[0])
        total_square_error = 0

        # Calculate mistake
        for sample_index in range(sample_size):
            curr_network_outputs = self.calculate_output(self.inputs[:, sample_index], parameters)
            curr_mistake_vector = calculate_mistake_vector(
                np.transpose(curr_network_outputs[-1]), self.outputs[:, sample_index])
            total_square_error += calculate_square_error(curr_mistake_vector)

        mean_square_error = total_square_error / sample_size
        return mean_square_error

    def calculate_output(self, vector_x, parameters):
        if len(parameters) != self.parameter_count:
            raise RuntimeError("Number of params must be equal to parameter count.")
        network_outputs = [np.transpose(vector_x)]

        output_vector = []
        for neuron_index in range(self.layers[1]):
            used_params = neuron_index * 4
            point_1, point_2, slack_1, slack_2 = parameters[used_params], parameters[used_params + 1], parameters[used_params + 2], parameters[used_params + 3]
            output_vector.append(1 / (1 + abs((vector_x[0] - point_1) / slack_1) + abs((vector_x[1] - point_2) / slack_2)))
        network_outputs.append(np.transpose(output_vector))

        used_params = 4 * self.layers[1]

        for layer_index in range(2, len(self.layers)):
            output_vector = []
            last_layer_count = self.layers[layer_index - 1]
            for neuron_index in range(self.layers[layer_index]):
                net_sum = sum(parameters[(used_params + 1) + i] * network_outputs[-1][i] for i in range(last_layer_count)) + parameters[used_params]
                output_vector.append(self.sigmoid(net_sum))
                used_params += (last_layer_count + 1)

            network_outputs.append(np.transpose(output_vector))
        return network_outputs

    def get_parameter_count(self):
        return self.parameter_count

