from helper import *
import random

def normal_distribution_add(solution, mutation_chance, sigma):
    for index in range(solution.shape[0]):
        solution[index] += random.gauss(0, sigma) if np.random.random() < mutation_chance else 0
    return solution


def normal_distribution_replace(solution, mutation_chance, sigma):
    for index in range(solution.shape[0]):
        solution[index] = random.gauss(0, sigma) if np.random.random() < mutation_chance else solution[index]
    return solution
