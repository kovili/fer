#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_NODE_COUNT 1000
#define NODE_SPACE 4
#define NODE_DRAW_SPACE (NODE_SPACE + 2)
#define BUFF_SIZE 1024

typedef struct node{
	struct node *parent;
	struct node *left;
	struct node *right;
	int value;
	int balance_factor;
} node;

void error(char *msg) {
	fprintf(stderr, "%s", msg);
	exit(1);
}

char *read_whole_file(FILE *fp) {
	long fsize;
	char *text;
	
	fseek(fp, 0, SEEK_END);
	fsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	
	text = malloc(fsize + 1);
	
	fread(text, 1, fsize, fp);
	fclose(fp);
	text[fsize] = 0;
	
	return text;
}

int extract_numbers(char *text, int *numbers) {
	int i, extracted;
	char *end;
	for(i = 0; i < MAX_NODE_COUNT; i++) {
		extracted = strtol(text, &end, 10);
		if(text == end) {
			return i;
		}
		text = end;
		numbers[i] = extracted;
	}
	return MAX_NODE_COUNT;
}


int read_values(char *filename, int *numbers) {
	FILE *fp;
	char *text;
	int node_count, i;
	
	if(access(filename, R_OK) != 0) {
		error("Ne mogu citati iz datoteke!\n");
	}
	
	fp = fopen(filename, "r");
	
	if(fp == NULL) {
		error("GRESKA");
	}
	
	text = read_whole_file(fp);
	node_count = extract_numbers(text, numbers);
	
	free(text);
	fclose(fp);
	
	printf("Broj cvorova je: %d\n", node_count);
	return node_count;
}

node *create_node(int value) {
	node *new_node = malloc(sizeof(node));
	memset(new_node, 0, sizeof(node));
	new_node->value = value;
	new_node->balance_factor = 0;
	return new_node;
}

int calculate_depth(node *head) {
	int left_depth = 0, right_depth = 0, tree_depth = 0;
	
	if(head == NULL) {
		return tree_depth;
	}
	
	left_depth = calculate_depth(head->left);
	right_depth = calculate_depth(head->right);
	
	tree_depth = left_depth < right_depth ? right_depth : left_depth;
	
	return tree_depth + 1;
}

void print_node(node *head, char *canvas, int offset, int current_depth, int max_depth, int canvas_width, int canvas_height, int right_child) {
	char value[NODE_DRAW_SPACE];	
	int added_offset = (1 << (max_depth - current_depth - 1));

	if(head == NULL) return;
	
	snprintf(value, NODE_DRAW_SPACE, "|%04d|", head->value);
	strncpy(canvas + offset, value, NODE_DRAW_SPACE);
	
	if(current_depth + 1 <= max_depth) {
		print_node(head->left, canvas, offset + (canvas_width * 3) - added_offset * NODE_DRAW_SPACE, current_depth + 1, max_depth, canvas_width, canvas_height, 0);
		print_node(head->right, canvas, offset + (canvas_width * 3) + added_offset * NODE_DRAW_SPACE, current_depth + 1, max_depth, canvas_width, canvas_height, 1);
	}
	
	if(current_depth != 1) {
		strncpy(canvas + offset - canvas_width + (NODE_DRAW_SPACE / 2), right_child ? "\\" : "/", 1);
		strncpy(canvas + offset - 2 * canvas_width + (NODE_DRAW_SPACE / 2) + (right_child ? -1 : 1), "_", 1);
	}
	
}

void print_tree(node **head) {
	int depth = calculate_depth(*head), draw_width = 1, draw_height = 1, center, i;
	char *canvas, number[5];
	
	if(depth == 0) {
		printf("PRAZNO STABLO\n");
	}
	
	//CALCULATE HEIGHT/WIDTH
	draw_width = ((draw_width << depth) - 1);
	draw_height = depth * 3;
	draw_width *= NODE_DRAW_SPACE;

	
	//SET CANVAS
	canvas = malloc(draw_width * draw_height + 1);
	memset(canvas, '.', draw_height * draw_width);
	canvas[draw_height * draw_width] = 0;

	
	print_node(*head, canvas, NODE_DRAW_SPACE * ((1 << (depth - 1)) - 1),  1, depth, draw_width, draw_height, 0);
	
		for(i = 1; i <= draw_height; i++) {
		canvas[i * draw_width - 1] = '\n';
	}
	
	printf("%s", canvas);
	free(canvas);
}

void print_balance(node *head) {	
	if(head == NULL) return;
	
	printf("%d - balance %d\n", head->value, head->balance_factor);
	
	print_balance(head->left);
	print_balance(head->right);
}

void rotate_node_right(node *root, node **head) {
	int root_is_right;
	node *pivot = root->left, *root_parent = root->parent;

	//CONNECT ROOT_PARENT TO PIVOT IF ROOT_PARENT EXISTS	
	if(root_parent != NULL) {
		root_is_right = root_parent->right == root ? 1 : 0;
		if(root_is_right) {
			root_parent->right = pivot;
		} else {
			root_parent->left = pivot;
		}
		pivot->parent = root_parent;
	}
	
	root->left = pivot->right;
	if(pivot->right != NULL) pivot->right->parent = root;
	root->balance_factor = 0;
	pivot->right = root;
	root->parent = pivot;
	pivot->balance_factor = 0;
	
	if(root == *head) {
		*head = pivot;
		pivot->parent = NULL;
	}
}

void rotate_node_left(node *root, node **head) {
	int root_is_left;
	node *pivot = root->right, *root_parent = root->parent;
	
	//CONNECT ROOT_PARENT TO PIVOT IF ROOT_PARENT EXISTS
	if(root_parent != NULL) {
		root_is_left = root_parent->left == root ? 1 : 0;
		if(root_is_left) {
			root_parent->left = pivot;
		} else {
			root_parent->right = pivot;
		}
		pivot->parent = root_parent;
	}
	
	root->right = pivot->left;
	if(pivot->left != NULL) pivot->left->parent = root;
	root->balance_factor = 0;
	pivot->left = root;
	root->parent = pivot;
	pivot->balance_factor = 0;
	
	if(root == *head) {
		*head = pivot;
		pivot->parent = NULL;
	}
}

void rotate_node_rightleft(node *root, node **head) {
	node *small_pivot = root->right->left, *small_root = root->right;
	int small_pivot_weight = small_pivot->balance_factor;
	
	//RIGHT
	rotate_node_right(small_root, head);
	
	//LEFT
	if(root->parent != NULL){
		if(root->parent->left == root) {
			root->parent->left = small_pivot;
		} else {
			root->parent->right = small_pivot;
		}
		small_pivot->parent = root->parent;
	}
	
	small_pivot->right = small_root;
	small_root->parent = small_pivot;
	
	root->right = small_pivot->left;
	if(small_pivot->left != NULL) small_pivot->left->parent = root;
	
	small_pivot->left = root;
	root->parent = small_pivot;
	
	//Recalculating balance
	small_pivot->balance_factor = 0;
	small_root->balance_factor = small_pivot_weight == 1 ? 0 : 1;
	root->balance_factor = small_pivot_weight == 1 ? -1 : 0;
	small_root->balance_factor = small_pivot_weight == 0 ? 0 : small_root->balance_factor;
	root->balance_factor = small_pivot_weight == 0 ? 0 : root->balance_factor;
	
	if(root == *head) {
		*head = small_pivot;
		small_pivot->parent = NULL;
	}
}

void rotate_node_leftright(node *root, node **head) {
	node *small_pivot = root->left->right, *small_root = root->left;
	int small_pivot_weight = small_pivot->balance_factor;
	
	//RIGHT
	rotate_node_left(small_root, head);
	
	//LEFT
	if(root->parent != NULL){
		if(root->parent->right == root) {
			root->parent->right = small_pivot;
		} else {
			root->parent->left = small_pivot;
		}
		small_pivot->parent = root->parent;
	}
	
	small_pivot->left = small_root;
	small_root->parent = small_pivot;
	
	root->left = small_pivot->right;
	if(small_pivot->right != NULL) small_pivot->right->parent = root;
	
	small_pivot->right = root;
	root->parent = small_pivot;
	
	//Recalculating balance
	small_pivot->balance_factor = 0;
	small_root->balance_factor = small_pivot_weight == 1 ? -1 : 0;
	root->balance_factor = small_pivot_weight == 1 ? 0 : 1;
	small_root->balance_factor = small_pivot_weight == 0 ? 0 : small_root->balance_factor;
	root->balance_factor = small_pivot_weight == 0 ? 0 : root->balance_factor;
	
	if(root == *head) {
		*head = small_pivot;
		small_pivot->parent = NULL;
	}
}

node *connect_child(node *parent, int value, int right) {
	node *child = create_node(value);
	if(right) {
		parent->right = child;
	} else {
		parent->left = child;
	}
	child->parent = parent;
	return child;
}

int insert_node(node **head, int value) {
	node *child, *traveler;
	int added_weight;
	if(*head == NULL) {
		*head = create_node(value);
		return (*head)->balance_factor;
	}
	
	traveler = *head;
	while(1) {
		if(traveler->value > value) {
			//INSERT CHILD
			if(traveler->left == NULL) {
				child = connect_child(traveler, value, 0);
				break;
			} else {
				traveler = traveler->left;
			}
		} else {
			//INSERT CHILD
			if(traveler->right == NULL) {
				child = connect_child(traveler, value, 1);
				break;
			} else {
				traveler = traveler->right;
			}
		}
	}
	
	traveler = child->parent;
	while(traveler != NULL) {
		added_weight = child == traveler->left ? -1 : 1;
		traveler->balance_factor += added_weight;
		
		if(traveler->balance_factor == 0) break;
		
		if(traveler->balance_factor == 2) {
			if(child->balance_factor > 0) {
				rotate_node_left(traveler, head);
			} else {
				rotate_node_rightleft(traveler, head);
			}
			break;
		} else if(traveler->balance_factor == -2) {
			if(child->balance_factor < 0) {
				rotate_node_right(traveler, head);
			} else {
				rotate_node_leftright(traveler, head);
			}
			break;
		}
		
		child = traveler;
		traveler = traveler->parent;
		
	}
	
}

void free_tree(node *head) {
	if(head == NULL) return;
	
	free_tree(head->left);
	free_tree(head->right);
	
	free(head);
}

void print_sorted(node *head) {
	if(head == NULL) return;
	
	print_sorted(head->left);
	printf("%d ", head->value);
	print_sorted(head->right);
}


int main(int argc, char **argv) {
	int numbers[MAX_NODE_COUNT] = {}, node_count, i;
	char *filename, input[NODE_SPACE + 1];
	node **head = malloc(sizeof(node *));
	*head = NULL;
	if(argc != 2) {
		error("Broj argumenata mora biti 1 - ime datoteke iz koje se citaju brojevi.\n");
	}
	
	filename = argv[1];
	node_count = read_values(filename, numbers);
	
	for(i = 0; i < node_count; i++) {
		insert_node(head, numbers[i]);
	}
	
	while(1) {
		print_tree(head);
		printf("STABLO ISPISANO - UPISITE SLJEDECI BROJ ILI \'q\' ZA ZAVRSETAK\n");
		printf("DODATNE OPCIJE: s - sortiran ispis -- b - ispis faktora ravnoteze\n");
			
		fgets(input, NODE_SPACE + 1, stdin);
		fflush(stdin);
		
		input[NODE_SPACE] = '\0';
		if(input[0] == 'q') {
			printf("Pozdrav korisnice :^)\n");
			break;
		}
		if(input[0] == 's') {
			printf("\nSortiran ispis: ");
			print_sorted(*head);
			printf("\n");
			continue;
		} else if(input[0] == 'b') {
			print_balance(*head);
			continue;
		}
		
		extract_numbers(input, numbers);
		insert_node(head, numbers[0]);
	}
	
	free_tree(*head);
	free(head);
	
	return 0;
}
