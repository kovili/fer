import tkinter as tk
import pyodbc
import pandas as pd
import prettytable
from io import StringIO

CONNECTION_SUCCESS_STATUS: str = 'Uspješno povezan na\n bazu podataka!'
CONNECTION_FAIL_STATUS: str = 'Neuspješno povezivanje na\n bazu podataka. Probaj opet!'
connection_string = "Driver={SQL Server Native Client 11.0};" \
                    "Server=DESKTOP-KBBICS5\SQL2019;" \
                    "Database=AdventureWorksDW2019;" \
                    "Trusted_Connection=yes;"


class BI_App(tk.Frame):

    def __init__(self, parent: tk.Tk, connection: pyodbc.Connection = None):
        tk.Frame.__init__(self, parent)

        # we need to set parent as a class attribute for later use
        self.parent = parent
        parent.title('POSINT Aplikacija')

        # button1 = tk.Button(self.parent, text="Make window larger!", command = self.make_window_bigger)

        self.connection = connection
        self.setup_grid()
        self.initialize_fact_table_button()

    def setup_grid(self):
        # Setup grid(0,0)
        self.input_connection_string_grid: tk.Frame = tk.Frame(self.parent, bg='#A4C5EA', width=400, height=150,
                                                               highlightbackground="black",
                                                               highlightthickness=1)
        self.input_connection_string_grid.grid(row=0, column=0, sticky="nsew")
        self.setup_input_connection_block()

        # Setup grid(0,1)
        self.run_sql_grid: tk.Frame = tk.Frame(self.parent, width=1200, height=150, highlightbackground="black",
                                               highlightthickness=1)
        self.run_sql_grid.grid(row=0, column=1, sticky="nsew")
        self.setup_run_sql_grid()

        # Setup grid(1,0)
        self.fact_table_frame: tk.Frame = tk.Frame(self.parent, bg='#A4C5EA', width=400, height=400,
                                                   highlightbackground="black",
                                                   highlightthickness=1)
        self.fact_table_frame.grid(row=1, column=0, sticky="nsew")

        # Setup grid(1,1)
        self.sql_frame: tk.Frame = tk.Frame(self.parent, width=1200, height=400,
                                            highlightbackground="black", highlightthickness=1)
        self.sql_frame.grid(row=1, column=1)

        # Setup grid(2,0)
        self.measures_dimensions_frame: tk.Frame = tk.Frame(self.parent, bg='#BCA9E1', width=400, height=400,
                                                            highlightbackground="black", highlightthickness=1)
        self.measures_dimensions_frame.grid(row=2, column=0)

        # Setup grid(2,1)
        self.results_frame: tk.Frame = tk.Frame(self.parent, bg='#E7ECA3', width=1200, height=400,
                                                highlightbackground="black", highlightthickness=1)
        self.results_frame.grid(row=2, column=1)

    def make_window_bigger(self):
        x = self.parent.winfo_height() + 10
        y = self.parent.winfo_width() + 10
        self.parent.geometry('{}x{}'.format(y, x))

    def setup_input_connection_block(self):
        curr_parent: tk.Frame = self.input_connection_string_grid

        # Create input
        self.connection_string_input: tk.Entry = tk.Entry(curr_parent, text='Unesi URL za povezivanje na bazu')
        self.connection_string_input.grid(row=0, column=0, padx=(15, 15))

        self.connection_string_status: tk.Label = tk.Label(curr_parent, bg='#A4C5EA', text='')
        self.connection_string_status.grid(row=0, column=1)

        # If input is none set focus
        if self.connection is None:
            self.connection_string_input.focus_set()
        else:
            self.connection_string_status.config(text=CONNECTION_SUCCESS_STATUS, fg='green')

        button: tk.Button = tk.Button(curr_parent, text='Unesi URL', command=self.connect_to_DB)
        button.grid(row=0, column=2, padx=(15, 0))

        self.connection_string_label: tk.Label = tk.Label(curr_parent, bg='#8db7e4', justify="left",
                                                          borderwidth=2, relief="groove",
                                                          text='' if self.connection is None else connection_string.replace(
                                                              ';', '\n'))
        self.connection_string_label.grid(row=1, column=0, columnspan=3, pady=(10, 0))

    def connect_to_DB(self):
        conn_string = self.connection_string_input.get()
        self.destroy_grid_elements(self.fact_table_frame)
        try:
            self.connection = pyodbc.connect(conn_string)
            self.connection_string_status.config(text=CONNECTION_SUCCESS_STATUS, fg='green')
            self.sql_run_button['state'] = 'normal'
            self.connection_string_label.config(borderwidth=2, text=conn_string.replace(';', '\n'))
            self.initialize_fact_table_button()

        except pyodbc.InterfaceError:
            self.connection_string_status.config(text=CONNECTION_FAIL_STATUS, fg='red')
            self.sql_run_button['state'] = 'disabled'
            self.connection_string_label.config(borderwidth=0, text="")

    def setup_run_sql_grid(self):
        curr_parent: tk.Frame = self.run_sql_grid
        self.sql_run_button: tk.Button = tk.Button(curr_parent, width=108, height=5, text='Pokreni SQL',
                                                   command=self.run_sql)
        self.sql_run_button.grid(row=0, column=0)
        self.sql_run_button['state'] = 'normal' if connection_string is not None else 'disabled'
        self.sql_run_button['font'] = 'helv36'

    def run_sql(self):
        pass

    def destroy_grid_elements(self, curr_parent):
        for child in curr_parent.winfo_children():
            child.destroy()

    def initialize_fact_table_button(self):
        curr_parent: tk.Frame = self.fact_table_frame

        df = pd.read_sql_query('SELECT * FROM tablica WHERE sifTipTablica = 1', self.connection)

        self.chosen_option = tk.IntVar()

        for index in range(len(df)):
            button = tk.Radiobutton(curr_parent, text=df['nazTablica'][index].rstrip(), bg='#A4C5EA',
                                    variable=self.chosen_option, value=df['sifTablica'][index],
                                    command=self.radio_button_clicked)
            button.pack(anchor='w')

    def radio_button_clicked(self):
        print(self.chosen_option.get())
        self.initialize_measure_dimensions_frame()
        self.build_simple_query()

    def initialize_measure_dimensions_frame(self):
        pass

    def build_simple_query(self):
        pass


if __name__ == '__main__':
    root = tk.Tk()
    try:
        cnxn = pyodbc.connect(connection_string)
        BI_App(root, cnxn)
    except pyodbc.InterfaceError:
        BI_App(root)
    root.mainloop()
